<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 07.07.15
 * Time: 17:01
 */

include("init.php");

/* preinit classes used in output, that could be changed while update */

kernel\Path::getRoot();
kernel\User::get();
kernel\Output::VERSION;
kernel\Lang::get();
kernel\Lang\Constant\Item::create();
kernel\Settings::get();
kernel\Template::get();
kernel\Template\Item::create();
kernel\Style::get();
kernel\Style\Item::create();

/* ^^ preinit classes */

kernel\Migration::get()->proceed();