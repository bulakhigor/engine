<?

include("init.php");

function debug($name){
    echo "<pre>";
    print_r($name);
    echo "<pre>";
}

/* check site is closed */
if (!kernel\User\Current::get()->readIsAdmin())
    if (intval(kernel\Settings::get()->readValue('maintenance_mode')))
    {
        kernel\Output::get()->error('Website is now in maintenance mode. Please visit us later.');
    }


// on index request
kernel\Router::get()->addRoute('/', function(){ return routePage(kernel\Settings::get()->readValue('index_page')); });

// specific
/*
kernel\Router::get()->addRoute('category/:categoryName/:productName', function($categoryName, $productName) {
    echo 'Category Page: '.$categoryName.', '.$productName;
    return true;
});
*/

// on all requests
kernel\Router::get()->addRoute('*', function($url){ return routePage($url); });

// other router for same request type
/*
kernel\Router::get()->addRoute('*', function($req){
    echo "do something";
    return true;
});
*/

// fallback function
kernel\Router::get()->addRoute('*', function($req){ return routeFail($req); });

// execute Router
kernel\Router::get()->execute();

/**
 * Action in case of routing page
 * @param string $url
 * @return bool
 */
function routePage($url)
{
    $page = kernel\Page::get()->readByUrl($url);
    if (!$page->readId()) return false; // page not found

    $trans = $page->readTrans(\kernel\Lang::get()->getUsing());
    if (!$trans->readIsEnabled())
    {
        $trans = $page->readTrans($page->readTransLanguagesEnabled()[0]);
        if (!$trans->readIsEnabled()) return false; // no enabled translations
        $noTranslationWarning = '<div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><#translation-missing-title#>&nbsp;&rarr;&nbsp;'.\kernel\Lang::get()->readById($trans->readLangId())->readName().'</h3>
                                    </div>
                                    <div class="panel-body">
                                        <#translation-missing#>
                                    </div>
                                </div>';
    } else {
        $noTranslationWarning = '';
    }

    if ($page->readIsUseAffix()) {
        $content = "<div class='row'>
                        <div class='col-md-9'>
                            <div class='btn-group page-affix-dropdown-btn visible-xs visible-sm'>
                                <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>Оглавление <span class='caret'></span></button>
                                <ul class='dropdown-menu nav' role='menu'>" . $trans->readAffix() . "</ul>
                            </div>" .
            $noTranslationWarning . $trans->readHtml() . "
                        </div>
                        <div class='hidden-xs hidden-sm col-md-3'>
                            <nav id='page-affix' data-spy='affix' data-offset-top='65'>
                                <ul class='nav'>" . $trans->readAffix() . "</ul>
                            </nav>
                        </div>
                    </div>";
    } else {
        $content = $noTranslationWarning . $trans->readHtml();
    }
    kernel\Output::get()->writeOption('title_short', $trans->readTitleShort())
                        ->writeOption('title_long', $trans->readTitleLong())
                        ->writeOption('contains', $content)
                        ->writeOption('keywords', $trans->readKeywords())
                        ->writeOption('description', $trans->readDescription())
                        ->writeOption('menu_active', $page->readOverrideActiveLinkId() ? $page->readOverrideActiveLinkId() : $url)
                        ->writeTemplate($page->readTemplateId())
                        ->render();
    return true;
}

/**
 * Action in case of routing failure
 * @param string $req
 * @return bool
 */
function routeFail($req)
{
    // show index page on error
    routePage(kernel\Settings::get()->readValue('index_page'));
    return true;
}