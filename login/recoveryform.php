<?
use kernel\Output\Snippets;

include("../init.php");

	$captcha = Snippets::captcha(kernel\Security\Captcha::set());
	$out = <<<HTM
		<p><#recovery-start-info#></p>

		<br><br>

        <div class='container-fluid'>
            <div class='row'>
		        <div class='panel panel-default col-xs-12 col-sm-8'>
		            <div class='panel-body'>
		                <form action='/login/recoverysearch.php' method='post'>
			                <div class='form-group'>
				                <label>Email</label>
				                <input class='form-control' name='myemail' value=''>
			                </div>
			                <br>
			                {$captcha}
			                <br><input type=submit class='btn btn-primary btn-lg' value='<#send-me-recovery#>'>
		                </form>
		            </div>
                </div>
            </div>
        </div>
HTM;

kernel\Output::get()->writeOption('title_short', '<#password-recovery#>')
                    ->writeOption('contains', $out)
                    ->render();
