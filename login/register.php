<?

use kernel\Output\Scripts;

include("../init.php");

	$newid = kernel\User::get()->write(
                kernel\User\Item::create()
			        ->writeName(kernel\Input::get()->read('name'))
			        ->writeEmail(kernel\Input::get()->read('email'))
			        ->writePassword(kernel\Input::get()->read('password'))
		);

	if ($newid)
	{
		$mail = "<#register-email#>";
		$resultsend = kernel\Email::send(kernel\Input::get()->read('email'), $mail, "<#register#>");
		$html = "<form name='autologin' action='/login/' method='post'>
					<input type='hidden' name='log' value='".trim(kernel\Input::get()->read('email'))."'>
					<input type='hidden' name='pwd' value='".trim(kernel\Input::get()->read('password'))."'>
					<input type='hidden' name='ref' value='".kernel\Input::get()->read('ref')."'>
					<input type='hidden' name='coupon_code' value='".kernel\Input::get()->read('coupon_code')."'>
					<center><button class='btn btn-primary btn-lg' type='submit'><#login#></button></center>
				</form>";
		$sc = "<script>$(function() { document.forms['autologin'].submit(); });</script>";
		Scripts::get()->addScript($sc);
		kernel\Output::get()->writeOption('title_short', 'Autologin')
                            ->writeOption('contains', $html)
                            ->render();
	} else {
		kernel\Output::get()->error("<#register-error#>: ".kernel\User::get()->readErrorMessage());
	}

