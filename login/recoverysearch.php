<?

include("../init.php");

if (!kernel\Security\Captcha::check(
        kernel\Input::get()->read('captcha_rid'),
        kernel\Input::get()->read('captcha'))
) {
    kernel\Output::get()->error("<#captcha-error#>");
}

if (kernel\User::get()->sendRecoveryEmail(kernel\Input::get()->read('myemail')))
{
    $out = "<div class='panel panel-success'>
				<div class='panel-heading'>
					<h3 class='panel-title'><#email-sent#></h3>
				</div>
	  			<div class='panel-body'>
					<#recovery-email-sent#>
				</div>
			</div>";
    kernel\Output::get()->writeOption('title_short', '<#password-recovery#>')
                        ->writeOption('contains', $out)
                        ->render();
} else {
    kernel\Output::get()->error("Сбой");
}

