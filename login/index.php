<?
use kernel\User;

include("../init.php");

$isValid = User::get()->validate(kernel\Input::get()->read('log'), kernel\Input::get()->read('pwd'));

if (!$isValid) {
	kernel\Output::get()->error("<#auth-error#>: ".kernel\User::get()->readErrorMessage());
}


if (kernel\Input::get()->read('ref'))
{	
	$redirect = kernel\Input::get()->read('ref').
				(strstr(kernel\Input::get()->read('ref'),'?') ? "&" : "?").
				(kernel\Input::get()->read('coupon_code') ? "coupon_code=".kernel\Input::get()->read('coupon_code') : "");
} elseif ($_SERVER['HTTP_REFERER'])
{
	$redirect = $_SERVER['HTTP_REFERER'].
				(strstr($_SERVER['HTTP_REFERER'],'?') ? "&" : "?").
				(kernel\Input::get()->read('coupon_code') ? "coupon_code=".kernel\Input::get()->read('coupon_code') : "");
} else {
	$redirect = '/index.php';
}
kernel\Output::get()->redirect($redirect);
