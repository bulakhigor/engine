<?
include("../init.php");


// check inputs
if ( ! (
        (kernel\Input::get()->readInt('myid')) &&
        (kernel\Input::get()->read('mykey')))
    ) {
    kernel\Output::get()->error("<#id-and-key-error#>");
}

// check password length
if (strlen(kernel\Input::get()->read('mypassword')) < kernel\Security::getPasswordLength()) {
    kernel\Output::get()->error("<#password-length-error#>");
}

// check captcha
if (!kernel\Security\Captcha::check(
        kernel\Input::get()->read('captcha_rid'),
        kernel\Input::get()->read('captcha'))
) {
    kernel\Output::get()->error("<#captcha-error#>");
}


if (kernel\User::get()->changePassword(
        kernel\Input::get()->readInt('myid'),
        kernel\Input::get()->read('mykey'),
        kernel\Input::get()->read('mypassword'))
) {
    $out = "<div class='panel panel-success'>
				<div class='panel-heading'>
					<h3 class='panel-title'><#password-changed#></h3>
				</div>
	  			<div class='panel-body'>
					<#password-really-changed#>
				</div>
			</div>";
    kernel\Output::get()->writeOption('title_short', '<#password-recovery#>')
                        ->writeOption('contains', $out)
                        ->render();
} else {
    kernel\Output::get()->error(kernel\User::get()->readErrorMessage());
}