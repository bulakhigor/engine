<?
use kernel\Output\Snippets;

include("../init.php");

$captcha = Snippets::captcha(kernel\Security\Captcha::set());
$out = <<<HTM
	<p><#recovery-form-notice#></p>
	<br><br>

    <div class='container-fluid'>
        <div class='row'>
            <div class='panel panel-default col-xs-12 col-sm-8'>
                <div class='panel-body'>
                    <form action='/login/recoveryfinish.php' method='post'>
                        <div class='form-group'>
                            <label><#your-id#></label>
                            <input class='form-control' name='myid' value=''>
                        </div>
                        <div class='form-group'>
                            <label><#your-key#></label>
                            <input class='form-control' name='mykey' value=''>
                        </div>
                        <div class='form-group'>
                            <label><#new-password#></label>
                            <input class='form-control' type=password name='mypassword' value=''>
                        </div>
                        <br>
                        {$captcha}
                        <br><input class='btn btn-primary btn-lg' type=submit value='<#set-new-password#>'>
                    </form>
                </div>
            </div>
        </div>
    </div>

HTM;

kernel\Output::get()->writeOption('title_short', '<#password-recovery#>')
                    ->writeOption('contains', $out)
                    ->render();
