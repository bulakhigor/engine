<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 04.08.15
 * Time: 18:23
 */

include_once("init.php");

// fetch input
$text = kernel\Input::get()->read('text');

// convert input string into array of cleaned words
$words = kernel\Regex::cleanSearch($text);

// search
$result = kernel\Page::get()->search($words);

// show results
$html = "<h3><#search-results-for#> ".implode(" ", $words)."</h3>";
foreach($result as $k => $row)
{
    $page = kernel\Page::get()->readById( $row['page_id'] );
    $lang = kernel\Lang::get()->readByCode($row['lang_code']);
    $trans = $page->readTrans( $lang->readCode() );
    $clear = strip_tags($trans->readHtml());
    $html .= "<p>
                <a href='/".$page->readUrl()."?lang=".$lang->readCode()."'>
                    <strong>".
                        ($k+1).". ".$trans->readTitleLong()." [".$lang->readName()."]
                    </strong>
                </a>
              </p>
              ";
    foreach($words as $word)
    {
        $pos = mb_stripos($clear, $word, 0, 'utf-8');
        if ($pos!==false) {
            $part = mb_substr($clear, $pos < 300 ? 0 : $pos - 300, $pos + 300, 'utf-8');
            $part = preg_replace("/".$word."/siu", "<strong>" . $word . "</strong>", $part);
            $html .= "
                <p>
                    <em>".
                        $part."&hellip;
                    </em>
                </p>";
        }
    }
}

kernel\Output::get()->writeOption('title_short', '<#search#>')
                    ->writeOption('contains', $html)
                    ->render();