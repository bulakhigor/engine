<?

include("init.php");

if (!kernel\Input::get()->read('service')) kernel\Output::get()->error('<#error-message-empty#>');
if (!kernel\Input::get()->read('Family-name')) kernel\Output::get()->error('<#error-name-empty#>');
if (!kernel\Input::get()->read('Name')) kernel\Output::get()->error('<#error-name-empty#>');
if (!kernel\Input::get()->read('sex')) kernel\Output::get()->error('sex is empty!');
if (!kernel\Input::get()->read('Email')) kernel\Output::get()->error('<#error-email-empty#>');


$ref = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : "/";
kernel\Email::send_to_admin(
    kernel\Input::get()->read('Email'),
    "<p><#message-from#> ".
    kernel\Input::get()->read('Family-name')." ".
    kernel\Input::get()->read('Name').
    "</p><p>Refer: ".$ref."</p>".
    "<p>sex: ".(kernel\Input::get()->read('sex')==1 ? 'male' : 'female')."</p>".
    "<p>".kernel\Input::get()->read('service')."</p>",
    '<#feedback-link#>'
);
kernel\Output::get()->redirect($ref);
