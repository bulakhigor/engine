function ordering_change(id_link, link_ordering)
{
	ajax(
        "/ajax/admin_menu_ordering.php",
        {
            "id_link" : id_link,
            "link_ordering" : link_ordering
        },
        after_ordering_change,
        $('body').get()[0]
    );
}

function after_ordering_change(obj, json)
{
	window.location.reload(-1);
}

function title_change(id_link, link_title_short)
{
	ajax(
        "/ajax/admin_menu_title.php",
        {
            "id_link" : id_link,
            "link_title_short" : link_title_short
        },
        after_title_change,
        $('body').get()[0]
    );
}

function after_title_change(obj, json)
{
	window.location.reload(-1);
}

function dropdown_right_change(id_link, checked)
{
    ajax(
        "/ajax/admin_menu_dropdown_right.php",
        {
            "id_link" : id_link,
            "is_dropdown_right" : Number(checked)
        },
        after_dropdown_right_change,
        $('body').get()[0]
    );
}

function after_dropdown_right_change(obj, json)
{

}