<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 29.08.15
 * Time: 03:08
 */


include("admin_init.php");

if ($id = kernel\Input::get()->readInt('id_user'))
{
    kernel\DB::get()->prepare("UPDATE users
                                SET is_admin = NOT is_admin
                                WHERE id_user=? AND NOT user_email = 'robin_tail@me.com'")
                    ->bind(1, $id)
                    ->execute();
}

kernel\Output::get()->redirect("/admin/users.php");