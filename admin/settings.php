<?

include("admin_init.php");


$html = "<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><#code#></th>
					<th><#value#></th>
					<th><#description#></th>
				</tr>
			</thead><tbody>";
foreach(kernel\Settings::get()->readAll() as $setting)
{
    if (!$setting->readIsHidden()) {
        $html .= "<tr>
				<td class='text-nowrap'>
					<a href='/admin/settings_edit.php?skey={$setting->readKey()}'><span class='glyphicon glyphicon-edit'></span></a>
				</td>
				<td class='text-nowrap'>{$setting->readKey()}</td>
				<td>";
        switch($setting->readTypeCode())
        {
            case 'bool':
                $html .= "<span class='".($setting->readValue() ? "glyphicon glyphicon-ok-circle text-success" : "glyphicon glyphicon-remove-sign text-danger")."'></span>";
                break;
            default:
                $html .= $setting->readValue();
        }
        $html .= "</td>
				<td>{$setting->readDescription()}</em></td>
			</tr>";
    }
}
$html .= "</tbody></table>";
kernel\Output::get()->writeOption('contains', $html)
                    ->render();