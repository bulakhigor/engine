<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 03.08.15
 * Time: 13:54
 */

include("admin_init.php");

$html = "<p><a href='/admin/user_edit.php' class='pull-right btn btn-lg btn-primary'><#new-user#></a></p>
        <table class='table'>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th><#name#></th>
                    <th><#email#></th>
                    <th class='text-center'><#admin#></th>
                </tr>
            </thead><tbody>";

foreach(kernel\User::get()->readAll() as $item)
{
    $html .= "<tr>
                <td>
                    <a href='/admin/user_edit.php?id_user=".$item->readId()."'><span class='glyphicon glyphicon-edit'></span></a>
                    <a href='/admin/user_remove.php?id_user=".$item->readId()."' onclick=\"return confirm('<#confirm-user-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                </td>
                <td>".$item->readName()."</td>
                <td>".$item->readEmail()."</td>
                <td class='text-center'><a onclick='return confirm(\"<#confirm-change-permissions#>\");' href='/admin/user_admin.php?id_user=".$item->readId()."'><span class='glyphicon ".($item->readIsAdmin() ? "glyphicon-ok-sign text-danger" : "glyphicon-remove-sign text-muted")."'></span></a></td>
            </tr>";
}

$html .= "</tbody></table>";

kernel\Output::get()->writeOption('contains', $html)
                    ->render();