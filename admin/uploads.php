<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 18.08.15
 * Time: 23:17
 */

use kernel\Output\Scripts;

include("admin_init.php");

Scripts::get()->addScript('<script type="text/javascript" src="/vendor/moxiecode/plupload/js/plupload.full.min.js"></script>', true);
Scripts::get()->addVar('upload_dir', kernel\Settings::get()->readValue('upload_dir'));
Scripts::get()->addVar('thumb_size_filemanager', kernel\Settings::get()->readValue('thumb_size_filemanager'));
Scripts::get()->addVar('load_more', '<#load-more#>');
Scripts::get()->addVar('confirm_image_remove', '<#confirm-image-remove#>');
Scripts::get()->addScript("<script src='/admin/upload.js'></script>");
Scripts::get()->addScript("<script src='/admin/page_edit.js'></script>");
Scripts::get()->addScript("<script>file_manager();</script>");

kernel\Output::get()->writeOption('contains', $html)
                    ->render();