<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 12:51
 */

include("admin_init.php");

kernel\Style::get()->remove(kernel\Input::get()->readInt('id_style'));

kernel\Output::get()->redirect('/admin/styles.php');