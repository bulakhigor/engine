<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 17.07.15
 * Time: 16:49
 */

include("admin_init.php");

$id = kernel\Input::get()->readInt('id_lang');

if (!$id) kernel\Output::get()->error('id_lang - <#error-smth-not-specified#>');

$result = kernel\Lang::get()->remove($id, $err);

if ($result)
{
    kernel\Output::get()->redirect('/admin/language.php');
} else {
    kernel\Output::get()->error($err);
}