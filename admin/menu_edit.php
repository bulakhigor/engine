<?

include("admin_init.php");

function iteration($parent=NULL,$depth=0)
{
	$children = kernel\Menu::get()->readAllByParentId($parent);
	$arr=array();
	foreach($children as $link)
	{
        if ($link->readPageId()) {
            $menuTitle = kernel\Page::get()->readById($link->readPageId())->readTitleShortInUsingLang();
        } else {
            $menuTitle = $link->readTitle();
        }
		$arr[] = array('id_link'=>$link->readId(), 'title'=>str_repeat(kernel\Settings::get()->readValue('menu_depth'),$depth).$menuTitle);
		$arr = array_merge($arr, iteration($link->readId(), $depth+1));
	}
	return $arr;
}

if (kernel\Input::get()->readInt('update'))
{
	// insert
	if (kernel\Input::get()->read('element_type')=='page')
	{
        kernel\Menu::get()->createPageItem(
            kernel\Input::get()->readInt('parent_link_id'),
            kernel\Input::get()->readInt('link_page_id')
        );
	} elseif (kernel\Input::get()->read('element_type')=='text')
	{
        kernel\Menu::get()->createTextItem(
            kernel\Input::get()->readInt('parent_link_id'),
            kernel\Input::get()->read('link_title_short')
        );
	}
	kernel\Output::get()->redirect('/admin/menu.php');
}

$parents = \kernel\Output\Forms::select(array('name'=>'parent_link_id', 'array'=>array_merge(array(array('id_link'=>NULL,'title'=>'<#no-parent#>')),iteration()), 'valuefield'=>'id_link', 'captionfield'=>'title', 'class'=>'form-control'));

$sortedPages = kernel\Page::get()->readAll();
usort(
    $sortedPages,
    /**
     * @param \kernel\Page\Item $a
     * @param \kernel\Page\Item $b
     * @return int
     */
    function($a, $b) {
        /**
         * @var \kernel\Page\Item $a
         * @var \kernel\Page\Item $b
         */
        return mb_strtolower($a->readTitleShortInUsingLang(), 'utf-8') <= mb_strtolower($b->readTitleShortInUsingLang(), 'utf-8') ? -1 : 1;
    }
);

$pages = \kernel\Output\Forms::select(array('name'=>'link_page_id', 'array'=>$sortedPages, 'valuefield'=>'readId', 'captionfield'=>'readTitleShortInUsingLang', 'class'=>'form-control'));

$html = <<<HTM
	<form action='' method='post'>
		<input type='hidden' name='update' value='1'>
		<div class='form-group'>
			<label><#parent#></label>
			{$parents}
		</div>		
		<div class='radio'>
			<label>
				<input type='radio' name='element_type' value='page' checked> <#page#>
			</label>
		</div>
		<div class='form-group'>
			<label><#page#></label>
			{$pages}
		</div>		
		<div class='radio'>
			<label>
				<input type='radio' name='element_type' value='text'> <#text#>
			</label>
		</div>
		<div class='form-group'>
			<label><#text#></label>
			<input name='link_title_short' class='form-control'>
		</div>		
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>
HTM;
kernel\Output::get()->writeOption('contains', $html)
                    ->render();