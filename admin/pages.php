<?

include("admin_init.php");

$pages = kernel\Page::get()->readAll();
$html = "
		<p><a href='/admin/page_edit.php' class='pull-right btn btn-lg btn-primary'><#new-page#></a></p>
		<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th>URL</th>
					<th><#title#></th>
					<th>SEO</th>
					<th><#template#></th>
				</tr>
			</thead><tbody>";
foreach($pages as $item)
{
    $idPage = $item->readId();
    $url = $item->readUrl();
    $titleShort = $item->readTitleShortInUsingLang();
    $templateCode = kernel\Template::get()->readById($item->readTemplateId())->readCode();
    $description = $item->readDescriptionInUsingLang();
    $keywords = $item->readKeywordsInUsingLang();
	$html .= "<tr>
				<td class='text-nowrap'>
					<a href='/admin/page_edit.php?id_page={$idPage}'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/admin/page_remove.php?id_page={$idPage}' onclick=\"return confirm('<#confirm-page-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
				</td>
				<td class='text-nowrap'><a href='/{$url}'>{$url}</a></td>
				<td>{$titleShort}</td>
				<td><#description#>: {$description}<br><#keywords#>: {$keywords}</td>
				<td>{$templateCode}</td>
			</tr>";
}
$html .= "</tbody></table>";
kernel\Output::get()->writeOption('contains', $html)
                    ->render();