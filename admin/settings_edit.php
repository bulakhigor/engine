<?

use kernel\Output\Scripts;

include("admin_init.php");

$skey = kernel\Input::get()->read('skey');
$setting = kernel\Settings::get()->readByKey($skey);
if ($setting->readIsHidden()) \kernel\Output::get()->error("This setting is hidden. You can not change it.");

if (kernel\Input::get()->readInt('update'))
{
	if (kernel\Input::get()->read('skey'))
	{
		// update
		kernel\Settings::get()->write(kernel\Input::get()->read('skey'), kernel\Input::get()->read('value'));
	}
	kernel\Output::get()->redirect('/admin/settings.php');
}

$html = <<<HTM
	<form id='settings_edit' action='' method='post'>
		<input type='hidden' name='update' value='1'>
		<div class='form-group'>
			<label><#code#></label>
			<input class='form-control' name='template_code' value='{$skey}' readonly disabled>
		</div>		
HTM;

switch($setting->readTypeCode())
{
    case 'int':
    case 'float':
        $html .= "<div class='form-group'>
			        <label><#value#></label>
			        <p class='help-block'>".$setting->readDescription()."</p>".
                    kernel\Output\Forms\Input::create()
                        ->setName('value')
                        ->setValue($setting->readValue())
                        ->setClass('form-control')
                        ->render().
                "</div>";
        break;
    case 'bool':
        $html .= "<div class='checkbox'><label>";
        $html .= kernel\Output\Forms\Input::create()
                    ->setType('checkbox')
                    ->setName('value')
                    ->setValue(1)
                    ->setChecked($setting->readValue())
                    ->render();
        $html .= $setting->readDescription()."</label></div>";
        break;
    case 'html':
        $html .= "<div class='form-group'>
			        <label><#value#></label>
			        <p class='help-block'>".$setting->readDescription()."</p>
                    <textarea class='form-control' style='display: none;' id='value' name='value'></textarea>
                    <div id='value_ace'>{$setting->readHtmlValueForEditor()}</div>
                </div>";
        Scripts::get()->addScript("<script src='/admin/settings_edit.js'></script>");
        break;
    case 'string':
    default:
        $html .= "<div class='form-group'>
			        <label><#value#></label>
			        <p class='help-block'>".$setting->readDescription()."</p>
                    <textarea class='form-control' style='height: 400px;' id='value' name='value'>{$setting->readValue()}</textarea>
                </div>";
}

$html .= <<<HTM
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>
HTM;
kernel\Output::get()->writeOption('contains', $html)
                    ->render();