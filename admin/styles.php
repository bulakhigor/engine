<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 08:43
 */

include("admin_init.php");

$styles = kernel\Style::get()->readAll();
$html = "
        <p><a href='/admin/style_edit.php' class='pull-right btn btn-lg btn-primary'><#new-style#></a></p>
        <table class='table'>
			<thead>
				<tr>
				    <th>&nbsp;</th>
				    <th><#code#></th>
				    <th><#description#></th>
				    <th><#type#></th>
                </tr>
            </thead><tbody>";

foreach($styles as $style)
{
    $html .= "<tr>
                <td class='text-nowrap'>
                    <a href='/admin/style_edit.php?id_style={$style->readId()}'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/admin/style_remove.php?id_style={$style->readId()}' onclick=\"return confirm('<#confirm-style-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                </td>
                <td>".$style->readCode()."</td>
                <td>".$style->readDescription()."</td>
                <td>".($style->readLink() ? '<#link#>' : '<#text#>')."</td>
              </tr>";
}

$html .= "</tbody></table>";

kernel\Output::get()->writeOption('contains', $html)
                    ->render();