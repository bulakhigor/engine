<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 17.07.15
 * Time: 16:55
 */

use kernel\Output\Scripts;

include("admin_init.php");

$lang = kernel\Lang::get()->readById(kernel\Input::get()->readInt('id_lang'));
if ($lang->readIsProtected()) kernel\Output::get()->error('<#error-language-protected#>');

if (kernel\Input::get()->readInt('update'))
{
    if (kernel\Lang::get()->isExists(
        kernel\Input::get()->read('lang_code'),
        kernel\Input::get()->readInt('id_lang')
    )){
        kernel\Output::get()->error('<#error-lang-code-exists#>');
    }

    kernel\Lang::get()->write(
        kernel\Input::get()->readInt('id_lang'),
        kernel\Input::get()->read('lang_code'),
        kernel\Input::get()->read('lang_name'),
        kernel\Input::get()->read('lang_flag')
    );
    kernel\Output::get()->redirect('/admin/language.php');
}

$id = $lang->readId();
$code = $lang->readCode();
$name = $lang->readName();
$flag = $lang->readFlag();

$html = <<<HTM
    <form action='' method='post'>
        <input type='hidden' name='id_lang' value='{$id}'>
        <input type='hidden' name='update' value='1'>
        <div class='form-group has-feedback'>
            <label class='control-label'><#code#></label>
            <input class='form-control' name='lang_code' value='{$code}' onchange="check_lang_code();">
            <span class="glyphicon form-control-feedback"></span>
            <div class='help-block'></div>
        </div>
        <div class="form-group">
            <label><#language#></label>
            <input class="form-control" name="lang_name" value="{$name}">
        </div>
        <div class="form-group">
            <label><#flag#></label>
            <p class="help-block"><#flag-help#></p>
            <input class="form-control" name="lang_flag" value="{$flag}">
        </div>
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>
HTM;

Scripts::get()->addScript("<script src='/admin/language_edit.js'></script>");
kernel\Output::get()->writeOption('contains', $html)
                    ->render();