<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 17.07.15
 * Time: 16:05
 */

include("admin_init.php");

$html = <<<HTM
    <p><a href='/admin/language_edit.php' class='pull-right btn btn-lg btn-primary'><#new-language#></a></p>
    <table class="table">
        <thead>
            <tr>
                <th>&nbsp;</th>
                <th><#code#></th>
                <th><#language#></th>
                <th class='text-center'><#flag#></th>
                <th class='text-center'><#enabled#></th>
                <th class='text-center'><#domestic#></th>
                <th class='text-center'><#international#></th>
                <th class='text-center'><#protected#></th>
            </tr>
        </thead>
        <tbody>
HTM;

foreach(kernel\Lang::get()->readAll() as $item)
{
    $html .= "<tr>
                    <td>
                        <a href='/admin/language_edit.php?id_lang=".$item->readId()."'><span class='glyphicon glyphicon-edit'></span></a>
                        <a href='/admin/language_remove.php?id_lang=".$item->readId()."' onclick=\"return confirm('<#confirm-language-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
                    </td>
                    <td>".$item->readCode()."</td>
                    <td>".$item->readName()."</td>
                    <td class='text-center'><img src='".$item->readFlag()."'></td>
                    <td class='text-center'><a href='/admin/language_enable.php?id_lang=".$item->readId()."'><span class='".($item->readIsEnabled() ? 'glyphicon glyphicon-ok-sign text-success' : 'glyphicon glyphicon-remove-sign text-danger')."'></span></a></td>
                    <td class='text-center'><a href='/admin/language_domestic.php?id_lang=".$item->readId()."'><span class='".($item->readIsDomestic() ? 'glyphicon glyphicon-ok-sign text-success' : 'glyphicon glyphicon-ok-circle text-muted')."'></span></a></td>
                    <td class='text-center'><a href='/admin/language_international.php?id_lang=".$item->readId()."'><span class='".($item->readIsInternational() ? 'glyphicon glyphicon-ok-sign text-success' : 'glyphicon glyphicon-ok-circle text-muted')."'></span></a></td>
                    <td class='text-center'><span class='".($item->readIsProtected() ? 'glyphicon glyphicon-lock text-danger' : '')."'></span></td>
            </tr>";
}

$html .= <<<HTM
		</tbody>
    </table>
HTM;


kernel\Output::get()->writeOption('contains', $html)
                    ->render();