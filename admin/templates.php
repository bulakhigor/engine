<?

include("admin_init.php");

$templates = \kernel\Template::get()->readAll();
$html = "
		<p><a href='/admin/template_edit.php' class='pull-right btn btn-lg btn-primary'><#new-template#></a></p>
		<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><#code#></th>
					<th><#description#></th>
				</tr>
			</thead><tbody>";
foreach($templates as $template)
{
	$html .= "<tr>
				<td class='text-nowrap'>
					<a href='/admin/template_edit.php?id_template={$template->readId()}'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/admin/template_remove.php?id_template={$template->readId()}' onclick=\"return confirm('<#confirm-template-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
				</td>
				<td class='text-nowrap'>{$template->readCode()}</td>
				<td>{$template->readDescription()}</td>
			</tr>";
}
$html .= "</tbody></table>";

kernel\Output::get()->writeOption('contains', $html)
                    ->render();