<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 29.08.15
 * Time: 04:01
 */

include("admin_init.php");

$id = kernel\Input::get()->readInt('id_user');
if (kernel\User::get()->readById($id)->readEmail() != 'robin_tail@me.com')
    kernel\User::get()->remove($id);

kernel\Output::get()->redirect("/admin/users.php");