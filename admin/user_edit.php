<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 29.08.15
 * Time: 03:13
 */

include("admin_init.php");


$item = kernel\User::get()->readById(kernel\Input::get()->readInt('id_user'));
if ($item->readEmail() == 'robin_tail@me.com') kernel\Output::get()->error("You can not change this user");

if (kernel\Input::get()->readInt('update'))
{
    if (!kernel\User::get()->write(
        kernel\User\Item::create()
            ->writeId(kernel\Input::get()->readInt('id_user'))
            ->writeName(kernel\Input::get()->read('user_name'))
            ->writePassword(kernel\Input::get()->read('user_password'))
            ->writeEmail(kernel\Input::get()->read('user_email'))
            ->writeIsAdmin(kernel\Input::get()->readBool('is_admin'))
    ))
    {
        kernel\Output::get()->error(kernel\User::get()->readErrorMessage());
    }
    kernel\Output::get()->redirect("/admin/users.php");
}



$checkedIsAdmin = $item->readIsAdmin() ? "checked" : "";

$out = <<<HTM
    <form action='' method='post'>
        <input type='hidden' name='id_user' value='{$item->readId()}'>
        <input type='hidden' name='update' value='1'>
        <div class='form-group has-feedback'>
            <label class='control-label'><#email#></label>
            <input class='form-control' name='user_email' value='{$item->readEmail()}' onchange="check_user_email();">
            <span class="glyphicon form-control-feedback"></span>
            <div class='help-block'></div>
        </div>
        <div class="form-group">
            <label><#name#></label>
            <input class="form-control" name="user_name" value="{$item->readName()}">
        </div>
        <div class="form-group">
            <label><#new-password#></label>
            <input class="form-control" name="user_password" value="">
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="is_admin" value="1" {$checkedIsAdmin}> <#admin-rights#>
            </label>
        </div>

		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>

HTM;

kernel\Output::get()->writeOption('contains', $out)
                    ->render();