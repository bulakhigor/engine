<?

use kernel\Output\Scripts;

include("admin_init.php");

function iteration_output($parent=NULL, $depth=0)
{
	$html = "";
    $plink = kernel\Menu::get()->readById($parent);
	$children = kernel\Menu::get()->readAllByParentId($parent);
	foreach($children as $link)
	{
		$ordering = \kernel\Output\Forms::select(array('array'=>range(0,count($children)), 'name'=>'link_ordering', 'selected'=>$link->readOrdering(), 'submit'=>"ordering_change(".$link->readId().", this.value);", 'class' => 'form-control'));
        if ($plink->readPageId())
        {
            $parentTitle = kernel\Page::get()->readById($plink->readPageId())->readTitleShortInUsingLang();
        } else {
            $parentTitle = "<em>".$plink->readTitle()."</em>";
        }
        if ($link->readPageId())
        {
            $menuTitle = "<a href='/admin/page_edit.php?id_page=".$link->readPageId()."'>".kernel\Page::get()->readById($link->readPageId())->readTitleShortInUsingLang()."</a>";
        } else {
            $menuTitle = "<input onchange='title_change(".$link->readId().", this.value);' class='form-control' value='".$link->readTitle()."'>";
        }
        if ($depth==0)
        {
            $dropdownRight = \kernel\Output\Forms\Input::create()
                            ->setType('checkbox')
                            ->setValue(1)
                            ->setChecked($link->readIsDropdownRight())
                            ->setOnChange("dropdown_right_change(".$link->readId().", this.checked);")
                            ->render();
        } else {
            $dropdownRight = "&nbsp;";
        }
		$html .= "<tr>
					<td class='text-nowrap'>
						<a href='/admin/menu_remove.php?id_link=".$link->readId()."' onclick=\"return confirm('<#confirm-menu-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
					</td>
					<td>".$parentTitle."</td>
					<td>".$menuTitle."</td>
					<td>".$ordering."</td>
					<td class='text-center'>".$dropdownRight."</td>
				</tr>";
		$html .= iteration_output($link->readId(), $depth+1);
	}
	return $html;
}

$html = "<p><a href='/admin/menu_edit.php' class='pull-right btn btn-lg btn-primary'><#new-element#></a></p>
		<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><#parent#></th>
					<th><#element#></th>
					<th><#ordering#></th>
					<th class='text-center'><#dropdown-right#></th>
				</tr>
			</thead><tbody>";
$html .= iteration_output();
$html .= "</tbody></table>";

Scripts::get()->addScript("<script src='/admin/menu.js'></script>");
kernel\Output::get()->writeOption('contains', $html)
                    ->render();