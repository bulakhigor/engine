# branch admintranslate


# add new language constants for admin menu

insert into language_const (const_code)
VALUES
  ('confirm-menu-remove'),
  ('parent'),
  ('element'),
  ('ordering'),
  ('dropdown-right'),
  ('new-element');

# add new translations for admin menu

insert into language_trans (const_id, lang_id, trans_value)
select id_const, id_lang, pre.trans_value
from (
       select 'confirm-menu-remove' as const_code, ttt.*
       from (
              select 'ru' as lang_code, 'Удалить элемент меню?' as trans_value
              union all
              select 'en', 'Remove menu element?'
            ) as ttt
     ) as pre
  inner join language_const on pre.const_code=language_const.const_code
  inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'parent' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Родитель' as trans_value
                union all
                select 'en', 'Parent'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'element' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Элемент' as trans_value
                union all
                select 'en', 'Element'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'ordering' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Сортировка' as trans_value
                union all
                select 'en', 'Order'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'dropdown-right' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Выпадает справа' as trans_value
                union all
                select 'en', 'Dropdown right'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-element' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новый элемент' as trans_value
                union all
                select 'en', 'New element'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


# add new language constants for admin menu_edit

insert into language_const (const_code)
VALUES
  ('no-parent'),
  ('page'),
  ('text');

# add new translations for admin menu_edit

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'no-parent' as const_code, ttt.*
         from (
                select 'ru' as lang_code, '-без родителя-' as trans_value
                union all
                select 'en', '-no parent-'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'page' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Страница' as trans_value
                union all
                select 'en', 'Page'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'text' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Текст' as trans_value
                union all
                select 'en', 'Text'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;
