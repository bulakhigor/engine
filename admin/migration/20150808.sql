# branch search

# new const
INSERT INTO language_const (const_code)
SELECT 'search';

# translations
INSERT INTO language_trans (lang_id, const_id, trans_value)
SELECT id_lang, id_const, 'Поиск'
FROM languages, language_const
WHERE lang_code='ru' AND const_code='search';

INSERT INTO language_trans (lang_id, const_id, trans_value)
SELECT id_lang, id_const, 'Search'
FROM languages, language_const
WHERE lang_code='en' AND const_code='search';
