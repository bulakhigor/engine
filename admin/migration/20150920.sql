# issue 76, branch trans

# add new field

alter table page_trans
add column is_trans_enabled bool not null default 1 after id_trans;

# add new language constants

insert into language_const (const_code)
VALUES ('translation-missing-title'),
       ('translation-missing');

# add new translations

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Извините, перевод отсутствует'
  from languages, language_const
  where lang_code='ru' and const_code='translation-missing-title';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Sorry, translation missing'
  from languages, language_const
  where lang_code='en' and const_code='translation-missing-title';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Перевод этой страницы на выбранном вами языке отсутствует &mdash; показан перевод на другом языке.'
  from languages, language_const
  where lang_code='ru' and const_code='translation-missing';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Page translation in selected language is not available &mdash; another translation shown.'
  from languages, language_const
  where lang_code='en' and const_code='translation-missing';


