# BRANCH: USER


# new settings

INSERT INTO `settings` (`rid`, `skey`, `value`, `description`, `is_hidden`)
VALUES
  (14, 'anti_brute_tries', '3', 'число попыток ввода пароля до временной блокировки', 0),
  (15, 'anti_brute_time', '600', 'время блокировки аккаунта при неудачных попытках ввода пароля (в секундах)', 0);
