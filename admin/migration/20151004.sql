# branch admintranslate

# add new language constants to admin styles

insert into language_const (const_code)
VALUES
  ('new-style'),
  ('code'),
  ('type'),
  ('confirm-style-remove'),
  ('link'),
  ('error-style-code-exists'),
  ('error-smth-not-specified'),
  ('link-to-css'),
  ('link-to-css-help'),
  ('or'),
  ('style-text'),
  ('style-text-help');

# add new translations for admin styles

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-style' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новый стиль' as trans_value
                union all
                select 'en', 'New style'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'code' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Код' as trans_value
                union all
                select 'en', 'Code'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'type' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Тип' as trans_value
                union all
                select 'en', 'Type'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-style-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Удалить этот стиль?' as trans_value
                union all
                select 'en', 'Remove this style?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'link' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Ссылка' as trans_value
                union all
                select 'en', 'Link'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-style-code-exists' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Этот код стиля уже существует' as trans_value
                union all
                select 'en', 'This style code is already exists'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-smth-not-specified' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'аргумент не определён' as trans_value
                union all
                select 'en', 'argument not specified'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'link-to-css' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Ссылка на файл CSS' as trans_value
                union all
                select 'en', 'Link to CSS file'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'link-to-css-help' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Ссылка имеет приоритет: если это поле заполнено, то ссылка используется вместо текста стиля.' as trans_value
                union all
                select 'en', 'Link has a priority: if this field is not empty, then link is used instead of style text.'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'or' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'или' as trans_value
                union all
                select 'en', 'or'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'style-text' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Текст стиля' as trans_value
                union all
                select 'en', 'Style text'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'style-text-help' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Опишите используемые классы. Теги &lt;style&gt; не нужны. Ссылка должна быть пустой для использования текста.' as trans_value
                union all
                select 'en', 'Declare your classes. Tags &lt;style&gt; are not required. Style link have to be empty to use this text.'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;




# add new language constants to admin templates


insert into language_const (const_code)
VALUES
  ('new-template'),
  ('confirm-template-remove'),
  ('error-template-code-exists'),
  ('choose'),
  ('several'),
  ('template-constants-help');

# add new translations for admin templates

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-template' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новый шаблон' as trans_value
                union all
                select 'en', 'New template'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-template-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Удалить этот шаблон?' as trans_value
                union all
                select 'en', 'Remove this template?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-template-code-exists' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Шаблон с таким кодом уже существует' as trans_value
                union all
                select 'en', 'Template with this code is already exists'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'choose' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Выберите' as trans_value
                union all
                select 'en', 'Choose'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'several' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Несколько' as trans_value
                union all
                select 'en', 'Several'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'template-constants-help' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Доступно использование переменных шаблонов' as trans_value
                union all
                select 'en', 'Use can use template snippets'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


# add new language constants for admin / themselves

insert into language_const (const_code)
VALUES
  ('new-language'),
  ('flag'),
  ('enabled'),
  ('domestic'),
  ('international'),
  ('protected'),
  ('confirm-language-remove'),
  ('error-lang-code-exists'),
  ('error-lang-const-code-exists');

insert into language_const (const_code)
VALUES
  ('error-language-protected'),
  ('flag-help');

insert into language_const (const_code)
VALUES
  ('new-lang-const'),
  ('confirm-lang-const-remove');


# add translations for admin / themeselves

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-language' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новый язык' as trans_value
                union all
                select 'en', 'New language'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'flag' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Флаг' as trans_value
                union all
                select 'en', 'Flag'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'enabled' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Включено' as trans_value
                union all
                select 'en', 'Enabled'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'domestic' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Локальный' as trans_value
                union all
                select 'en', 'Domestic'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'international' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Международный' as trans_value
                union all
                select 'en', 'International'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'protected' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Защита' as trans_value
                union all
                select 'en', 'Protected'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-lang-code-exists' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Язык с таким кодом уже существует' as trans_value
                union all
                select 'en', 'Language with this code is already exists'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-lang-const-code-exists' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Языковая константа с таким кодом уже существует' as trans_value
                union all
                select 'en', 'Language constant with this code is already exists'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-language-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Удалить этот язык?' as trans_value
                union all
                select 'en', 'Remove this language?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

# ---------------------------------------------


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-language-protected' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Вы не можете редактировать защищённый язык' as trans_value
                union all
                select 'en', 'You can not edit protected language'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'flag-help' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Укажите относительный путь к маленькой картинке флага, например /images/flags/gb.gif' as trans_value
                union all
                select 'en', 'Specify the relative path to small flag image, like /images/flags/gb.gif'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


# ----------------------------------------------


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-lang-const' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новая константа' as trans_value
                union all
                select 'en', 'New constant'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-lang-const-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Удалить эту константу?' as trans_value
                union all
                select 'en', 'Remove this constant?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;



# new language constants for admin settings

insert into language_const (const_code)
VALUES
  ('value'),
  ('error-setting-hidden');

# new translations for admin settings

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'value' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Значение' as trans_value
                union all
                select 'en', 'Value'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-setting-hidden' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Вы не можете редактировать скрытую настройку.' as trans_value
                union all
                select 'en', 'You can not edit hidden setting.'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


# new language constants for uploads

insert into language_const (const_code)
VALUES
  ('select-files'),
  ('upload'),
  ('insert-thumbnail');

# new translations for uploads

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'select-files' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Выбрать файлы' as trans_value
                union all
                select 'en', 'Select files'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'upload' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Загрузить' as trans_value
                union all
                select 'en', 'Upload'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'insert-thumbnail' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Вставлять как эскиз' as trans_value
                union all
                select 'en', 'Insert as thumbnail'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


# new constants for admin users

insert into language_const (const_code)
VALUES
  ('admin'),
  ('admin-rights'),
  ('new-user'),
  ('confirm-user-remove'),
  ('confirm-change-permissions');

# new translations for admin users

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'admin' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Администратор' as trans_value
                union all
                select 'en', 'Administrator'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'admin-rights' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Права администратора' as trans_value
                union all
                select 'en', 'Administrator rights'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-user' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новый пользователь' as trans_value
                union all
                select 'en', 'New user'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-user-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Удалить этого пользователя?' as trans_value
                union all
                select 'en', 'Remove this user?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-change-permissions' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Изменить права доступа этому пользователю?' as trans_value
                union all
                select 'en', 'Change permissions for this user?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


