# feedback

# new constant
INSERT INTO language_const (const_code)
  SELECT 'feedback-link';

# translations
INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT id_lang, id_const, 'Обратная связь'
  FROM languages, language_const
  WHERE lang_code='ru' AND const_code='feedback-link';

INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT id_lang, id_const, 'Feedback'
  FROM languages, language_const
  WHERE lang_code='en' AND const_code='feedback-link';


# new constant
INSERT INTO language_const (const_code)
  SELECT 'message-from';

# translations
INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT id_lang, id_const, 'Сообщение от'
  FROM languages, language_const
  WHERE lang_code='ru' AND const_code='message-from';

INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT id_lang, id_const, 'Message from'
  FROM languages, language_const
  WHERE lang_code='en' AND const_code='message-from';
