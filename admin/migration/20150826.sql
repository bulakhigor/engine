# issue 61

# new page property

ALTER TABLE `pages`
ADD COLUMN  `override_active_link_id` int(10) unsigned DEFAULT NULL,
ADD CONSTRAINT `tomenu` FOREIGN KEY (`override_active_link_id`) REFERENCES `menu` (`id_link`) ON DELETE SET NULL ON UPDATE CASCADE;