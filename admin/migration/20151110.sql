# add new settings

# use raw page editor (ace), bool, default is false

INSERT INTO `settings` (`skey`, `value`, `description`, `is_hidden`, `type_id`)
SELECT 'use_raw_page_editor', '0', 'Использовать редактор страниц без визуализации', 0, settings_types.id_type
FROM settings_types
WHERE settings_types.type_code='bool';
