# BRANCH LANG


# list of languages

CREATE TABLE `languages` (
  `id_lang` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(2) NOT NULL DEFAULT '',
  `lang_name` varchar(20) NOT NULL DEFAULT '',
  `lang_flag` varchar(255) NOT NULL DEFAULT '',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_domestic` tinyint(1) NOT NULL DEFAULT '0',
  `is_international` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lang`),
  UNIQUE KEY `lang_code` (`lang_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# special setting for branch CHINA

INSERT INTO `languages` (`id_lang`, `lang_code`, `lang_name`, `lang_flag`, `is_enabled`, `is_protected`, `is_domestic`, `is_international`)
VALUES
  (1, 'ru', 'Русский', '/images/flags/ru.gif', 0, 1, 0, 0),
  (2, 'en', 'English', '/images/flags/gb.gif', 0, 1, 0, 0),
  (3, 'cn', '简体', '/images/flags/cn.gif', 1, 0, 1, 0),
  (4, 'tw', '繁體', '/images/flags/tw.gif', 1, 0, 0, 1);


# list of language constants

CREATE TABLE `language_const` (
  `id_const` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `const_code` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_const`),
  UNIQUE KEY `const_code` (`const_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# move language constants from old table of translations

INSERT INTO language_const (const_code)
  SELECT lang_code
  FROM language;

# list of language constants translations

CREATE TABLE `language_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(10) unsigned NOT NULL,
  `const_id` int(10) unsigned NOT NULL,
  `trans_value` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_trans`),
  UNIQUE KEY `lang_id` (`lang_id`,`const_id`),
  KEY `toconst` (`const_id`),
  CONSTRAINT `toconst` FOREIGN KEY (`const_id`) REFERENCES `language_const` (`id_const`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tolang` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# move language translations of 'ru'

INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT languages.id_lang, id_const, ru
  FROM language, language_const, languages
  WHERE languages.lang_code='ru' and language_const.const_code = language.lang_code;

# move language translations of 'en'

INSERT INTO language_trans (lang_id, const_id, trans_value)
SELECT languages.id_lang, id_const, en
FROM language, language_const, languages
WHERE languages.lang_code='en' and language_const.const_code = language.lang_code;

# move language translations of 'cn' for branch CHINA

INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT languages.id_lang, id_const, cn
  FROM language, language_const, languages
  WHERE languages.lang_code='cn' and language_const.const_code = language.lang_code;

# move language translations of 'tw' for branch CHINA

INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT languages.id_lang, id_const, tw
  FROM language, language_const, languages
  WHERE languages.lang_code='tw' and language_const.const_code = language.lang_code;


# remove old table

DROP TABLE language;

# new table of page translations

CREATE TABLE `page_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `page_title_short` varchar(150) NOT NULL DEFAULT '',
  `page_title_long` varchar(255) NOT NULL DEFAULT '',
  `page_html` longtext NOT NULL,
  `page_keywords` varchar(255) NOT NULL DEFAULT '',
  `page_description` varchar(255) NOT NULL DEFAULT '',
  `page_affix` longtext NOT NULL,
  PRIMARY KEY (`id_trans`),
  KEY `topage2` (`page_id`),
  KEY `tolang2` (`lang_id`),
  CONSTRAINT `tolang2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `topage2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id_page`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

# move page content in cn for branch CHINA

insert into page_trans (page_id, lang_id, page_title_short, page_title_long, page_html, page_keywords, page_description, page_affix)
  SELECT id_page, id_lang, page_title_short_cn, page_title_long_cn, page_html_cn, page_keywords_cn, page_description_cn, page_affix
  FROM pages, languages
  WHERE languages.lang_code='cn';

# move page content in cn for branch CHINA

insert into page_trans (page_id, lang_id, page_title_short, page_title_long, page_html, page_keywords, page_description, page_affix)
  SELECT id_page, id_lang, page_title_short_tw, page_title_long_tw, page_html_tw, page_keywords_tw, page_description_tw, page_affix
  FROM pages, languages
  WHERE languages.lang_code='tw';


# remove old fields from table pages for branch CHINA

ALTER TABLE pages
DROP COLUMN page_title_short_cn;

ALTER TABLE pages
DROP COLUMN page_title_short_tw;

ALTER TABLE pages
DROP COLUMN page_title_long_cn;

ALTER TABLE pages
DROP COLUMN page_title_long_tw;

ALTER TABLE pages
DROP COLUMN page_keywords_cn;

ALTER TABLE pages
DROP COLUMN page_keywords_tw;

ALTER TABLE pages
DROP COLUMN page_description_cn;

ALTER TABLE pages
DROP COLUMN page_description_tw;

ALTER TABLE pages
DROP COLUMN page_html_cn;

ALTER TABLE pages
DROP COLUMN page_html_tw;

ALTER TABLE pages
DROP COLUMN page_affix;
