# branch admintranslate

# update admin style

UPDATE `styles`
SET style_text = 'body {\r\n    background-image: none !important;\r\n}\r\n\r\n.modal\r\n{\r\n    z-index: 65537;\r\n}\r\n\r\n#filemanager #uploads .uploads-period\r\n{\r\n    margin-right: 20px;\r\n}\r\n\r\n#filemanager #uploads .uploads-period .uploads-period-icon\r\n{\r\n    font-size: 100px;\r\n    cursor: pointer;\r\n}\r\n\r\n#filemanager #uploads .loadmore\r\n{\r\n    clear: both;\r\n    margin-bottom: 25px;\r\n}\r\n\r\n#filemanager #uploads .uploads-period.active, #filemanager #uploads .uploads-terminator\r\n{\r\n    margin-right: 0px;\r\n    clear: both;\r\n}\r\n\r\n.sidebar {\r\n    background-color: #003D66;\r\n    text-align: center;\r\n    color: white;\r\n    height: 2000px;\r\n}\r\n\r\n.sidebar .btn-link\r\n{\r\n    color: white;\r\n}\r\n\r\n.sidebar > p > a > img{\r\n    width: 100px;\r\n    height: auto;\r\n    margin: 10px;\r\n}\r\n\r\n@media (min-width: 768px) {\r\n    nav.navbar {\r\n        display: none;\r\n    }\r\n}\r\n\r\n.panel-admin{\r\n	min-height: 300px;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb > img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\na.robin-thumb > span.upload-killer {\r\n    display: none; \r\n    position: absolute; \r\n    right: 20px; \r\n    top: 20px; \r\n    padding: 15px; \r\n    background-color: rgba(0,0,0,0.25); \r\n    color: white; \r\n    border-radius: 50%;\r\n}\r\n\r\na.robin-thumb:hover > span.upload-killer {\r\n    display: inline-block !important;\r\n}'
WHERE style_code = 'admin';

# update admin template

UPDATE `templates`
SET template_html = '<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n\r\n    <%styles%>\r\n\r\n</head>\r\n<body>\r\n<nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"/admin/\">Engine <small><%version%></small></a>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                <%lang_selector%>\r\n                <%admin_menu%>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"hidden-xs col-sm-3 col-md-3 sidebar\">\r\n            <%lang_selector_button%>\r\n            <p><a href=\'/admin/\'><img src=\"/images/engine/logo_transparent_white_600.png\"></a></p>\r\n            <div class=\"list-group\">\r\n                <%admin_menu_list%>\r\n            </div>\r\n            <p>Engine <%version%></p>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-9 col-md-9\">\r\n            <p class=\'hidden-xs\'> </p>\r\n            <%contains%>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<%scripts%>\r\n</body>\r\n</html>'
WHERE template_code = 'admin';

# add menu new language constants for admin menu

insert into language_const (const_code)
VALUES
  ('pages'),
  ('menu'),
  ('styles'),
  ('templates'),
  ('languages'),
  ('lang-consts'),
  ('settings'),
  ('images'),
  ('users');

# add new translations for admin menu

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Страницы'
  from languages, language_const
  where lang_code='ru' and const_code='pages';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Pages'
  from languages, language_const
  where lang_code='en' and const_code='pages';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Меню'
  from languages, language_const
  where lang_code='ru' and const_code='menu';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Menu'
  from languages, language_const
  where lang_code='en' and const_code='menu';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Стили'
  from languages, language_const
  where lang_code='ru' and const_code='styles';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Styles'
  from languages, language_const
  where lang_code='en' and const_code='styles';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Шаблоны'
  from languages, language_const
  where lang_code='ru' and const_code='templates';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Templates'
  from languages, language_const
  where lang_code='en' and const_code='templates';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Языки'
  from languages, language_const
  where lang_code='ru' and const_code='languages';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Languages'
  from languages, language_const
  where lang_code='en' and const_code='languages';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Языковые константы'
  from languages, language_const
  where lang_code='ru' and const_code='lang-consts';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Language constants'
  from languages, language_const
  where lang_code='en' and const_code='lang-consts';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Настройки'
  from languages, language_const
  where lang_code='ru' and const_code='settings';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Settings'
  from languages, language_const
  where lang_code='en' and const_code='settings';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Изображения'
  from languages, language_const
  where lang_code='ru' and const_code='images';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Images'
  from languages, language_const
  where lang_code='en' and const_code='images';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Пользователи'
  from languages, language_const
  where lang_code='ru' and const_code='users';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Users'
  from languages, language_const
  where lang_code='en' and const_code='users';



