# BRANCH: STYLES


# styles table

CREATE TABLE `styles` (
  `id_style` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_code` varchar(50) NOT NULL,
  `style_description` varchar(255) DEFAULT NULL,
  `style_link` varchar(255) DEFAULT NULL,
  `style_text` longtext,
  PRIMARY KEY (`id_style`),
  UNIQUE KEY `style_code` (`style_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# move setting 'custom_style' as a style with code 'common' and id=1

INSERT INTO `styles` (`id_style`, `style_code`, `style_description`, `style_link`, `style_text`)
SELECT 1, 'common', 'Стандартный стиль страницы', NULL, value
FROM settings
WHERE settings.skey = 'custom_style';

# admin style with code 'admin' and id=2

INSERT INTO `styles` (`id_style`, `style_code`, `style_description`, `style_link`, `style_text`)
VALUES
  (2, 'admin', 'Админ-Центр', NULL, 'body {\r\n            background-image: none !important;\r\n        }\r\n\r\n        .modal\r\n        {\r\n            z-index: 65537;\r\n        }\r\n\r\n        .sidebar {\r\n            background-color: #003D66;\r\n            height: 100%;\r\n            display: none;\r\n            text-align: center;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            .sidebar {\r\n                height: 1300px;\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .sidebar > p > a > img{\r\n            width: 100px;\r\n            height: auto;\r\n            margin: 10px;\r\n        }\r\n\r\n        .sidebar > h4 > a\r\n        {\r\n            color: white;\r\n        }\r\n\r\n        .sidebar > h4{\r\n            color: #fff;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            nav.navbar {\r\n                display: none;\r\n            }\r\n        }\r\n\r\n	.panel-admin{\r\n		min-height: 300px;\r\n	}\r\n\r\n    a.robin-thumb\r\n    {\r\n    	position: relative;\r\n    }\r\n    \r\n    a.robin-thumb > img\r\n    {\r\n    	border-radius: 5px;\r\n    	margin: 5px;\r\n    	border: 1px solid rgba(0,0,0,0.3);\r\n    }');


# template's styles table

CREATE TABLE `template_styles` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `style_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `template_id` (`template_id`,`style_id`),
  KEY `tostyle` (`style_id`),
  CONSTRAINT `totemplate2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tostyle` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id_style`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# set 'common' (1) style to all templates excluding 'admin' and 'email'

INSERT INTO template_styles (style_id, template_id)
SELECT 1, id_template
FROM templates
WHERE NOT template_code IN ('admin', 'email');

# set 'admin' (2) style to template 'admin'

INSERT INTO template_styles (style_id, template_id)
SELECT 2, id_template
FROM templates
WHERE template_code='admin';

# hide setting 'custom_style' (DEPRECATED)

UPDATE settings
SET is_hidden=1
WHERE skey='custom_style';