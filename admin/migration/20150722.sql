# branch DEV-MASTER
# better settings


# create settings types

CREATE TABLE `settings_types` (
  `id_type` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_code` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_type`),
  UNIQUE KEY `type_code` (`type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# add settings types

INSERT INTO `settings_types` (`id_type`, `type_code`)
VALUES
  (1, 'string'),
  (2, 'int'),
  (3, 'bool'),
  (4, 'float'),
  (5, 'html');


# remove deprecated settings

DELETE FROM settings WHERE skey='custom_style';

# add type column to settings table

ALTER TABLE settings
ADD COLUMN `type_id` int(10) unsigned NOT NULL;

# fill types of settings

# strings

UPDATE settings
SET settings.type_id = (
  SELECT settings_types.id_type
  FROM settings_types
  WHERE settings_types.type_code='string'
)
WHERE settings.skey IN ('email_from', 'index_page', 'site_image', 'captcha_font_file', 'upload_dir', 'email_prefix', 'email_logo', 'email_tech');

# ints

UPDATE settings
SET settings.type_id = (
  SELECT settings_types.id_type
  FROM settings_types
  WHERE settings_types.type_code='int'
)
WHERE settings.skey IN ('thumb_size', 'last_usage_report', 'uploads_show_per_cycle', 'anti_brute_tries', 'anti_brute_time');

# htmls

UPDATE settings
SET settings.type_id = (
  SELECT settings_types.id_type
  FROM settings_types
  WHERE settings_types.type_code='html'
)
WHERE settings.skey IN ('menu_depth');

# bools

UPDATE settings
SET settings.type_id = (
  SELECT settings_types.id_type
  FROM settings_types
  WHERE settings_types.type_code='bool'
)
WHERE settings.skey IN ('use_double_click_menu', 'maintenance_mode');

# failsafe set others to string

UPDATE settings
SET settings.type_id = (
  SELECT settings_types.id_type
  FROM settings_types
  WHERE settings_types.type_code='string'
)
WHERE settings.type_id=0;

# create constraint

ALTER TABLE settings
ADD KEY `totype` (`type_id`),
ADD CONSTRAINT `totype` FOREIGN KEY (`type_id`) REFERENCES `settings_types` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE;
