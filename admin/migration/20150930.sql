# branch admintranslate

# add new language constants for admin page_edit

insert into language_const (const_code)
VALUES
  ('error-page-url-exists'),
  ('error-url-not-specified'),
  ('error-file-or-directory-exists'),
  ('do-not-use'),
  ('override-active-link'),
  ('override-active-link-help'),
  ('use-affix'),
  ('use-affix-help'),
  ('language'),
  ('enable-translation'),
  ('title-short'),
  ('title-long'),
  ('contains'),
  ('save'),
  ('load-more'),
  ('confirm-image-remove');

# add new translations for admin page_edit

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-page-url-exists' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Страница с таким URL уже существует' as trans_value
                union all
                select 'en', 'Page with this URL is already exists'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-url-not-specified' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'URL не указан' as trans_value
                union all
                select 'en', 'URL not specified'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'error-file-or-directory-exists' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Файл или каталог с таким именем уже существует' as trans_value
                union all
                select 'en', 'File or directory already exists'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'do-not-use' as const_code, ttt.*
         from (
                select 'ru' as lang_code, '-не использовать-' as trans_value
                union all
                select 'en', '-do not use-'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'override-active-link' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Переопределение активного пункта меню' as trans_value
                union all
                select 'en', 'Override active menu item'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'override-active-link-help' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Если данная страница не входит в главное меню, то можно установить значение активного пункта меню при её просмотре. Данная настройка имеет приоритет над меню.' as trans_value
                union all
                select 'en', 'If main menu does not include this page, you can set an active menu item for it. This feature has a priority over main menu settings.'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'use-affix' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Использовать оглавление' as trans_value
                union all
                select 'en', 'Use affixed table of contents'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'use-affix-help' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Оглавление создаётся автоматически на основе анализа вложенности заголовков содержимого страницы.' as trans_value
                union all
                select 'en', 'Table of contents will be generated automatically by analysis of text headers hierarchy.'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'language' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Язык' as trans_value
                union all
                select 'en', 'Language'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'enable-translation' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Задействовать этот перевод страницы' as trans_value
                union all
                select 'en', 'Enable this translation'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'title-short' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Заголовок короткий' as trans_value
                union all
                select 'en', 'Short title'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'title-long' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Заголовок длинный' as trans_value
                union all
                select 'en', 'Long title'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'contains' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Содержимое' as trans_value
                union all
                select 'en', 'Content'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'save' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Сохранить' as trans_value
                union all
                select 'en', 'Save'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'load-more' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Загрузить ещё' as trans_value
                union all
                select 'en', 'Load more'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-image-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Удалить это изображение?' as trans_value
                union all
                select 'en', 'Remove this image?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;
