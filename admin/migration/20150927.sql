# branch admintranslate

# add new language constants for admin cp index page

insert into language_const (const_code)
VALUES
  ('self-update'),
  ('self-update-warning'),
  ('branch'),
  ('version'),
  ('commit'),
  ('last-changes'),
  ('hosting'),
  ('host'),
  ('root-path'),
  ('extensions');


# add new translations for admin cp index page

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Обновление'
  from languages, language_const
  where lang_code='ru' and const_code='self-update';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Self update'
  from languages, language_const
  where lang_code='en' and const_code='self-update';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'ВНИМАНИЕ: Это действие может повлечь конфликты, если ваши файлы были изменены. Вы можете отменить это с помощью команды: git merge --abort. Вы уверены, что хотите продолжить?'
  from languages, language_const
  where lang_code='ru' and const_code='self-update-warning';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'WARNING: This action may cause some conflicts if your files were changed. You can undo this action with command: git merge --abort. Are you sure you want to continue?'
  from languages, language_const
  where lang_code='en' and const_code='self-update-warning';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Ветка'
  from languages, language_const
  where lang_code='ru' and const_code='branch';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Branch'
  from languages, language_const
  where lang_code='en' and const_code='branch';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Версия'
  from languages, language_const
  where lang_code='ru' and const_code='version';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Version'
  from languages, language_const
  where lang_code='en' and const_code='version';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Коммит'
  from languages, language_const
  where lang_code='ru' and const_code='commit';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Commit'
  from languages, language_const
  where lang_code='en' and const_code='commit';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Последние изменения'
  from languages, language_const
  where lang_code='ru' and const_code='last-changes';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Last changes'
  from languages, language_const
  where lang_code='en' and const_code='last-changes';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Хостинг'
  from languages, language_const
  where lang_code='ru' and const_code='hosting';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Hosting'
  from languages, language_const
  where lang_code='en' and const_code='hosting';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Хост'
  from languages, language_const
  where lang_code='ru' and const_code='host';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Host'
  from languages, language_const
  where lang_code='en' and const_code='host';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Путь'
  from languages, language_const
  where lang_code='ru' and const_code='root-path';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Root path'
  from languages, language_const
  where lang_code='en' and const_code='root-path';


insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Расширения'
  from languages, language_const
  where lang_code='ru' and const_code='extensions';

insert into language_trans (lang_id, const_id, trans_value)
  select id_lang, id_const, 'Extensions'
  from languages, language_const
  where lang_code='en' and const_code='extensions';

