# branch admintranslate

# add new language constants for admin pages

insert into language_const (const_code)
VALUES
  ('new-page'),
  ('title'),
  ('template'),
  ('confirm-page-remove'),
  ('description'),
  ('keywords');

# add new translations for admin pages

insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'new-page' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Новая страница' as trans_value
                union all
                select 'en', 'New page'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'title' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Заголовок' as trans_value
                union all
                select 'en', 'Title'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'template' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Шаблон' as trans_value
                union all
                select 'en', 'Template'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'confirm-page-remove' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Вы действительно хотите удалить эту страницу?' as trans_value
                union all
                select 'en', 'Are you sure you want to remove this page?'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'description' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Описание' as trans_value
                union all
                select 'en', 'Description'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;


insert into language_trans (const_id, lang_id, trans_value)
  select id_const, id_lang, pre.trans_value
  from (
         select 'keywords' as const_code, ttt.*
         from (
                select 'ru' as lang_code, 'Слова' as trans_value
                union all
                select 'en', 'Keywords'
              ) as ttt
       ) as pre
    inner join language_const on pre.const_code=language_const.const_code
    inner join languages on pre.lang_code=languages.lang_code;
