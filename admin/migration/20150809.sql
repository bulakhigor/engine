# branch search

# new constant
INSERT INTO language_const (const_code)
  SELECT 'search-results-for';

# translations
INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT id_lang, id_const, 'Результаты поиска для'
  FROM languages, language_const
  WHERE lang_code='ru' AND const_code='search-results-for';

INSERT INTO language_trans (lang_id, const_id, trans_value)
  SELECT id_lang, id_const, 'Search results for'
  FROM languages, language_const
  WHERE lang_code='en' AND const_code='search-results-for';
