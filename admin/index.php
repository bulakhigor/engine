<?

use kernel\Output\Scripts;

include("admin_init.php");

$git = new kernel\Git();
$revisions = $git->getLastRevisions();
$version = kernel\Output::VERSION;
$branch = $git->getCurrentBranch();
$commit = $git->getCurrentCommit();
$commit = $commit ? $commit : "FAILED";
$phpversion = phpversion();
$gitversion = $git->getVersion();
$extensions = implode(", ",get_loaded_extensions());
$ext_count = count(get_loaded_extensions());
$changes = implode("<br>", $revisions);
$root = kernel\Path::getRoot();

$html = <<<HTM
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary panel-admin">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Engine
                    </div>
                    <div class="panel-body">
                        <div class="pull-right">
                            <a onclick="return confirm('<#self-update-warning#>');" class="btn btn-warning" href="/selfupdate.php" role="button">
                                <span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> <#self-update#>
                            </a>
                        </div>
                        <p><#branch#>: <strong>{$branch}</strong></p>
                        <p><#version#>: <strong>{$version}</strong></p>
                        <p><#commit#>: <strong><small>{$commit}</small></strong></p>
                        <p><#last-changes#>:</p>
                        <div class="well well-sm">
                            {$changes}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary panel-admin">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-globe" aria-hidden="true"></span> <#hosting#>
                    </div>
                    <div class="panel-body">
                        <p><#host#>: <strong>{$_SERVER['SERVER_NAME']}</strong></p>
                        <p><#root-path#>: <strong><small>{$root}</small></strong></p>
                        <p>PHP <#version#>: <strong>{$phpversion}</strong></p>
                        <p>git <#version#>: <strong>{$gitversion}</strong></p>
                        <p><#extensions#>: <a href="#" onclick="toggle_extensions();">({$ext_count})</a></p>
                        <div id='extensions' class="well well-sm small" style="display: none;">
                            {$extensions}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
HTM;

Scripts::get()->addScript('<script src="/admin/index.js"></script>');
kernel\Output::get()->writeOption('contains', $html)
                    ->render();