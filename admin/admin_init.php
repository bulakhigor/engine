<?

include("../init.php");

// check rights
if (!kernel\User\Current::get()->readIsAdmin()) die("Access denied.");

kernel\Output::get()->writeTemplate(kernel\Template::get()->getAdminCPCode());