<?

use kernel\Output\Scripts;

include("admin_init.php");

function iteration($parent=NULL,$depth=0)
{
    $children = kernel\Menu::get()->readAllByParentId($parent);
    $arr=array();
    foreach($children as $link)
    {
        if ($link->readPageId()) {
            $menuTitle = kernel\Page::get()->readById($link->readPageId())->readTitleShortInUsingLang();
        } else {
            $menuTitle = $link->readTitle();
        }
        $arr[] = array('id_link'=>$link->readId(), 'title'=>str_repeat(kernel\Settings::get()->readValue('menu_depth'),$depth).$menuTitle);
        $arr = array_merge($arr, iteration($link->readId(), $depth+1));
    }
    return $arr;
}

if (kernel\Input::get()->readInt('update'))
{
    if (kernel\Page::get()->isExists(
        kernel\Input::get()->read('page_url'),
        kernel\Input::get()->readInt('id_page')
    )){
        kernel\Output::get()->error("<#error-page-url-exists#>");
    }

    $page = kernel\Page\Item::create()
            ->writeId(kernel\Input::get()->readInt('id_page'))
            ->writeUrl(kernel\Input::get()->read('page_url'))
            ->writeOverrideActiveLinkId(kernel\Input::get()->readIntOrNull('override_active_link_id'))
            ->writeTemplateId(kernel\Input::get()->readInt('template_id'))
            ->writeIsUseAffix(kernel\Input::get()->readBool('is_use_affix'));

    foreach(kernel\Lang::get()->readAll() as $lang) {
        if ($lang->readIsEnabled()) {
            $trans = kernel\Page\Item\Trans::create()
                ->writeIsEnabled(kernel\Input::get()->read('is_trans_enabled')[$lang->readCode()])
                ->writeLangId($lang->readId())
                ->writeTitleShort(kernel\Input::get()->read('page_title_short')[$lang->readCode()])
                ->writeTitleLong(kernel\Input::get()->read('page_title_long')[$lang->readCode()])
                ->writeHtml(kernel\Input::get()->read('page_html')[$lang->readCode()])
                ->writeKeywords(kernel\Input::get()->read('page_keywords')[$lang->readCode()])
                ->writeDescription(kernel\Input::get()->read('page_description')[$lang->readCode()]);
            if ($page->readIsUseAffix()) $trans->cacheAffixFromHtml();
            $page->writeTrans($lang->readCode(), $trans);
        }
    }
    kernel\Page::get()->write($page);
	kernel\Output::get()->redirect('/admin/pages.php');
}

$page = kernel\Page::get()->readById(kernel\Input::get()->readInt('id_page'));

$override_active_link_html = \kernel\Output\Forms::select(array('name'=>'override_active_link_id', 'array'=>array_merge(array(array('id_link'=>NULL,'title'=>'<#do-not-use#>')),iteration()), 'selected'=>$page->readOverrideActiveLinkId(), 'valuefield'=>'id_link', 'captionfield'=>'title', 'class'=>'form-control'));

$templates_html = kernel\Output\Forms::select(array('name'=>'template_id', 'array'=>kernel\Template::get()->readAll() , 'valuefield'=>'readId', 'captionfield'=>'readCode', 'selected' => $page->readTemplateId(), 'class'=>'form-control'));

$is_use_affix = kernel\Output\Forms\Input::create()
                    ->setName('is_use_affix')
                    ->setType('checkbox')
                    ->setValue(1)
                    ->setChecked($page->readIsUseAffix())
                    ->render();

$lang_enabled_array = array();

$html = <<<HTM
	<form action='' method='post' onsubmit='before_submit();'>
		<input type='hidden' name='id_page' value='{$page->readId()}'>
		<input type='hidden' name='update' value='1'>
		<div class='form-group has-feedback'>
			<label class='control-label'>URL</label>
			<input class='form-control' name='page_url' value='{$page->readUrl()}' onchange="check_page_url();">
			<span class="glyphicon form-control-feedback"></span>
			<div class='help-block'></div>
		</div>
		<div class='form-group'>
		    <label><#override-active-link#></label>
		    <p class="help-block"><#override-active-link-help#></p>
		    {$override_active_link_html}
		</div>
		<div class='form-group'>
		    <label><#template#></label>
		    {$templates_html}
		</div>
		<div class='checkbox'>
		    <label>
		        {$is_use_affix} <#use-affix#>
		    </label>
		    <p class="help-block"><#use-affix-help#></p>
		</div>
HTM;

foreach(kernel\Lang::get()->readAll() as $lang)
{
    if ($lang->readIsEnabled())
    {
        $lang_enabled_array[] = $lang->readCode();
        $html .= "<h4 class='text-center'>&nbsp;<br><#language#>: ".$lang->readName()."</h4>";
        $transEnabledCheckbox = \kernel\Output\Forms\Input::create()
                        ->setType('checkbox')
                        ->setName('is_trans_enabled['.$lang->readCode().']')
                        ->setValue(1)
                        ->setChecked($page->readTrans($lang->readCode())->readIsEnabled())
                        ->setOnChange("toggle_trans_enabled(this);")
                        ->render();
        $translationDisplay = $page->readTrans($lang->readCode())->readIsEnabled() ? 'block' : 'none';
        if (kernel\Settings::get()->readValue('use_raw_page_editor'))
        {
            $editor = "<div id='page_html_ace_{$lang->readCode()}'>{$page->readTrans($lang->readCode())->readHtmlForEditor()}</div>
                        <textarea id='page_html_{$lang->readCode()}' name='page_html[{$lang->readCode()}]' style='display: none;'></textarea>";
        } else {
            $editor = "<textarea class='form-control' style='height: 400px;' name='page_html[{$lang->readCode()}]'>{$page->readTrans($lang->readCode())->readHtmlForEditor()}</textarea>";
        }

        $html .= <<<HTM
        <div class="checkbox">
            <label>
                {$transEnabledCheckbox}
                <#enable-translation#>
            </label>
        </div>
        <div id="translation_{$lang->readCode()}" style="display: {$translationDisplay}">
            <div class='form-group'>
                <label><#title-short#></label>
                <input class='form-control' name='page_title_short[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readTitleShort()}'>
            </div>
            <div class='form-group'>
                <label><#title-long#></label>
                <input class='form-control' name='page_title_long[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readTitleLong()}'>
            </div>
            <div class='form-group'>
                <label><#keywords#></label>
                <input class='form-control' name='page_keywords[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readKeywords()}'>
            </div>
            <div class='form-group'>
                <label><#description#></label>
                <input class='form-control' name='page_description[{$lang->readCode()}]' value='{$page->readTrans($lang->readCode())->readDescription()}'>
            </div>
            <div class='form-group'>
                <label><#contains#></label>
                {$editor}
            </div>
		</div>
HTM;
    }
}

$html .= <<<HTM
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>
HTM;


Scripts::get()->addScript('<script type="text/javascript" src="/vendor/moxiecode/plupload/js/plupload.full.min.js"></script>', true);
Scripts::get()->addVar('languages', $lang_enabled_array);
Scripts::get()->addVar('use_raw_page_editor', kernel\Settings::get()->readValue('use_raw_page_editor'));
Scripts::get()->addVar('upload_dir', kernel\Settings::get()->readValue('upload_dir'));
Scripts::get()->addVar('thumb_size_filemanager', kernel\Settings::get()->readValue('thumb_size_filemanager'));
Scripts::get()->addVar('load_more', '<#load-more#>');
Scripts::get()->addVar('confirm_image_remove', '<#confirm-image-remove#>');
Scripts::get()->addScript("<script src='/admin/upload.js'></script>");
Scripts::get()->addScript("<script src='/admin/page_edit.js'></script>");
kernel\Output::get()->writeOption('contains', $html)
                    ->render();