<?

include("admin_init.php");

$lang_headers = "";
foreach(kernel\Lang::get()->readAll() as $entry)
{
    if ($entry->readIsEnabled()) {
        $lang_headers .= "<th>" . $entry->readName() . "</th>";
    }
}
$consts = kernel\Lang\Constant::get()->readAll();

$html = "
		<p><a href='/admin/language_const_edit.php' class='pull-right btn btn-lg btn-primary'><#new-lang-const#></a></p>
		<table class='table'>
			<thead>
				<tr>
					<th>&nbsp;</th>
					<th><#code#></th>
					{$lang_headers}
				</tr>
			</thead><tbody>";
foreach($consts as $const)
{
	$html .= "<tr>
				<td class='text-nowrap'>
					<a href='/admin/language_const_edit.php?id_const=".$const->readId()."'><span class='glyphicon glyphicon-edit'></span></a>
					<a href='/admin/language_const_remove.php?id_const=".$const->readId()."' onclick=\"return confirm('<#confirm-lang-const-remove#>');\"><span class='glyphicon glyphicon-remove'></span></a>
				</td>
				<td class='text-nowrap'>".$const->readCode()."</td>";
    foreach(kernel\Lang::get()->readAll() as $entry) {
        if ($entry->readIsEnabled()) {
            $html .= "<td>" . $const->readTrans($entry->readCode()) . "</td>";
        }
    }
	$html .= "
    		</tr>";
}
$html .= "</tbody></table>";

kernel\Output::get()->writeOption('contains', $html)
                    ->render();