<?

include("admin_init.php");

if (kernel\Lang::get()->isExists($_GET['lang'], 0))
{
    if (kernel\Lang::get()->readByCode($_GET['lang'])->readIsAdminCPAvailable()) {
        setrawcookie('admincp_lang', $_GET['lang'], time() + 86400 * 30, '/');
    }
}
$ref = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : "/admin/";
kernel\Output::get()->redirect($ref);

