<?

use kernel\Output\Scripts;

include("admin_init.php");

if (kernel\Input::get()->readInt('update'))
{
    if (kernel\Template::get()->isExists(
        kernel\Input::get()->read('template_code'),
        kernel\Input::get()->readInt('id_template')
    )) {
        kernel\Output::get()->error("<#error-template-code-exists#>");
    }

    kernel\Template::get()->write(
        kernel\Template\Item::create()
            ->writeId(kernel\Input::get()->readInt('id_template'))
            ->writeCode(kernel\Input::get()->read('template_code'))
            ->writeHtml(kernel\Input::get()->read('template_html'))
            ->writeDescription(kernel\Input::get()->read('template_description'))
            ->writeStyles(array_keys(kernel\Input::get()->readIntArray('styles')))
    );
	kernel\Output::get()->redirect('/admin/templates.php');
}


$template = kernel\Template::get()->readById(kernel\Input::get()->readInt('id_template'));

$styles = $template->readStyles();
$all_styles = kernel\Style::get()->readAll();
$all_styles_html = "";
foreach($all_styles as $style)
{
    $all_styles_html .= "<li><a href='#'><label>".kernel\Output\Forms\Input::create()
                                ->setType('checkbox')
                                ->setName('styles['.$style->readId().']')
                                ->setId('styles_'.$style->readId())
                                ->setValue(1)
                                ->setChecked(in_array($style->readId(), $styles) ? true : false)
                                ->render().
                                "&nbsp;".$style->readCode()."&nbsp;&mdash;&nbsp;".$style->readDescription()."
                        </label></a></li>";
}

if (!count($styles))
{
    $styles_selector_title = "<#choose#>";
} elseif (count($styles)===1) {
    $styles_selector_title = kernel\Style::get()->readById($styles[0])->readCode();
} else {
    $styles_selector_title = "<#several#> (".count($styles).")";
}

$html = <<<HTM
	<form action='' method='post' onsubmit="ace_fetch('template_html', 'template_html_ace');">
		<input type='hidden' name='id_template' value='{$template->readId()}'>
		<input type='hidden' name='update' value='1'>
		<div class='form-group has-feedback'>
			<label class='control-label'><#code#></label>
			<input class='form-control' name='template_code' value='{$template->readCode()}' onchange="check_template_code();">
			<span class="glyphicon form-control-feedback"></span>
			<div class='help-block'></div>
		</div>
		<div class='form-group'>
			<label><#description#></label>
			<input class='form-control' name='template_description' value='{$template->readDescription()}'>
		</div>
		<div class='form-group'>
		    <label><#styles#></label><br>
            <div class="btn-group">
                <button type="button" class="btn btn-default">{$styles_selector_title}</button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    {$all_styles_html}
                </ul>
            </div>
        </div>
		<div class='form-group'>
			<label><#template#></label>
			<p class='help-block'><a href='#' onclick="toggle_options_list();"><#template-constants-help#></a>
			    <span id='options_list' style='display: none;'><%options%></span>
			</p>
			<textarea style='display: none;' id='template_html' name='template_html'></textarea>
			<div id='template_html_ace'>{$template->readHtmlForEditor()}</div>
		</div>
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>
HTM;
Scripts::get()->addScript("<script src='/admin/template_edit.js'></script>");
kernel\Output::get()->writeOption('contains', $html)
                    ->render();