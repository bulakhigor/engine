-- MySQL dump 10.13  Distrib 5.1.62, for unknown-freebsd8.3 (i386)
--
-- Host: mositong.mysql    Database: mositong_db
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `captcha`
--

DROP TABLE IF EXISTS `captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `captcha` (
  `id_captcha` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_create_unix` int(11) NOT NULL,
  `code` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_captcha`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captcha`
--

LOCK TABLES `captcha` WRITE;
/*!40000 ALTER TABLE `captcha` DISABLE KEYS */;
INSERT INTO `captcha` VALUES (1,1434105610,'066991'),(2,1434132279,'418822'),(3,1434132432,'084312'),(4,1435438005,'017908'),(5,1435438006,'249224'),(6,1435508015,'233020'),(7,1436129937,'143584'),(8,1436269002,'838755'),(9,1436269017,'354993'),(10,1436269041,'391773'),(11,1436275763,'672737'),(12,1436304329,'034371'),(13,1436403760,'529480'),(14,1436439708,'511592'),(15,1436440459,'328070'),(16,1436461531,'776733'),(17,1436462189,'991745'),(18,1436468223,'255225'),(19,1437229459,'308772');
/*!40000 ALTER TABLE `captcha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_const`
--

DROP TABLE IF EXISTS `language_const`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_const` (
  `id_const` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `const_code` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_const`),
  UNIQUE KEY `const_code` (`const_code`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_const`
--

LOCK TABLES `language_const` WRITE;
/*!40000 ALTER TABLE `language_const` DISABLE KEYS */;
INSERT INTO `language_const` VALUES (1,'admin-cp'),(2,'auth-error'),(3,'brand'),(4,'cancel'),(5,'captcha-enter'),(6,'captcha-error'),(7,'captcha-help'),(8,'change-password'),(9,'email'),(10,'email-sent'),(11,'error'),(12,'error-email-empty'),(13,'error-message'),(14,'error-message-empty'),(15,'error-name-empty'),(16,'id-and-key-error'),(17,'key-error'),(18,'login'),(19,'logout'),(20,'message'),(21,'name'),(22,'new-password'),(23,'password'),(24,'password-changed'),(25,'password-changed-email'),(26,'password-length-error'),(27,'password-really-changed'),(28,'password-recovery'),(29,'quick-edit'),(30,'recover-password'),(31,'recovery-email-link'),(32,'recovery-email-notice'),(33,'recovery-email-sent'),(34,'recovery-form-notice'),(35,'recovery-key'),(36,'recovery-request'),(37,'recovery-start-info'),(38,'redirect'),(39,'redirect-help'),(40,'register'),(41,'register-email'),(42,'register-error'),(43,'remember-your-password'),(44,'remove'),(45,'send-me-recovery'),(46,'set-new-password'),(47,'user-not-found-error'),(48,'your-id'),(49,'your-key');
/*!40000 ALTER TABLE `language_const` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_trans`
--

DROP TABLE IF EXISTS `language_trans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(10) unsigned NOT NULL,
  `const_id` int(10) unsigned NOT NULL,
  `trans_value` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_trans`),
  UNIQUE KEY `lang_id` (`lang_id`,`const_id`),
  KEY `toconst` (`const_id`),
  CONSTRAINT `toconst` FOREIGN KEY (`const_id`) REFERENCES `language_const` (`id_const`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tolang` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_trans`
--

LOCK TABLES `language_trans` WRITE;
/*!40000 ALTER TABLE `language_trans` DISABLE KEYS */;
INSERT INTO `language_trans` VALUES (1,1,3,'[�����]'),(2,1,8,'������� ������'),(3,1,19,'�����'),(4,1,18,'����'),(5,1,9,'EMail'),(6,1,23,'������'),(7,1,30,'� �� ����� ������'),(8,1,38,'���������������'),(9,1,39,'������� ����, ���� ������ �� ����������'),(10,1,5,'������� ��� ������������'),(11,1,7,'��� �������������� ����� ���������� ������ �������� ���, ������������ �� ��������, ����� �����������, ��� �� ��������� ���������, � �� �������.'),(12,1,20,'���������'),(13,1,2,'������ �����������'),(14,1,16,'ID � ���� ������ ���� �������'),(15,1,26,'������ ������ ���� �� ������ 6 ��������'),(16,1,6,'��� ������������ ������ �����������'),(17,1,47,'������������ �� ������'),(18,1,11,'������'),(19,1,1,'����� �����'),(20,1,13,'��������� �� ������'),(21,1,29,'���������'),(22,1,44,'�������'),(23,1,21,'���'),(24,1,4,'������'),(25,1,41,'<p>�� ���������� ��� �� �����������. �� ��������� � ����� ������������������. ���� �� ���� �������������� �� email ������ � ��������� ���� ������. ������ ������ ����� ������������ ��� ��������.</p><p>����������� ���������� ��� ���������� ������� �� �����. � ����� ������������ ������������ �������� �����, � ������� ������� �� ������� ������ ������ ��� ���� � ����� ��������, � ����� ������ �������� ������� �������.</p>'),(26,1,40,'�����������'),(27,1,42,'������ �����������'),(28,1,10,'������ ����������'),(29,1,33,'������ � ����������� ��� �������������� ������ ���� ������� ���������� �� ������� �����. ��������� ������������� �� ����� ������������ ���'),(30,1,32,'���� ��������� ����������� �������������� �������� ������ �� ������ email. ���� �� �� ����������� �������������� ������, ������ �������������� � ������� ��� ������. ���� �� ������������� ��������� �������������� ������, �������� ��� ��������� ��� ���������� ���� ��������.'),(31,1,35,'����'),(32,1,31,'�������� �������� - ���� ���� ������������ �� ����� ������������ ���. ����� ������������ ������, �������� �� ��������� ������ � ������� ��������� ��������� � �����.'),(33,1,28,'�������������� ������'),(34,1,34,'�� ������ ���� �������� ������ � ��������� ���������� �������������� ������. � ���� ������ ������� ��� ID � ����. ������� ���������� ��� ������, � ����� ����� ������, ������� �� ������ ��������� ����� ������� ������, ������� ������ ���� �� ������ 6 ��������.'),(35,1,48,'��� ID'),(36,1,49,'��� ����'),(37,1,22,'����� ������'),(38,1,46,'������ ����� ������'),(39,1,37,'���� �� ������ ���� ������, �� ������ ��������������� ���� ������ ��� ��������������. ����� ������������ ������, ������� ���������� ���� email. ����� �������� ���� �����, ������� ������ ������ �� ��� ������� email ����� � ����������� ��� ������� ������ ������ ����� ������� ������. ���������� ��� �������������� ������ ������������� �� ����� ������������ ���.'),(40,1,45,'������� ������ �� �����'),(41,1,24,'������ � ����� ������� ������ ��� ������� �������.'),(42,1,43,'��������� ��� ������, ��������� ����������� ����� �� ����.'),(43,1,25,'������ �������'),(44,1,36,'������ �� �������������� ������'),(45,1,27,'������ ��� ������� �������. ������������ ������ ���������� �� ��� ������� �����.'),(46,1,17,'���� �������'),(47,1,14,'�� ������ �� ��������'),(48,1,15,'�� �� ����� ���'),(49,1,12,'�� �� ����� email'),(64,2,3,'[BRAND]'),(65,2,8,'Change password'),(66,2,19,'Logout'),(67,2,18,'Login'),(68,2,9,'EMail'),(69,2,23,'Password'),(70,2,30,'Recover password'),(71,2,38,'Redirecting'),(72,2,39,'Click here if nothing happens'),(73,2,5,'Enter the security code'),(74,2,7,'To prevent spam you have to enter numbers from this picture to make us sure you are not a robot.'),(75,2,20,'Message'),(76,2,2,'Authentication error'),(77,2,16,'ID and Key have to be entered'),(78,2,26,'Password have to be 6 or more chars of length'),(79,2,6,'Security code is invalid'),(80,2,47,'User not found'),(81,2,11,'Error'),(82,2,1,'Admin CP'),(83,2,13,'Error message'),(84,2,29,'Edit'),(85,2,44,'Remove'),(86,2,21,'Name'),(87,2,4,'Cancel'),(88,2,41,'<p>Thank you for registering. We take care of your privacy. Sign in is implemented with email address and password you entered. Password can always be recovered or changed.</p><p>Authorization is required to perform orders on the website. There is a personal address book, with which you can place orders for you and your friends, and also to give useful gifts.</p>'),(89,2,40,'Registration'),(90,2,42,'Registration error'),(91,2,10,'Email sent'),(92,2,33,'The email with password recovery information sent. It is valid until the day ends.'),(93,2,32,'There was requested the ability to recover a forgotten password for your email. If you are not requested for password recovery, just ignore and delete this email. If you really asked for password recovery, we send you details to complete this operation.'),(94,2,35,'Key'),(95,2,31,'Please note that this key is valid until the end of the day. To recover the password, go to the following link and enter the required details in the form.'),(96,2,28,'Password recovery'),(97,2,34,'You should have received a email with details of password recovery. In this email are indicated your ID and Key. Please enter the data, and the new password you want to assign to your account, which must be at least 6 characters.'),(98,2,48,'Account ID'),(99,2,49,'Account Key'),(100,2,22,'NEW password'),(101,2,46,'Set new password'),(102,2,37,'If you have forgotten your password, you can use this form to recover. To recover your password, please enter your email.\nAfter submitting this form, the system will send an email with instructions to set a new password for your account. Instructions to reset your password are valid until the end of the day.'),(103,2,45,'Recover my password'),(104,2,24,'Your account password has been changed.'),(105,2,43,'Please remember your password.'),(106,2,25,'Password changed'),(107,2,36,'Password recovery request'),(108,2,27,'Account password changed. Notification to your email sent.'),(109,2,17,'Account key is invalid'),(110,2,14,'You did not write anything'),(111,2,15,'You did not enter your name'),(112,2,12,'You did not enter your email'),(127,3,3,'????'),(128,3,8,'????'),(129,3,19,'??'),(130,3,18,'??'),(131,3,9,'?EMail'),(132,3,23,'??'),(133,3,30,'??????'),(134,3,38,'??'),(135,3,39,'?????????????'),(136,3,5,'?????'),(137,3,7,'????????????????????????????????????????'),(138,3,20,'??'),(139,3,2,'????'),(140,3,16,'ID???????'),(141,3,26,'??????6???'),(142,3,6,'????????'),(143,3,47,'?????'),(144,3,11,'??'),(145,3,1,'Admin page'),(146,3,13,'????'),(147,3,29,'??'),(148,3,44,'??'),(149,3,21,'??'),(150,3,4,'??'),(151,3,41,'<p>??????????????????????????????????????????????</p><p>??????????????????????????????????????????</p>'),(152,3,40,'??'),(153,3,42,'????'),(154,3,10,'??'),(155,3,33,'???????????????????????????????????'),(156,3,32,'??????????????????????????????????????????????????????????????????????????'),(157,3,35,'??'),(158,3,31,'?? - ???????????????????????????????????????????????'),(159,3,28,'????'),(160,3,34,'?????????????????????????ID?????????????????????????????6????'),(161,3,48,'??ID'),(162,3,49,'????'),(163,3,22,'???'),(164,3,46,'????????'),(165,3,37,'?????????????????????????????????????????????????????????????????????????????????????????????'),(166,3,45,'???????'),(167,3,24,'??????????????'),(168,3,43,'???????????????????'),(169,3,25,'?????'),(170,3,36,'??????'),(171,3,27,'??????????????????????'),(172,3,17,'??????'),(173,3,14,'?????????'),(174,3,15,'?????????'),(175,3,12,'?????????'),(190,4,3,'????'),(191,4,8,'????'),(192,4,19,'??'),(193,4,18,'??'),(194,4,9,'?EMail'),(195,4,23,'??'),(196,4,30,'??????'),(197,4,38,'??'),(198,4,39,'?????????????'),(199,4,5,'?????'),(200,4,7,'????????????????????????????????????????'),(201,4,20,'??'),(202,4,2,'????'),(203,4,16,'ID???????'),(204,4,26,'??????6???'),(205,4,6,'????????'),(206,4,47,'?????'),(207,4,11,'??'),(208,4,1,'Admin page'),(209,4,13,'????'),(210,4,29,'??'),(211,4,44,'??'),(212,4,21,'??'),(213,4,4,'??'),(214,4,41,'<p>??????????????????????????????????????????????</p> <p>??????????????????????????????????????????</p>'),(215,4,40,'??'),(216,4,42,'????'),(217,4,10,'??'),(218,4,33,'???????????????????????????????????'),(219,4,32,'??????????????????????????????????????????????????????????????????????????'),(220,4,35,'??'),(221,4,31,'?? - ???????????????????????????????????????????????'),(222,4,28,'????'),(223,4,34,'?????????????????????????ID?????????????????????????????6????'),(224,4,48,'??ID'),(225,4,49,'????'),(226,4,22,'???'),(227,4,46,'????????'),(228,4,37,'?????????????????????????????????????????????????????????????????????????????????????????????'),(229,4,45,'???????'),(230,4,24,'??????????????'),(231,4,43,'???????????????????'),(232,4,25,'?????'),(233,4,36,'??????'),(234,4,27,'??????????????????????'),(235,4,17,'??????'),(236,4,14,'?????????'),(237,4,15,'?????????'),(238,4,12,'?????????');
/*!40000 ALTER TABLE `language_trans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id_lang` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(2) NOT NULL DEFAULT '',
  `lang_name` varchar(20) NOT NULL DEFAULT '',
  `lang_flag` varchar(255) NOT NULL DEFAULT '',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_domestic` tinyint(1) NOT NULL DEFAULT '0',
  `is_international` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lang`),
  UNIQUE KEY `lang_code` (`lang_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'ru','�������','/images/flags/ru.gif',0,1,0,0),(2,'en','English','/images/flags/gb.gif',0,1,0,0),(3,'cn','??','/images/flags/cn.gif',1,0,1,0),(4,'tw','??','/images/flags/tw.gif',1,0,0,1);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id_link` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_link_id` int(10) unsigned DEFAULT NULL,
  `link_page_id` int(10) unsigned DEFAULT NULL,
  `link_title_short` varchar(50) DEFAULT NULL,
  `link_ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_link`),
  KEY `toparent` (`parent_link_id`),
  KEY `topage` (`link_page_id`),
  CONSTRAINT `topage` FOREIGN KEY (`link_page_id`) REFERENCES `pages` (`id_page`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `toparent` FOREIGN KEY (`parent_link_id`) REFERENCES `menu` (`id_link`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (8,NULL,1,NULL,0),(20,NULL,6,NULL,0),(23,NULL,11,NULL,0),(33,23,13,NULL,1),(34,23,14,NULL,2),(35,23,15,NULL,3),(37,20,17,NULL,1),(38,20,18,NULL,2),(39,NULL,17,NULL,0);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `id_migration` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `migration_timestamp` int(11) NOT NULL,
  `executed_file` varchar(255) NOT NULL DEFAULT '',
  `executed_at` int(11) NOT NULL,
  `snapshot_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_migration`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES (1,20150708,'20150708.sql',1436609559,'1436609558.sql'),(2,20150714,'20150714.sql',1436991258,'1436991257.sql'),(3,20150716,'20150716.sql',1437327023,'1437327021.sql'),(4,20150720,'20150720.sql',1437424879,'1437424878.sql');
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_trans`
--

DROP TABLE IF EXISTS `page_trans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `page_title_short` varchar(150) NOT NULL DEFAULT '',
  `page_title_long` varchar(255) NOT NULL DEFAULT '',
  `page_html` longtext NOT NULL,
  `page_keywords` varchar(255) NOT NULL DEFAULT '',
  `page_description` varchar(255) NOT NULL DEFAULT '',
  `page_affix` longtext NOT NULL,
  PRIMARY KEY (`id_trans`),
  KEY `topage2` (`page_id`),
  KEY `tolang2` (`lang_id`),
  CONSTRAINT `tolang2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `topage2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id_page`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_trans`
--

LOCK TABLES `page_trans` WRITE;
/*!40000 ALTER TABLE `page_trans` DISABLE KEYS */;
INSERT INTO `page_trans` VALUES (8,1,3,'??','??','<p><span><a href=\"test_baidu\">Sed</a> ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>\r\n<p><a href=\"rest\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"138\" height=\"264\" /></a></p>\r\n<p>qwe�</p>','??????????????????????','??? - ????????????',''),(9,6,3,'??','??','<p>???????????????????????</p>\r\n<p><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" alt=\"\" width=\"157\" height=\"117\" /></p>','??','???????????',''),(10,7,3,'??','???????','<p><strong>????</strong>???<strong>????</strong>?<strong>????</strong>?<strong>??</strong>??<a class=\"new\" title=\"?????????\" href=\"https://zh.wikipedia.org/w/index.php?title=%E6%8D%95%E6%BC%81&action=edit&redlink=1\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AD%9A%E9%A1%9E\">??</a>????????????????????????????????????</p>\r\n<p>?????????????????????????????????????????????????<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%8A%E9%AD%9A\">??</a>???????????<a title=\"??????\" href=\"https://zh.wikipedia.org/wiki/%E6%B5%B7%E6%B4%8B%E7%94%9F%E6%85%8B%E7%B3%BB%E7%B5%B1\">??????</a>?</p>\r\n<p>???????????????????????????????????????????????????????????????????????????????????<a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%B1%92%E9%AD%9A\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%89%E9%AD%9A\">??</a>?????????????????????????????</p>\r\n<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a></p>','????','',''),(11,8,3,'VIP','VIP','<p><img src=\"images/upload/o_19q17lqcufqvsbu1401hd71fqvh.jpg\" alt=\"\" width=\"153\" height=\"109\" /></p>\r\n<p>�</p>\r\n<p style=\"text-align: right;\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"156\" height=\"299\" /></p>','VIP','???????',''),(13,11,3,'????','????','<h4><a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></h4>\r\n<p>�</p>','????','????????????',''),(14,12,3,'???????','???????','','???????','?????????????????',''),(20,18,3,'??','??','<h3><span style=\"background-color: #cccccc;\">????</span> �<span style=\"background-color: #cccccc;\">????</span> �<span style=\"background-color: #cccccc;\">????</span></h3>\r\n<hr />\r\n<p>�</p>\r\n<p>�</p>','????','?????????',''),(23,1,4,'??','??','<p><span><a href=\"test_baidu\">But</a> I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</span></p>\r\n<p><a href=\"rest\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"102\" height=\"195\" /></a></p>','??????????????????????','??? ? ????????????',''),(24,6,4,'??','??','<p><span>??????????????????????</span></p>\r\n<p><span><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" alt=\"\" width=\"157\" height=\"117\" /></span></p>','??','???????????',''),(25,7,4,'??','???????','<p>?????????????????????<a class=\"new\" title=\"?????????\" href=\"https://zh.wikipedia.org/w/index.php?title=%E6%8D%95%E6%BC%81&action=edit&redlink=1\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AD%9A%E9%A1%9E\">??</a>????????????????????????????????????</p>\r\n<p>?????????????????????????????????????????????????<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%8A%E9%AD%9A\">??</a>???????????<a title=\"??????\" href=\"https://zh.wikipedia.org/wiki/%E6%B5%B7%E6%B4%8B%E7%94%9F%E6%85%8B%E7%B3%BB%E7%B5%B1\">??????</a>?</p>\r\n<p>???????????????????????????????????????????????????????????????????????????????????<a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%B1%92%E9%AD%9A\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%89%E9%AD%9A\">??</a>?????????????????????????????</p>\r\n<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a></p>','????','',''),(26,8,4,'VIP','VIP','<p><img src=\"images/upload/o_19q17lqcufqvsbu1401hd71fqvh.jpg\" alt=\"\" width=\"153\" height=\"109\" /></p>\r\n<p style=\"padding-left: 30px; text-align: right;\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"156\" height=\"299\" /></p>','VIP','???????',''),(28,11,4,'????','????','<h4>�<a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>��<a title=\"??\" href=\"rest/view\">??</a></h4>','????','????????????',''),(29,12,4,'???????','???????','','??????','?????? ????? ??????',''),(35,18,4,'??','??','<h3><span style=\"background-color: #cccccc;\">????</span> �<span style=\"background-color: #cccccc;\">????</span> �<span style=\"background-color: #cccccc;\">????</span></h3>\r\n<hr />\r\n<p>�</p>\r\n<p>�</p>','????','?????????',''),(38,9,3,'','','<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg\" alt=\"\" /></a><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q17kvu240ue5s1j9nhpt10lb9.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q17kvu240ue5s1j9nhpt10lb9.jpg.thumb.jpg\" alt=\"\" /></a></p>','','',''),(39,9,4,'','','','','',''),(40,17,3,'????','????','<p><span style=\"color: #000000;\"><span style=\"background-color: #cccccc;\">??</span> �<span style=\"background-color: #cccccc;\">??</span> �<span style=\"background-color: #cccccc;\">?????</span> �<span style=\"background-color: #cccccc;\">????????????</span>� <span style=\"background-color: #cccccc;\">????</span></span></p>\r\n<hr />\r\n<p><img style=\"border-width: 2px;\" src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" alt=\"??\" width=\"244\" height=\"182\" /></p>\r\n<p><!-- pagebreak --></p>\r\n<p>�</p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>?????????????????????????????�<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�?????????????????????????????????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??????????????????????????????????????????????????????????????????????<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????????????????????<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>\r\n<p>�</p>\r\n<p>�</p>','?????????????????????????????????????????????????????????','??????????',''),(41,17,4,'????','????','<p><span style=\"color: #000000;\"><span style=\"background-color: #cccccc;\">??</span> �<span style=\"background-color: #cccccc;\">??</span> �<span style=\"background-color: #cccccc;\">?????</span> �<span style=\"background-color: #cccccc;\">????????????</span> �<span style=\"background-color: #cccccc;\">????</span></span></p>\r\n<hr />\r\n<p><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" alt=\"??\" width=\"273\" height=\"203\" /></p>\r\n<p><!-- pagebreak --></p>\r\n<p>�</p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>?????????????????????????????�<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�?????????????????????????????????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??????????????????????????????????????????????????????????????????????<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????????????????????<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>','?????????????????????????????????????????????????????????','??????????',''),(46,13,3,'??','??','<h4><a title=\"??\" href=\"zhong\">??</a> �<a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></h4>','??','??',''),(47,13,4,'??','??','<h4><a title=\"??\" href=\"zhong\">??</a> �<a title=\"??\" href=\"rest/russ\">??</a>��<a title=\"??\" href=\"rest/view\">??</a></h4>','??','??',''),(48,15,3,'??','??','<h4>�</h4>','??','??',''),(49,15,4,'??','??','<h4>�</h4>','??','??',''),(50,14,3,'???','???','','???','????????????????�???',''),(51,14,4,'???','???','','???','???????????????????',''),(52,16,3,'??','??','<p><strong>??</strong><span>???</span><strong>??</strong><span>?</span><strong>??</strong><span>???</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E4%BA%BA%E9%A1%9E\">??</a><span>??</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%8B%95%E7%89%A9\">??</a><span>???</span><a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%A3%9F%E7%89%A9\">??</a><span>???????????????????????????????????</span></p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-cn/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>???????????�????????????????��<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�????????????�????????????�???????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??�?????????????????????????�??????????????????????????????�?????????�<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-cn/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????�??????????????�<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-cn/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>\r\n<p>�</p>\r\n<hr />\r\n<p><span style=\"text-decoration: underline;\"><strong>????</strong></span><strong>� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �</strong></p>\r\n<p>????????????????????????????????????????????�<strong> � �</strong></p>\r\n<p><br />�� � ��</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\"><br />�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>','','???????',''),(53,16,4,'??','??','<p><strong>??</strong><span>???</span><strong>??</strong><span>?</span><strong>??</strong><span>???</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E4%BA%BA%E9%A1%9E\">??</a><span>??</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%8B%95%E7%89%A9\">??</a><span>???</span><a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%A3%9F%E7%89%A9\">??</a><span>???????????????????????????????????</span></p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>?????????????????????????????�<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�?????????????????????????????????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??????????????????????????????????????????????????????????????????????<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????????????????????<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>','','???????','');
/*!40000 ALTER TABLE `page_trans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id_page` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_url` varchar(50) NOT NULL DEFAULT '',
  `template_id` int(10) unsigned NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_page`),
  UNIQUE KEY `page_url` (`page_url`),
  KEY `totemplate` (`template_id`),
  CONSTRAINT `totemplate` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'index',7),(6,'lvyou',7),(7,'buyu',7),(8,'VIP',7),(9,'test_baidu',15),(11,'food',7),(12,'clubs',7),(13,'restaurant',7),(14,'cafe',7),(15,'bar',7),(16,'hunt',7),(17,'activities',7),(18,'yee',7);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `skey` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`),
  UNIQUE KEY `key` (`skey`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'thumb_size','200','������ ������� ������',0),(2,'email_from','no@no.no','�� ���� ���������� �����',0),(3,'index_page','index','URL ������� ��������',0),(4,'site_image','','����������� ��������',0),(5,'captcha_font_file','images/captcha/digits2.png','���� � �������� � �������',0),(6,'custom_style','body\r\n{\r\n	background-image: url(http://mositong.com/images/design/background.jpg);\r\n	background-repeat: no-repeat;\r\n	\r\n	background-position: top center;\r\n        padding-top: 20px;\r\n}\r\n.je_header_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			vertical-align: top;\r\n}\r\n.je_footer_block {\r\n			margin-bottom: 30px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 42px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			color: #fff;\r\n}\r\n.je_footer_block&gt;p&gt;a {\r\n			color: #fff;\r\n			text-decoration: underline;\r\n		}\r\n.je_weather_block {\r\n			margin-bottom: 10px;\r\n			padding: 10px;\r\n			padding-top: 18px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n			text-align: center;\r\n}\r\nli {\r\n list-style-type: none;\r\n}\r\n.je_main_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n}\r\n.je_main_block &gt; .nav-pills&gt;li.active&gt;a, .nav-pills&gt;li.active&gt;a:focus, .nav-pills&gt;li.active&gt;a:hover {\r\n			background-color: #ca0d11;\r\n			color: #fff;\r\n}\r\n.wh {\r\ncolor:#fff;\r\n}\r\n.je_main_block &gt; .nav &gt; li &gt; a {\r\n			color: #ca0d11;\r\n}\r\na:hover {\r\n			color:red !important;\r\n}\r\n.mce-fullscreen\r\n{\r\n	padding-top: 60px !important;\r\n}\r\n\r\n.modal\r\n{\r\n	z-index: 65537;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb &gt; img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\n/* affix and scrollspy */\r\n\r\n.affix\r\n{\r\n   top: 30px;\r\n}\r\n\r\n.page-affix-dropdown-btn\r\n{\r\n    position: fixed;\r\n    left: 20px;\r\n    top: 65px;\r\n    z-index: 2;\r\n}\r\n\r\n.page-affix-dropdown-btn &gt; .nav\r\n{\r\n    max-height: 65vh;\r\n    width: 65vw;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.page-affix-dropdown-btn .nav &gt; li &gt; a\r\n{\r\n    padding: 5px 10px !important;\r\n    color: #262626;\r\n}\r\n\r\n#page-affix .nav .nav\r\n{\r\n    display: none;\r\n}\r\n\r\n#page-affix &gt; .nav &gt; li &gt; .nav\r\n{\r\n    font-size: smaller;\r\n}\r\n\r\n#page-affix &gt; .nav .nav &gt; li &gt; a, .page-affix-dropdown-btn .nav .nav &gt; li &gt; a\r\n{\r\n    padding-left: 25px !important;\r\n}\r\n\r\n#page-affix &gt; .nav .nav .nav &gt; li &gt; a, .page-affix-dropdown-btn .nav .nav .nav &gt; li &gt; a\r\n{\r\n    padding-left: 50px !important;\r\n}\r\n\r\n\r\n#page-affix li &gt; a\r\n{\r\n    border-left: 2px solid transparent;\r\n    padding: 3px;\r\n}\r\n\r\n#page-affix li.active &gt; a\r\n{\r\n    border-left: 2px solid black;\r\n}\r\n\r\n#page-affix li.active &gt; .nav\r\n{\r\n    display: block;\r\n}\r\n\r\nh1 &gt; a[id]:not([href]), h2 &gt; a[id]:not([href]), h3 &gt; a[id]:not([href]), h4 &gt; a[id]:not([href])\r\n{\r\n    top: -130px;\r\n    position: relative;\r\n    display: block;\r\n}\r\n@media screen and (min-width: 992px) {\r\n    .je_weather_block {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 992px) {\r\n    .je_weather_block {\r\n  display: none !important;\r\n  }\r\n}\r\n@media screen and (min-width: 360px) {\r\n    .lang {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 360px) {\r\n    .lang {\r\n  position:absolute; !important;\r\n  top: 90px; !important;\r\n  left: 130px; !important;\r\n  }\r\n}','����������� �����',1),(7,'last_usage_report','1437536073','��������� ����������� �� �������������',1),(8,'upload_dir','images/upload','������� ��� �������� �����������',0),(9,'menu_depth','&nbsp;&nbsp;&nbsp;','����������� ����������� ���� ����� ������� ������',0),(10,'uploads_show_per_cycle','20','������� ���������� ������� �� ���',0),(11,'email_prefix','NN','������� �����',0),(12,'email_logo','images/logo.png','���� � ������',0),(13,'email_tech','j.e.morrow@yandex.ru','����� ������������ ��������������',0),(14,'anti_brute_tries','3','����� ������� ����� ������ �� ��������� ����������',0),(15,'anti_brute_time','600','����� ���������� �������� ��� ��������� �������� ����� ������ (� ��������)',0),(16,'use_double_click_menu','1','�������� ������������ � �������� ����������� ����',0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id_style` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_code` varchar(50) NOT NULL,
  `style_description` varchar(255) DEFAULT NULL,
  `style_link` varchar(255) DEFAULT NULL,
  `style_text` longtext,
  PRIMARY KEY (`id_style`),
  UNIQUE KEY `style_code` (`style_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'common','����������� ����� ��������','','body\r\n{\r\n	background-image: url(http://mositong.com/images/design/background.jpg);\r\n	background-repeat: no-repeat;\r\n	\r\n	background-position: top center;\r\n        padding-top: 20px;\r\n}\r\n\r\n.je_header_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			vertical-align: top;\r\n}\r\n\r\n.je_footer_block {\r\n			margin-bottom: 30px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 42px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			color: #fff;\r\n}\r\n\r\n.je_footer_block > p > a {\r\n			color: #fff;\r\n			text-decoration: underline;\r\n		}\r\n		\r\n.je_weather_block {\r\n			margin-bottom: 10px;\r\n			padding: 10px;\r\n			padding-top: 18px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n			text-align: center;\r\n}\r\n\r\nli {\r\n list-style-type: none;\r\n}\r\n\r\n.je_main_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n}\r\n\r\n.je_main_block > .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {\r\n			background-color: #ca0d11;\r\n			color: #fff;\r\n}\r\n\r\n.wh {\r\n    color:#fff;\r\n}\r\n\r\n.je_main_block > .nav > li > a {\r\n			color: #ca0d11;\r\n}\r\n\r\na:hover {\r\n			color:red !important;\r\n}\r\n\r\n.mce-fullscreen\r\n{\r\n	padding-top: 60px !important;\r\n}\r\n\r\n.modal\r\n{\r\n	z-index: 65537;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb > img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\n/* affix and scrollspy */\r\n\r\n.affix\r\n{\r\n   top: 30px;\r\n}\r\n\r\n.page-affix-dropdown-btn\r\n{\r\n    position: fixed;\r\n    left: 20px;\r\n    top: 65px;\r\n    z-index: 2;\r\n}\r\n\r\n.page-affix-dropdown-btn > .nav\r\n{\r\n    max-height: 65vh;\r\n    width: 65vw;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.page-affix-dropdown-btn .nav > li > a\r\n{\r\n    padding: 5px 10px !important;\r\n    color: #262626;\r\n}\r\n\r\n#page-affix .nav .nav\r\n{\r\n    display: none;\r\n}\r\n\r\n#page-affix > .nav > li > .nav\r\n{\r\n    font-size: smaller;\r\n}\r\n\r\n#page-affix > .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav > li > a\r\n{\r\n    padding-left: 25px !important;\r\n}\r\n\r\n#page-affix > .nav .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav .nav > li > a\r\n{\r\n    padding-left: 50px !important;\r\n}\r\n\r\n\r\n#page-affix li > a\r\n{\r\n    border-left: 2px solid transparent;\r\n    padding: 3px;\r\n}\r\n\r\n#page-affix li.active > a\r\n{\r\n    border-left: 2px solid black;\r\n}\r\n\r\n#page-affix li.active > .nav\r\n{\r\n    display: block;\r\n}\r\n\r\nh1 > a[id]:not([href]), h2 > a[id]:not([href]), h3 > a[id]:not([href]), h4 > a[id]:not([href])\r\n{\r\n    top: -130px;\r\n    position: relative;\r\n    display: block;\r\n}\r\n@media screen and (min-width: 992px) {\r\n    .je_weather_block {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 992px) {\r\n    .je_weather_block {\r\n  display: none !important;\r\n  }\r\n}\r\n@media screen and (min-width: 360px) {\r\n    .lang {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 360px) {\r\n    .lang {\r\n  position:absolute; !important;\r\n  top: 90px; !important;\r\n  left: 130px; !important;\r\n  }\r\n}'),(2,'admin','�����-�����','','body {\r\n            background-image: none !important;\r\n        }\r\n\r\n        .modal\r\n        {\r\n            z-index: 65537;\r\n        }\r\n\r\n        .sidebar {\r\n            background-color: #003D66;\r\n            height: 100%;\r\n            display: none;\r\n            text-align: center;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            .sidebar {\r\n                height: 1300px;\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .sidebar > p > a > img{\r\n            width: 100px;\r\n            height: auto;\r\n            margin: 10px;\r\n        }\r\n\r\n        .sidebar > h4 > a\r\n        {\r\n            color: white;\r\n        }\r\n\r\n        .sidebar > h4{\r\n            color: #fff;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            nav.navbar {\r\n                display: none;\r\n            }\r\n        }\r\n\r\n	.panel-admin{\r\n		min-height: 300px;\r\n	}\r\n\r\n\r\n    a.robin-thumb\r\n    {\r\n    	position: relative;\r\n    }\r\n    \r\n    a.robin-thumb > img\r\n    {\r\n    	border-radius: 5px;\r\n    	margin: 5px;\r\n    	border: 1px solid rgba(0,0,0,0.3);\r\n    }');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_styles`
--

DROP TABLE IF EXISTS `template_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_styles` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `style_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `template_id` (`template_id`,`style_id`),
  KEY `tostyle` (`style_id`),
  CONSTRAINT `totemplate2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tostyle` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id_style`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_styles`
--

LOCK TABLES `template_styles` WRITE;
/*!40000 ALTER TABLE `template_styles` DISABLE KEYS */;
INSERT INTO `template_styles` VALUES (13,7,1),(2,8,1),(4,11,2),(11,15,1);
/*!40000 ALTER TABLE `template_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id_template` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(50) NOT NULL DEFAULT '',
  `template_html` text NOT NULL,
  `template_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_template`),
  UNIQUE KEY `template_code` (`template_code`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (3,'email','<html>\r\n <head>\r\n  <meta content=\'text/html; charset=utf-8\' http-equiv=\'Content-Type\'>\r\n </head>\r\n <body style=\'background-color: #eeeeee; font-face: sans-serif; padding-top: 40px;\' alink=\'#000099\' link=\'#000099\' text=\'#000000\' vlink=\'#000099\'>\r\n   <table align=\'center\' bgcolor=\'#ffffff\' border=\'0\' cellpadding=\'5\' cellspacing=\'0\' width=\'80%\'>\r\n    <tbody>\r\n     <tr>\r\n       <td style=\'padding-left: 50px; padding-top: 20px; padding-bottom: 20px;\' valign=\'middle\'><h3 style=\'margin: 0px;\'><%title%></h3></td>\r\n       <td style=\'padding-right: 50px; padding-top: 20px; padding-bottom: 20px;\' align=\'right\' valign=\'top\'>\r\n       <img alt=\'<%prefix%>\' src=\'<%logo%>\' border=\'0\' height=\'100px\'></td>\r\n     </tr><tr>\r\n      <td style=\'border-top: 1px solid #cccccc; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' valign=\'top\'>\r\n        <%content%>\r\n      </td>\r\n    </tr><tr>\r\n      <td style=\'background-color: #666666; color: white; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' nowrap=\'nowrap\' valign=\'top\'>\r\n    <span style=\'font-weight: bold; margin-right: 50px;\'><#brand#></span>\r\n   <span style=\'color: silver; margin-right: 50px;\'><#slogan#></span>\r\n    <a style=\'color: silver;\' href=\'http://<%domain%>\'><%domain%></a>\r\n       </td>\r\n     </tr>\r\n   </tbody>\r\n </table>\r\n <br>\r\n <div align=\'center\'><font color=\'#666666\' size=\'-2\'><a style=\'color: #666666;\' href=\'http://<%domain%>/login/recoveryform.php\'>� �� ����� ������</a>.</font></div>\r\n</body></html>','����������� ������'),(7,'page','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','��������'),(8,'page_backup','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <!-- Bootstrap -->\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <link href=\"/vendor/twbs/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n  <!-- custom -->\r\n<style>\r\n  <%custom_style%>\r\n</style>\r\n\r\n\r\n    <link rel=\"stylesheet\" href=\"/lytebox/lytebox.css\" type=\"text/css\" media=\"screen\">\r\n\r\n    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->\r\n    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->\r\n    <!--[if lt IE 9]>\r\n      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\r\n      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>\r\n    <![endif]-->\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','��������'),(11,'admin','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n\r\n    <%styles%>\r\n\r\n    <link rel=\"stylesheet\" href=\"/lytebox/lytebox.css\" type=\"text/css\" media=\"screen\">\r\n\r\n</head>\r\n<body>\r\n<nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"/admin/\">Engine <small><%version%></small></a>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                <%admin_menu%>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-3 col-md-2 sidebar\">\r\n            <p><a href=\'/admin/\'><img src=\"/images/design/engine_logo.png\"></a></p>\r\n            <h4><a href=\'/admin/\'>Engine <small><%version%></small></a></h4>\r\n            <div class=\"list-group\">\r\n                <%admin_menu_list%>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-9 col-md-10\"><br>\r\n            <%contains%>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<%scripts%>\r\n</body>\r\n</html>','�����-�����'),(15,'test_baidu','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n    <script type=\"text/javascript\">\r\n        var cse;\r\n        \r\n        //??????????\r\n        function display (data) {\r\n            console.log(data);\r\n        }\r\n    \r\n        //?????????\r\n        function init () {\r\n            cse = new BCse.Search(\"7099775790379624042\");    //?????API??ID??????????\r\n            \r\n            cse.getResult(\"???\", display);    //????????????1???????2??????????????????\r\n        }\r\n    \r\n        function loadScript () { \r\n            var script = document.createElement(\"script\"); \r\n            script.type = \"text/javascript\";\r\n            script.charset = \"utf-8\";\r\n            script.src = \"http://zhannei.baidu.com/api/customsearch/apiaccept?sid=7099775790379624042&v=2.0&callback=init\";\r\n            var s = document.getElementsByTagName(\'script\')[0];\r\n            s.parentNode.insertBefore(script, s);\r\n        }\r\n    \r\n        loadScript();\r\n    </script>\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','');
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id_upload` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_upload`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (1,'o_19q1644vt1cig1uug12mr5uf1up59.jpg','o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg',4,1436706555),(2,'o_19q164dub13n1gl01jjs1ug9p8qe.jpg','o_19q164dub13n1gl01jjs1ug9p8qe.jpg.thumb.jpg',4,1436706556),(3,'o_19q17kvu240ue5s1j9nhpt10lb9.jpg','o_19q17kvu240ue5s1j9nhpt10lb9.jpg.thumb.jpg',4,1436708146),(4,'o_19q17lqcufqvsbu1401hd71fqvh.jpg','o_19q17lqcufqvsbu1401hd71fqvh.jpg.thumb.jpg',4,1436708173),(5,'o_19q189l5s1f0r1ars1io7174tsag9.jpg','o_19q189l5s1f0r1ars1io7174tsag9.jpg.thumb.jpg',4,1436708823),(6,'o_19q3ugg9k1gsd104a1td417o2nch9.png','o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg',3,1436799233);
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_name` varchar(255) NOT NULL DEFAULT '',
  `tries` int(11) NOT NULL DEFAULT '0',
  `date_last_try` int(11) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `lang_code` varchar(2) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'1ab4f0253ac67be0d7ffca1cfcb424e8','robin_tail@me.com','robin',0,0,1,'cn'),(3,'ead09a0f7accc3ee1d3874aa5f92a3a7','j.e.morrow@yandex.ru','galahed',0,0,1,'tw'),(4,'37e531f32473be47b911d5f10d16ad04','info@mositong.com','Mositong',0,0,1,'tw');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-22 21:46:21
