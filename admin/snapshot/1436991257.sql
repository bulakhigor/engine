-- MySQL dump 10.13  Distrib 5.1.62, for unknown-freebsd8.3 (i386)
--
-- Host: mositong.mysql    Database: mositong_db
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `captcha`
--

DROP TABLE IF EXISTS `captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `captcha` (
  `id_captcha` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_create_unix` int(11) NOT NULL,
  `code` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_captcha`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captcha`
--

LOCK TABLES `captcha` WRITE;
/*!40000 ALTER TABLE `captcha` DISABLE KEYS */;
INSERT INTO `captcha` VALUES (1,1434105610,'066991'),(2,1434132279,'418822'),(3,1434132432,'084312'),(4,1435438005,'017908'),(5,1435438006,'249224'),(6,1435508015,'233020'),(7,1436129937,'143584'),(8,1436269002,'838755'),(9,1436269017,'354993'),(10,1436269041,'391773'),(11,1436275763,'672737'),(12,1436304329,'034371'),(13,1436403760,'529480'),(14,1436439708,'511592'),(15,1436440459,'328070'),(16,1436461531,'776733'),(17,1436462189,'991745'),(18,1436468223,'255225');
/*!40000 ALTER TABLE `captcha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id_lang` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(50) NOT NULL DEFAULT '',
  `ru` varchar(500) NOT NULL DEFAULT '',
  `cn` varchar(500) NOT NULL,
  `tw` varchar(500) NOT NULL,
  `en` varchar(500) NOT NULL,
  `lang_group` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_lang`),
  UNIQUE KEY `code` (`lang_code`)
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'brand','[�����]','????','????','[BRAND]','global'),(13,'change-password','������� ������','????','????','Change password','global'),(14,'logout','�����','??','??','Logout','global'),(17,'login','����','??','??','Login','global'),(18,'email','EMail','?EMail','?EMail','EMail','global'),(19,'password','������','??','??','Password','global'),(20,'recover-password','� �� ����� ������','??????','??????','Recover password','global'),(21,'redirect','���������������','??','??','Redirecting','global'),(22,'redirect-help','������� ����, ���� ������ �� ����������','?????????????','?????????????','Click here if nothing happens','global'),(23,'captcha-enter','������� ��� ������������','?????','?????','Enter the security code','global'),(24,'captcha-help','��� �������������� ����� ���������� ������ �������� ���, ������������ �� ��������, ����� �����������, ��� �� ��������� ���������, � �� �������.','????????????????????????????????????????','????????????????????????????????????????','To prevent spam you have to enter numbers from this picture to make us sure you are not a robot.','global'),(173,'message','���������','??','??','Message','global'),(187,'auth-error','������ �����������','????','????','Authentication error','global'),(188,'id-and-key-error','ID � ���� ������ ���� �������','ID???????','ID???????','ID and Key have to be entered','global'),(189,'password-length-error','������ ������ ���� �� ������ 6 ��������','??????6???','??????6???','Password have to be 6 or more chars of length','global'),(190,'captcha-error','��� ������������ ������ �����������','????????','????????','Security code is invalid','global'),(191,'user-not-found-error','������������ �� ������','?????','?????','User not found','global'),(194,'error','������','??','??','Error','global'),(195,'admin-cp','����� �����','Admin page','Admin page','Admin CP',NULL),(201,'error-message','��������� �� ������','????','????','Error message','global'),(207,'quick-edit','���������','??','??','Edit','global'),(209,'remove','�������','??','??','Remove','global'),(211,'name','���','??','??','Name','global'),(226,'cancel','������','??','??','Cancel','global'),(269,'register-email','<p>�� ���������� ��� �� �����������. �� ��������� � ����� ������������������. ���� �� ���� �������������� �� email ������ � ��������� ���� ������. ������ ������ ����� ������������ ��� ��������.</p><p>����������� ���������� ��� ���������� ������� �� �����. � ����� ������������ ������������ �������� �����, � ������� ������� �� ������� ������ ������ ��� ���� � ����� ��������, � ����� ������ �������� ������� �������.</p>','<p>??????????????????????????????????????????????</p><p>??????????????????????????????????????????</p>','<p>??????????????????????????????????????????????</p> <p>??????????????????????????????????????????</p>','<p>Thank you for registering. We take care of your privacy. Sign in is implemented with email address and password you entered. Password can always be recovered or changed.</p><p>Authorization is required to perform orders on the website. There is a personal address book, with which you can place orders for you and your friends, and also to give useful gifts.</p>','global'),(270,'register','�����������','??','??','Registration','global'),(271,'register-error','������ �����������','????','????','Registration error','global'),(272,'email-sent','������ ����������','??','??','Email sent','global'),(273,'recovery-email-sent','������ � ����������� ��� �������������� ������ ���� ������� ���������� �� ������� �����. ��������� ������������� �� ����� ������������ ���','???????????????????????????????????','???????????????????????????????????','The email with password recovery information sent. It is valid until the day ends.','global'),(274,'recovery-email-notice','���� ��������� ����������� �������������� �������� ������ �� ������ email. ���� �� �� ����������� �������������� ������, ������ �������������� � ������� ��� ������. ���� �� ������������� ��������� �������������� ������, �������� ��� ��������� ��� ���������� ���� ��������.','??????????????????????????????????????????????????????????????????????????','??????????????????????????????????????????????????????????????????????????','There was requested the ability to recover a forgotten password for your email. If you are not requested for password recovery, just ignore and delete this email. If you really asked for password recovery, we send you details to complete this operation.','global'),(275,'recovery-key','����','??','??','Key','global'),(276,'recovery-email-link','�������� �������� - ���� ���� ������������ �� ����� ������������ ���. ����� ������������ ������, �������� �� ��������� ������ � ������� ��������� ��������� � �����.','?? - ???????????????????????????????????????????????','?? - ???????????????????????????????????????????????','Please note that this key is valid until the end of the day. To recover the password, go to the following link and enter the required details in the form.','global'),(277,'password-recovery','�������������� ������','????','????','Password recovery','global'),(278,'recovery-form-notice','�� ������ ���� �������� ������ � ��������� ���������� �������������� ������. � ���� ������ ������� ��� ID � ����. ������� ���������� ��� ������, � ����� ����� ������, ������� �� ������ ��������� ����� ������� ������, ������� ������ ���� �� ������ 6 ��������.','?????????????????????????ID?????????????????????????????6????','?????????????????????????ID?????????????????????????????6????','You should have received a email with details of password recovery. In this email are indicated your ID and Key. Please enter the data, and the new password you want to assign to your account, which must be at least 6 characters.','global'),(279,'your-id','��� ID','??ID','??ID','Account ID','global'),(280,'your-key','��� ����','????','????','Account Key','global'),(281,'new-password','����� ������','???','???','NEW password','global'),(282,'set-new-password','������ ����� ������','????????','????????','Set new password','global'),(283,'recovery-start-info','���� �� ������ ���� ������, �� ������ ��������������� ���� ������ ��� ��������������. ����� ������������ ������, ������� ���������� ���� email. ����� �������� ���� �����, ������� ������ ������ �� ��� ������� email ����� � ����������� ��� ������� ������ ������ ����� ������� ������. ���������� ��� �������������� ������ ������������� �� ����� ������������ ���.','?????????????????????????????????????????????????????????????????????????????????????????????','?????????????????????????????????????????????????????????????????????????????????????????????','If you have forgotten your password, you can use this form to recover. To recover your password, please enter your email.\nAfter submitting this form, the system will send an email with instructions to set a new password for your account. Instructions to reset your password are valid until the end of the day.','global'),(284,'send-me-recovery','������� ������ �� �����','???????','???????','Recover my password','global'),(285,'password-changed','������ � ����� ������� ������ ��� ������� �������.','??????????????','??????????????','Your account password has been changed.','global'),(286,'remember-your-password','��������� ��� ������, ��������� ����������� ����� �� ����.','???????????????????','???????????????????','Please remember your password.','global'),(287,'password-changed-email','������ �������','?????','?????','Password changed','global'),(288,'recovery-request','������ �� �������������� ������','??????','??????','Password recovery request','global'),(289,'password-really-changed','������ ��� ������� �������. ������������ ������ ���������� �� ��� ������� �����.','??????????????????????','??????????????????????','Account password changed. Notification to your email sent.','global'),(290,'key-error','���� �������','??????','??????','Account key is invalid','global'),(307,'error-message-empty','�� ������ �� ��������','?????????','?????????','You did not write anything','global'),(308,'error-name-empty','�� �� ����� ���','?????????','?????????','You did not enter your name','global'),(309,'error-email-empty','�� �� ����� email','?????????','?????????','You did not enter your email','global');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id_link` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_link_id` int(10) unsigned DEFAULT NULL,
  `link_page_id` int(10) unsigned DEFAULT NULL,
  `link_title_short` varchar(50) DEFAULT NULL,
  `link_ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_link`),
  KEY `toparent` (`parent_link_id`),
  KEY `topage` (`link_page_id`),
  CONSTRAINT `topage` FOREIGN KEY (`link_page_id`) REFERENCES `pages` (`id_page`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `toparent` FOREIGN KEY (`parent_link_id`) REFERENCES `menu` (`id_link`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (8,NULL,1,NULL,0),(12,8,5,NULL,0),(13,NULL,1,NULL,0),(14,NULL,5,NULL,4),(15,14,6,NULL,1),(16,NULL,6,NULL,0),(18,16,7,NULL,0),(20,NULL,6,NULL,0),(21,20,7,NULL,1),(22,NULL,8,NULL,8),(23,NULL,11,NULL,0),(24,NULL,12,NULL,3),(28,NULL,7,NULL,0),(29,NULL,NULL,'fun',0),(30,29,7,NULL,0),(32,NULL,11,NULL,0);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `id_migration` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `migration_timestamp` int(11) NOT NULL,
  `executed_file` varchar(255) NOT NULL DEFAULT '',
  `executed_at` int(11) NOT NULL,
  `snapshot_file` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_migration`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES (1,20150708,'20150708.sql',1436609559,'1436609558.sql');
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id_page` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_title_short_cn` varchar(150) NOT NULL DEFAULT '',
  `page_title_short_tw` varchar(150) NOT NULL,
  `page_title_long_cn` varchar(255) NOT NULL DEFAULT '',
  `page_title_long_tw` varchar(255) NOT NULL,
  `page_html_cn` longtext NOT NULL,
  `page_html_tw` longtext NOT NULL,
  `page_keywords_cn` varchar(255) NOT NULL DEFAULT '',
  `page_keywords_tw` varchar(255) NOT NULL,
  `page_description_cn` varchar(255) NOT NULL DEFAULT '',
  `page_description_tw` varchar(255) NOT NULL,
  `page_url` varchar(50) NOT NULL DEFAULT '',
  `template_id` int(10) unsigned NOT NULL DEFAULT '7',
  `page_affix` longtext NOT NULL,
  PRIMARY KEY (`id_page`),
  UNIQUE KEY `page_url` (`page_url`),
  KEY `totemplate` (`template_id`),
  CONSTRAINT `totemplate` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'??','??','??','??','<p><span><a href=\"test_baidu\">Sed</a> ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</span></p>\r\n<p><a href=\"rest\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"138\" height=\"264\" /></a></p>\r\n<p>qwe�</p>','<p><span><a href=\"test_baidu\">But</a> I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</span></p>\r\n<p><a href=\"rest\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"102\" height=\"195\" /></a></p>','??????????????????????','??????????????????????','??? - ????????????','??? ? ????????????','index',7,''),(5,'Lorem','Lorem','Ipsum','Ipsum','<p>Amet</p>','<p>Amet</p>','Dolor','Dolor','Sit','Sit','tests',7,''),(6,'????','????','????','????','<p>???????????????????????</p>\r\n<p><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" alt=\"\" width=\"157\" height=\"117\" /></p>','<p><span>??????????????????????</span></p>\r\n<p><span><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" alt=\"\" width=\"157\" height=\"117\" /></span></p>','??','??','???????????','???????????','lvyou',7,''),(7,'??','??','???????','???????','<p><strong>????</strong>???<strong>????</strong>?<strong>????</strong>?<strong>??</strong>??<a class=\"new\" title=\"?????????\" href=\"https://zh.wikipedia.org/w/index.php?title=%E6%8D%95%E6%BC%81&action=edit&redlink=1\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AD%9A%E9%A1%9E\">??</a>????????????????????????????????????</p>\r\n<p>?????????????????????????????????????????????????<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%8A%E9%AD%9A\">??</a>???????????<a title=\"??????\" href=\"https://zh.wikipedia.org/wiki/%E6%B5%B7%E6%B4%8B%E7%94%9F%E6%85%8B%E7%B3%BB%E7%B5%B1\">??????</a>?</p>\r\n<p>???????????????????????????????????????????????????????????????????????????????????<a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%B1%92%E9%AD%9A\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%89%E9%AD%9A\">??</a>?????????????????????????????</p>\r\n<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a></p>','<p>?????????????????????<a class=\"new\" title=\"?????????\" href=\"https://zh.wikipedia.org/w/index.php?title=%E6%8D%95%E6%BC%81&action=edit&redlink=1\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AD%9A%E9%A1%9E\">??</a>????????????????????????????????????</p>\r\n<p>?????????????????????????????????????????????????<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%8A%E9%AD%9A\">??</a>???????????<a title=\"??????\" href=\"https://zh.wikipedia.org/wiki/%E6%B5%B7%E6%B4%8B%E7%94%9F%E6%85%8B%E7%B3%BB%E7%B5%B1\">??????</a>?</p>\r\n<p>???????????????????????????????????????????????????????????????????????????????????<a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%B1%92%E9%AD%9A\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%89%E9%AD%9A\">??</a>?????????????????????????????</p>\r\n<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a></p>','????','????','','','buyu',7,''),(8,'VIP','VIP','VIP','VIP','<p><img src=\"images/upload/o_19q17lqcufqvsbu1401hd71fqvh.jpg\" alt=\"\" width=\"153\" height=\"109\" /></p>\r\n<p>�</p>\r\n<p style=\"text-align: right;\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"156\" height=\"299\" /></p>','<p><img src=\"images/upload/o_19q17lqcufqvsbu1401hd71fqvh.jpg\" alt=\"\" width=\"153\" height=\"109\" /></p>\r\n<p style=\"padding-left: 30px; text-align: right;\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"156\" height=\"299\" /></p>','VIP','VIP','???????','???????','VIP',7,''),(9,'','','','','<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg\" alt=\"\" /></a><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q17kvu240ue5s1j9nhpt10lb9.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q17kvu240ue5s1j9nhpt10lb9.jpg.thumb.jpg\" alt=\"\" /></a></p>','','','','','','test_baidu',15,''),(10,'','','','','','','','','','','����',11,''),(11,'?????','?????','?????','?????','<h4><a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></h4>\r\n<p>�</p>','<h4>�<a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>��<a title=\"??\" href=\"rest/view\">??</a></h4>','????','????','????????????','????????????','rest',7,''),(12,'???????','???????','???????','???????','','','???????','??????','?????????????????','?????? ????? ??????','clubs',7,''),(13,'??','??','??','??','<h4><a title=\"??\" href=\"zhong\">??</a> �<a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></h4>','<h4><a title=\"??\" href=\"zhong\">??</a> �<a title=\"??\" href=\"rest/russ\">??</a>��<a title=\"??\" href=\"rest/view\">??</a></h4>','??','??','??','??','rest/zhong',7,''),(14,'??','??','??','??','<p><a title=\"??\" href=\"rest/zhong\">??</a>� <a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></p>','<p><a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a><span>�</span><span> </span><a title=\"??\" href=\"rest/view\">??</a></p>','??','??','????????????????�???','???????????????????','rest/russ',7,''),(15,'??','??','??','??','<h4><a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>� <a href=\"rest/view\">??</a></h4>','<h4><a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>��<a href=\"rest/view\">??</a></h4>','??','??','??','??','rest/view',7,'');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `skey` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`rid`),
  UNIQUE KEY `key` (`skey`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'thumb_size','200','������ ������� ������',0),(2,'email_from','no@no.no','�� ���� ���������� �����',0),(3,'index_page','index','URL ������� ��������',0),(4,'site_image','','����������� ��������',0),(5,'captcha_font_file','images/captcha/digits2.png','���� � �������� � �������',0),(6,'custom_style','body\r\n{\r\n	background-image: url(http://mositong.com/images/design/background.jpg);\r\n	background-repeat: no-repeat;\r\n	\r\n	background-position: top center;\r\n        padding-top: 20px;\r\n}\r\n.je_header_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			vertical-align: top;\r\n}\r\n.je_footer_block {\r\n			margin-bottom: 30px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 42px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			color: #fff;\r\n}\r\n.je_footer_block&gt;p&gt;a {\r\n			color: #fff;\r\n			text-decoration: underline;\r\n		}\r\n.je_weather_block {\r\n			margin-bottom: 10px;\r\n			padding: 10px;\r\n			padding-top: 18px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n			text-align: center;\r\n}\r\nli {\r\n list-style-type: none;\r\n}\r\n.je_main_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n}\r\n.je_main_block &gt; .nav-pills&gt;li.active&gt;a, .nav-pills&gt;li.active&gt;a:focus, .nav-pills&gt;li.active&gt;a:hover {\r\n			background-color: #ca0d11;\r\n			color: #fff;\r\n}\r\n.wh {\r\ncolor:#fff;\r\n}\r\n.je_main_block &gt; .nav &gt; li &gt; a {\r\n			color: #ca0d11;\r\n}\r\na:hover {\r\n			color:red !important;\r\n}\r\n.mce-fullscreen\r\n{\r\n	padding-top: 60px !important;\r\n}\r\n\r\n.modal\r\n{\r\n	z-index: 65537;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb &gt; img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\n/* affix and scrollspy */\r\n\r\n.affix\r\n{\r\n   top: 30px;\r\n}\r\n\r\n.page-affix-dropdown-btn\r\n{\r\n    position: fixed;\r\n    left: 20px;\r\n    top: 65px;\r\n    z-index: 2;\r\n}\r\n\r\n.page-affix-dropdown-btn &gt; .nav\r\n{\r\n    max-height: 65vh;\r\n    width: 65vw;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.page-affix-dropdown-btn .nav &gt; li &gt; a\r\n{\r\n    padding: 5px 10px !important;\r\n    color: #262626;\r\n}\r\n\r\n#page-affix .nav .nav\r\n{\r\n    display: none;\r\n}\r\n\r\n#page-affix &gt; .nav &gt; li &gt; .nav\r\n{\r\n    font-size: smaller;\r\n}\r\n\r\n#page-affix &gt; .nav .nav &gt; li &gt; a, .page-affix-dropdown-btn .nav .nav &gt; li &gt; a\r\n{\r\n    padding-left: 25px !important;\r\n}\r\n\r\n#page-affix &gt; .nav .nav .nav &gt; li &gt; a, .page-affix-dropdown-btn .nav .nav .nav &gt; li &gt; a\r\n{\r\n    padding-left: 50px !important;\r\n}\r\n\r\n\r\n#page-affix li &gt; a\r\n{\r\n    border-left: 2px solid transparent;\r\n    padding: 3px;\r\n}\r\n\r\n#page-affix li.active &gt; a\r\n{\r\n    border-left: 2px solid black;\r\n}\r\n\r\n#page-affix li.active &gt; .nav\r\n{\r\n    display: block;\r\n}\r\n\r\nh1 &gt; a[id]:not([href]), h2 &gt; a[id]:not([href]), h3 &gt; a[id]:not([href]), h4 &gt; a[id]:not([href])\r\n{\r\n    top: -130px;\r\n    position: relative;\r\n    display: block;\r\n}\r\n@media screen and (min-width: 992px) {\r\n    .je_weather_block {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 992px) {\r\n    .je_weather_block {\r\n  display: none !important;\r\n  }\r\n}\r\n@media screen and (min-width: 360px) {\r\n    .lang {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 360px) {\r\n    .lang {\r\n  position:absolute; !important;\r\n  top: 90px; !important;\r\n  left: 130px; !important;\r\n  }\r\n}','����������� �����',1),(7,'last_usage_report','1436943071','��������� ����������� �� �������������',1),(8,'upload_dir','images/upload','������� ��� �������� �����������',0),(9,'menu_depth','&nbsp;&nbsp;&nbsp;','����������� ����������� ���� ����� ������� ������',0),(10,'uploads_show_per_cycle','20','������� ���������� ������� �� ���',0),(11,'email_prefix','NN','������� �����',0),(12,'email_logo','images/logo.png','���� � ������',0),(13,'email_tech','j.e.morrow@yandex.ru','����� ������������ ��������������',0);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id_style` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_code` varchar(50) NOT NULL,
  `style_description` varchar(255) DEFAULT NULL,
  `style_link` varchar(255) DEFAULT NULL,
  `style_text` longtext,
  PRIMARY KEY (`id_style`),
  UNIQUE KEY `style_code` (`style_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'common','����������� ����� ��������','','body\r\n{\r\n	background-image: url(http://mositong.com/images/design/background.jpg);\r\n	background-repeat: no-repeat;\r\n	\r\n	background-position: top center;\r\n        padding-top: 20px;\r\n}\r\n\r\n.je_header_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			vertical-align: top;\r\n}\r\n\r\n.je_footer_block {\r\n			margin-bottom: 30px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 42px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			color: #fff;\r\n}\r\n\r\n.je_footer_block > p > a {\r\n			color: #fff;\r\n			text-decoration: underline;\r\n		}\r\n		\r\n.je_weather_block {\r\n			margin-bottom: 10px;\r\n			padding: 10px;\r\n			padding-top: 18px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n			text-align: center;\r\n}\r\n\r\nli {\r\n list-style-type: none;\r\n}\r\n\r\n.je_main_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n}\r\n\r\n.je_main_block > .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {\r\n			background-color: #ca0d11;\r\n			color: #fff;\r\n}\r\n\r\n.wh {\r\n    color:#fff;\r\n}\r\n\r\n.je_main_block > .nav > li > a {\r\n			color: #ca0d11;\r\n}\r\n\r\na:hover {\r\n			color:red !important;\r\n}\r\n\r\n.mce-fullscreen\r\n{\r\n	padding-top: 60px !important;\r\n}\r\n\r\n.modal\r\n{\r\n	z-index: 65537;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb > img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\n/* affix and scrollspy */\r\n\r\n.affix\r\n{\r\n   top: 30px;\r\n}\r\n\r\n.page-affix-dropdown-btn\r\n{\r\n    position: fixed;\r\n    left: 20px;\r\n    top: 65px;\r\n    z-index: 2;\r\n}\r\n\r\n.page-affix-dropdown-btn > .nav\r\n{\r\n    max-height: 65vh;\r\n    width: 65vw;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.page-affix-dropdown-btn .nav > li > a\r\n{\r\n    padding: 5px 10px !important;\r\n    color: #262626;\r\n}\r\n\r\n#page-affix .nav .nav\r\n{\r\n    display: none;\r\n}\r\n\r\n#page-affix > .nav > li > .nav\r\n{\r\n    font-size: smaller;\r\n}\r\n\r\n#page-affix > .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav > li > a\r\n{\r\n    padding-left: 25px !important;\r\n}\r\n\r\n#page-affix > .nav .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav .nav > li > a\r\n{\r\n    padding-left: 50px !important;\r\n}\r\n\r\n\r\n#page-affix li > a\r\n{\r\n    border-left: 2px solid transparent;\r\n    padding: 3px;\r\n}\r\n\r\n#page-affix li.active > a\r\n{\r\n    border-left: 2px solid black;\r\n}\r\n\r\n#page-affix li.active > .nav\r\n{\r\n    display: block;\r\n}\r\n\r\nh1 > a[id]:not([href]), h2 > a[id]:not([href]), h3 > a[id]:not([href]), h4 > a[id]:not([href])\r\n{\r\n    top: -130px;\r\n    position: relative;\r\n    display: block;\r\n}\r\n@media screen and (min-width: 992px) {\r\n    .je_weather_block {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 992px) {\r\n    .je_weather_block {\r\n  display: none !important;\r\n  }\r\n}\r\n@media screen and (min-width: 360px) {\r\n    .lang {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 360px) {\r\n    .lang {\r\n  position:absolute; !important;\r\n  top: 90px; !important;\r\n  left: 130px; !important;\r\n  }\r\n}'),(2,'admin','�����-�����','','body {\r\n            background-image: none !important;\r\n        }\r\n\r\n        .modal\r\n        {\r\n            z-index: 65537;\r\n        }\r\n\r\n        .sidebar {\r\n            background-color: #003D66;\r\n            height: 100%;\r\n            display: none;\r\n            text-align: center;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            .sidebar {\r\n                height: 1300px;\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .sidebar > p > a > img{\r\n            width: 100px;\r\n            height: auto;\r\n            margin: 10px;\r\n        }\r\n\r\n        .sidebar > h4 > a\r\n        {\r\n            color: white;\r\n        }\r\n\r\n        .sidebar > h4{\r\n            color: #fff;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            nav.navbar {\r\n                display: none;\r\n            }\r\n        }\r\n\r\n	.panel-admin{\r\n		min-height: 300px;\r\n	}\r\n\r\n\r\n    a.robin-thumb\r\n    {\r\n    	position: relative;\r\n    }\r\n    \r\n    a.robin-thumb > img\r\n    {\r\n    	border-radius: 5px;\r\n    	margin: 5px;\r\n    	border: 1px solid rgba(0,0,0,0.3);\r\n    }');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_styles`
--

DROP TABLE IF EXISTS `template_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_styles` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `style_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `template_id` (`template_id`,`style_id`),
  KEY `tostyle` (`style_id`),
  CONSTRAINT `totemplate2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tostyle` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id_style`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_styles`
--

LOCK TABLES `template_styles` WRITE;
/*!40000 ALTER TABLE `template_styles` DISABLE KEYS */;
INSERT INTO `template_styles` VALUES (5,7,1),(2,8,1),(4,11,2),(11,15,1);
/*!40000 ALTER TABLE `template_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id_template` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(50) NOT NULL DEFAULT '',
  `template_html` text NOT NULL,
  `template_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_template`),
  UNIQUE KEY `template_code` (`template_code`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (3,'email','<html>\r\n <head>\r\n  <meta content=\'text/html; charset=utf-8\' http-equiv=\'Content-Type\'>\r\n </head>\r\n <body style=\'background-color: #eeeeee; font-face: sans-serif; padding-top: 40px;\' alink=\'#000099\' link=\'#000099\' text=\'#000000\' vlink=\'#000099\'>\r\n   <table align=\'center\' bgcolor=\'#ffffff\' border=\'0\' cellpadding=\'5\' cellspacing=\'0\' width=\'80%\'>\r\n    <tbody>\r\n     <tr>\r\n       <td style=\'padding-left: 50px; padding-top: 20px; padding-bottom: 20px;\' valign=\'middle\'><h3 style=\'margin: 0px;\'><%title%></h3></td>\r\n       <td style=\'padding-right: 50px; padding-top: 20px; padding-bottom: 20px;\' align=\'right\' valign=\'top\'>\r\n       <img alt=\'<%prefix%>\' src=\'<%logo%>\' border=\'0\' height=\'100px\'></td>\r\n     </tr><tr>\r\n      <td style=\'border-top: 1px solid #cccccc; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' valign=\'top\'>\r\n        <%content%>\r\n      </td>\r\n    </tr><tr>\r\n      <td style=\'background-color: #666666; color: white; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' nowrap=\'nowrap\' valign=\'top\'>\r\n    <span style=\'font-weight: bold; margin-right: 50px;\'><#brand#></span>\r\n   <span style=\'color: silver; margin-right: 50px;\'><#slogan#></span>\r\n    <a style=\'color: silver;\' href=\'http://<%domain%>\'><%domain%></a>\r\n       </td>\r\n     </tr>\r\n   </tbody>\r\n </table>\r\n <br>\r\n <div align=\'center\'><font color=\'#666666\' size=\'-2\'><a style=\'color: #666666;\' href=\'http://<%domain%>/login/recoveryform.php\'>� �� ����� ������</a>.</font></div>\r\n</body></html>','����������� ������'),(7,'page','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','��������'),(8,'page_backup','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <!-- Bootstrap -->\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <link href=\"/vendor/twbs/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n  <!-- custom -->\r\n<style>\r\n  <%custom_style%>\r\n</style>\r\n\r\n\r\n    <link rel=\"stylesheet\" href=\"/lytebox/lytebox.css\" type=\"text/css\" media=\"screen\">\r\n\r\n    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->\r\n    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->\r\n    <!--[if lt IE 9]>\r\n      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\r\n      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>\r\n    <![endif]-->\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','��������'),(11,'admin','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n\r\n    <%styles%>\r\n\r\n    <link rel=\"stylesheet\" href=\"/lytebox/lytebox.css\" type=\"text/css\" media=\"screen\">\r\n\r\n</head>\r\n<body>\r\n<nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"/admin/\">Engine <small><%version%></small></a>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                <%admin_menu%>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-3 col-md-2 sidebar\">\r\n            <p><a href=\'/admin/\'><img src=\"/images/design/engine_logo.png\"></a></p>\r\n            <h4><a href=\'/admin/\'>Engine <small><%version%></small></a></h4>\r\n            <div class=\"list-group\">\r\n                <%admin_menu_list%>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-9 col-md-10\"><br>\r\n            <%contains%>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<%scripts%>\r\n</body>\r\n</html>','�����-�����'),(15,'test_baidu','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n    <script type=\"text/javascript\">\r\n        var cse;\r\n        \r\n        //??????????\r\n        function display (data) {\r\n            console.log(data);\r\n        }\r\n    \r\n        //?????????\r\n        function init () {\r\n            cse = new BCse.Search(\"7099775790379624042\");    //?????API??ID??????????\r\n            \r\n            cse.getResult(\"???\", display);    //????????????1???????2??????????????????\r\n        }\r\n    \r\n        function loadScript () { \r\n            var script = document.createElement(\"script\"); \r\n            script.type = \"text/javascript\";\r\n            script.charset = \"utf-8\";\r\n            script.src = \"http://zhannei.baidu.com/api/customsearch/apiaccept?sid=7099775790379624042&v=2.0&callback=init\";\r\n            var s = document.getElementsByTagName(\'script\')[0];\r\n            s.parentNode.insertBefore(script, s);\r\n        }\r\n    \r\n        loadScript();\r\n    </script>\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','');
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id_upload` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_upload`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (1,'o_19q1644vt1cig1uug12mr5uf1up59.jpg','o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg',4,1436706555),(2,'o_19q164dub13n1gl01jjs1ug9p8qe.jpg','o_19q164dub13n1gl01jjs1ug9p8qe.jpg.thumb.jpg',4,1436706556),(3,'o_19q17kvu240ue5s1j9nhpt10lb9.jpg','o_19q17kvu240ue5s1j9nhpt10lb9.jpg.thumb.jpg',4,1436708146),(4,'o_19q17lqcufqvsbu1401hd71fqvh.jpg','o_19q17lqcufqvsbu1401hd71fqvh.jpg.thumb.jpg',4,1436708173),(5,'o_19q189l5s1f0r1ars1io7174tsag9.jpg','o_19q189l5s1f0r1ars1io7174tsag9.jpg.thumb.jpg',4,1436708823),(6,'o_19q3ugg9k1gsd104a1td417o2nch9.png','o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg',3,1436799233);
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_name` varchar(255) NOT NULL DEFAULT '',
  `tries` int(11) NOT NULL DEFAULT '0',
  `date_last_try` int(11) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `lang_code` varchar(2) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'1ab4f0253ac67be0d7ffca1cfcb424e8','robin_tail@me.com','robin',0,0,1,'cn'),(3,'ead09a0f7accc3ee1d3874aa5f92a3a7','j.e.morrow@yandex.ru','galahed',0,0,1,'tw'),(4,'37e531f32473be47b911d5f10d16ad04','info@mositong.com','Mositong',0,0,1,'cn');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-15 23:14:18
