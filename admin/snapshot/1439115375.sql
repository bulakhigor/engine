-- MySQL dump 10.13  Distrib 5.1.73, for portbld-freebsd8.4 (i386)
--
-- Host: mositong.mysql    Database: mositong_db
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES cp1251 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `captcha`
--

DROP TABLE IF EXISTS `captcha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `captcha` (
  `id_captcha` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date_create_unix` int(11) NOT NULL,
  `code` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_captcha`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `captcha`
--

LOCK TABLES `captcha` WRITE;
/*!40000 ALTER TABLE `captcha` DISABLE KEYS */;
INSERT INTO `captcha` VALUES (1,1434105610,'066991'),(2,1434132279,'418822'),(3,1434132432,'084312'),(4,1435438005,'017908'),(5,1435438006,'249224'),(6,1435508015,'233020'),(7,1436129937,'143584'),(8,1436269002,'838755'),(9,1436269017,'354993'),(10,1436269041,'391773'),(11,1436275763,'672737'),(12,1436304329,'034371'),(13,1436403760,'529480'),(14,1436439708,'511592'),(15,1436440459,'328070'),(16,1436461531,'776733'),(17,1436462189,'991745'),(18,1436468223,'255225'),(19,1437229459,'308772'),(20,1438216309,'684335'),(21,1438530109,'787304'),(22,1438762017,'563307');
/*!40000 ALTER TABLE `captcha` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_const`
--

DROP TABLE IF EXISTS `language_const`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_const` (
  `id_const` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `const_code` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_const`),
  UNIQUE KEY `const_code` (`const_code`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_const`
--

LOCK TABLES `language_const` WRITE;
/*!40000 ALTER TABLE `language_const` DISABLE KEYS */;
INSERT INTO `language_const` VALUES (1,'admin-cp'),(2,'auth-error'),(3,'brand'),(4,'cancel'),(5,'captcha-enter'),(6,'captcha-error'),(7,'captcha-help'),(8,'change-password'),(9,'email'),(10,'email-sent'),(11,'error'),(12,'error-email-empty'),(13,'error-message'),(14,'error-message-empty'),(15,'error-name-empty'),(16,'id-and-key-error'),(17,'key-error'),(18,'login'),(19,'logout'),(20,'message'),(21,'name'),(22,'new-password'),(23,'password'),(24,'password-changed'),(25,'password-changed-email'),(26,'password-length-error'),(27,'password-really-changed'),(28,'password-recovery'),(29,'quick-edit'),(30,'recover-password'),(31,'recovery-email-link'),(32,'recovery-email-notice'),(33,'recovery-email-sent'),(34,'recovery-form-notice'),(35,'recovery-key'),(36,'recovery-request'),(37,'recovery-start-info'),(38,'redirect'),(39,'redirect-help'),(40,'register'),(41,'register-email'),(42,'register-error'),(43,'remember-your-password'),(44,'remove'),(64,'search'),(45,'send-me-recovery'),(46,'set-new-password'),(47,'user-not-found-error'),(48,'your-id'),(49,'your-key');
/*!40000 ALTER TABLE `language_const` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_trans`
--

DROP TABLE IF EXISTS `language_trans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(10) unsigned NOT NULL,
  `const_id` int(10) unsigned NOT NULL,
  `trans_value` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_trans`),
  UNIQUE KEY `lang_id` (`lang_id`,`const_id`),
  KEY `toconst` (`const_id`),
  CONSTRAINT `toconst` FOREIGN KEY (`const_id`) REFERENCES `language_const` (`id_const`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tolang` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_trans`
--

LOCK TABLES `language_trans` WRITE;
/*!40000 ALTER TABLE `language_trans` DISABLE KEYS */;
INSERT INTO `language_trans` VALUES (1,1,3,'[�����]'),(2,1,8,'������� ������'),(3,1,19,'�����'),(4,1,18,'����'),(5,1,9,'EMail'),(6,1,23,'������'),(7,1,30,'� �� ����� ������'),(8,1,38,'���������������'),(9,1,39,'������� ����, ���� ������ �� ����������'),(10,1,5,'������� ��� ������������'),(11,1,7,'��� �������������� ����� ���������� ������ �������� ���, ������������ �� ��������, ����� �����������, ��� �� ��������� ���������, � �� �������.'),(12,1,20,'���������'),(13,1,2,'������ �����������'),(14,1,16,'ID � ���� ������ ���� �������'),(15,1,26,'������ ������ ���� �� ������ 6 ��������'),(16,1,6,'��� ������������ ������ �����������'),(17,1,47,'������������ �� ������'),(18,1,11,'������'),(19,1,1,'����� �����'),(20,1,13,'��������� �� ������'),(21,1,29,'���������'),(22,1,44,'�������'),(23,1,21,'���'),(24,1,4,'������'),(25,1,41,'<p>�� ���������� ��� �� �����������. �� ��������� � ����� ������������������. ���� �� ���� �������������� �� email ������ � ��������� ���� ������. ������ ������ ����� ������������ ��� ��������.</p><p>����������� ���������� ��� ���������� ������� �� �����. � ����� ������������ ������������ �������� �����, � ������� ������� �� ������� ������ ������ ��� ���� � ����� ��������, � ����� ������ �������� ������� �������.</p>'),(26,1,40,'�����������'),(27,1,42,'������ �����������'),(28,1,10,'������ ����������'),(29,1,33,'������ � ����������� ��� �������������� ������ ���� ������� ���������� �� ������� �����. ��������� ������������� �� ����� ������������ ���'),(30,1,32,'���� ��������� ����������� �������������� �������� ������ �� ������ email. ���� �� �� ����������� �������������� ������, ������ �������������� � ������� ��� ������. ���� �� ������������� ��������� �������������� ������, �������� ��� ��������� ��� ���������� ���� ��������.'),(31,1,35,'����'),(32,1,31,'�������� �������� - ���� ���� ������������ �� ����� ������������ ���. ����� ������������ ������, �������� �� ��������� ������ � ������� ��������� ��������� � �����.'),(33,1,28,'�������������� ������'),(34,1,34,'�� ������ ���� �������� ������ � ��������� ���������� �������������� ������. � ���� ������ ������� ��� ID � ����. ������� ���������� ��� ������, � ����� ����� ������, ������� �� ������ ��������� ����� ������� ������, ������� ������ ���� �� ������ 6 ��������.'),(35,1,48,'��� ID'),(36,1,49,'��� ����'),(37,1,22,'����� ������'),(38,1,46,'������ ����� ������'),(39,1,37,'���� �� ������ ���� ������, �� ������ ��������������� ���� ������ ��� ��������������. ����� ������������ ������, ������� ���������� ���� email. ����� �������� ���� �����, ������� ������ ������ �� ��� ������� email ����� � ����������� ��� ������� ������ ������ ����� ������� ������. ���������� ��� �������������� ������ ������������� �� ����� ������������ ���.'),(40,1,45,'������� ������ �� �����'),(41,1,24,'������ � ����� ������� ������ ��� ������� �������.'),(42,1,43,'��������� ��� ������, ��������� ����������� ����� �� ����.'),(43,1,25,'������ �������'),(44,1,36,'������ �� �������������� ������'),(45,1,27,'������ ��� ������� �������. ������������ ������ ���������� �� ��� ������� �����.'),(46,1,17,'���� �������'),(47,1,14,'�� ������ �� ��������'),(48,1,15,'�� �� ����� ���'),(49,1,12,'�� �� ����� email'),(64,2,3,'[BRAND]'),(65,2,8,'Change password'),(66,2,19,'Logout'),(67,2,18,'Login'),(68,2,9,'EMail'),(69,2,23,'Password'),(70,2,30,'Recover password'),(71,2,38,'Redirecting'),(72,2,39,'Click here if nothing happens'),(73,2,5,'Enter the security code'),(74,2,7,'To prevent spam you have to enter numbers from this picture to make us sure you are not a robot.'),(75,2,20,'Message'),(76,2,2,'Authentication error'),(77,2,16,'ID and Key have to be entered'),(78,2,26,'Password have to be 6 or more chars of length'),(79,2,6,'Security code is invalid'),(80,2,47,'User not found'),(81,2,11,'Error'),(82,2,1,'Admin CP'),(83,2,13,'Error message'),(84,2,29,'Edit'),(85,2,44,'Remove'),(86,2,21,'Name'),(87,2,4,'Cancel'),(88,2,41,'<p>Thank you for registering. We take care of your privacy. Sign in is implemented with email address and password you entered. Password can always be recovered or changed.</p><p>Authorization is required to perform orders on the website. There is a personal address book, with which you can place orders for you and your friends, and also to give useful gifts.</p>'),(89,2,40,'Registration'),(90,2,42,'Registration error'),(91,2,10,'Email sent'),(92,2,33,'The email with password recovery information sent. It is valid until the day ends.'),(93,2,32,'There was requested the ability to recover a forgotten password for your email. If you are not requested for password recovery, just ignore and delete this email. If you really asked for password recovery, we send you details to complete this operation.'),(94,2,35,'Key'),(95,2,31,'Please note that this key is valid until the end of the day. To recover the password, go to the following link and enter the required details in the form.'),(96,2,28,'Password recovery'),(97,2,34,'You should have received a email with details of password recovery. In this email are indicated your ID and Key. Please enter the data, and the new password you want to assign to your account, which must be at least 6 characters.'),(98,2,48,'Account ID'),(99,2,49,'Account Key'),(100,2,22,'NEW password'),(101,2,46,'Set new password'),(102,2,37,'If you have forgotten your password, you can use this form to recover. To recover your password, please enter your email.\nAfter submitting this form, the system will send an email with instructions to set a new password for your account. Instructions to reset your password are valid until the end of the day.'),(103,2,45,'Recover my password'),(104,2,24,'Your account password has been changed.'),(105,2,43,'Please remember your password.'),(106,2,25,'Password changed'),(107,2,36,'Password recovery request'),(108,2,27,'Account password changed. Notification to your email sent.'),(109,2,17,'Account key is invalid'),(110,2,14,'You did not write anything'),(111,2,15,'You did not enter your name'),(112,2,12,'You did not enter your email'),(127,3,3,'????'),(128,3,8,'????'),(129,3,19,'??'),(130,3,18,'??'),(131,3,9,'?EMail'),(132,3,23,'??'),(133,3,30,'??????'),(134,3,38,'??'),(135,3,39,'?????????????'),(136,3,5,'?????'),(137,3,7,'????????????????????????????????????????'),(138,3,20,'??'),(139,3,2,'????'),(140,3,16,'ID???????'),(141,3,26,'??????6???'),(142,3,6,'????????'),(143,3,47,'?????'),(144,3,11,'??'),(145,3,1,'Admin page'),(146,3,13,'????'),(147,3,29,'??'),(148,3,44,'??'),(149,3,21,'??'),(150,3,4,'??'),(151,3,41,'<p>??????????????????????????????????????????????</p><p>??????????????????????????????????????????</p>'),(152,3,40,'??'),(153,3,42,'????'),(154,3,10,'??'),(155,3,33,'???????????????????????????????????'),(156,3,32,'??????????????????????????????????????????????????????????????????????????'),(157,3,35,'??'),(158,3,31,'?? - ???????????????????????????????????????????????'),(159,3,28,'????'),(160,3,34,'?????????????????????????ID?????????????????????????????6????'),(161,3,48,'??ID'),(162,3,49,'????'),(163,3,22,'???'),(164,3,46,'????????'),(165,3,37,'?????????????????????????????????????????????????????????????????????????????????????????????'),(166,3,45,'???????'),(167,3,24,'??????????????'),(168,3,43,'???????????????????'),(169,3,25,'?????'),(170,3,36,'??????'),(171,3,27,'??????????????????????'),(172,3,17,'??????'),(173,3,14,'?????????'),(174,3,15,'?????????'),(175,3,12,'?????????'),(190,4,3,'????'),(191,4,8,'????'),(192,4,19,'??'),(193,4,18,'??'),(194,4,9,'?EMail'),(195,4,23,'??'),(196,4,30,'??????'),(197,4,38,'??'),(198,4,39,'?????????????'),(199,4,5,'?????'),(200,4,7,'????????????????????????????????????????'),(201,4,20,'??'),(202,4,2,'????'),(203,4,16,'ID???????'),(204,4,26,'??????6???'),(205,4,6,'????????'),(206,4,47,'?????'),(207,4,11,'??'),(208,4,1,'Admin page'),(209,4,13,'????'),(210,4,29,'??'),(211,4,44,'??'),(212,4,21,'??'),(213,4,4,'??'),(214,4,41,'<p>??????????????????????????????????????????????</p> <p>??????????????????????????????????????????</p>'),(215,4,40,'??'),(216,4,42,'????'),(217,4,10,'??'),(218,4,33,'???????????????????????????????????'),(219,4,32,'??????????????????????????????????????????????????????????????????????????'),(220,4,35,'??'),(221,4,31,'?? - ???????????????????????????????????????????????'),(222,4,28,'????'),(223,4,34,'?????????????????????????ID?????????????????????????????6????'),(224,4,48,'??ID'),(225,4,49,'????'),(226,4,22,'???'),(227,4,46,'????????'),(228,4,37,'?????????????????????????????????????????????????????????????????????????????????????????????'),(229,4,45,'???????'),(230,4,24,'??????????????'),(231,4,43,'???????????????????'),(232,4,25,'?????'),(233,4,36,'??????'),(234,4,27,'??????????????????????'),(235,4,17,'??????'),(236,4,14,'?????????'),(237,4,15,'?????????'),(238,4,12,'?????????'),(253,1,64,'�����'),(254,2,64,'Search'),(255,3,64,'??'),(256,4,64,'??');
/*!40000 ALTER TABLE `language_trans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id_lang` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` varchar(2) NOT NULL DEFAULT '',
  `lang_name` varchar(20) NOT NULL DEFAULT '',
  `lang_flag` varchar(255) NOT NULL DEFAULT '',
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_domestic` tinyint(1) NOT NULL DEFAULT '0',
  `is_international` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_lang`),
  UNIQUE KEY `lang_code` (`lang_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'ru','�������','/images/flags/ru.gif',0,1,0,0),(2,'en','English','/images/flags/gb.gif',0,1,0,0),(3,'cn','??','/images/flags/cn.gif',1,0,1,0),(4,'tw','??','/images/flags/tw.gif',1,0,0,1);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id_link` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_link_id` int(10) unsigned DEFAULT NULL,
  `link_page_id` int(10) unsigned DEFAULT NULL,
  `link_title_short` varchar(50) DEFAULT NULL,
  `link_ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_link`),
  KEY `toparent` (`parent_link_id`),
  KEY `topage` (`link_page_id`),
  CONSTRAINT `topage` FOREIGN KEY (`link_page_id`) REFERENCES `pages` (`id_page`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `toparent` FOREIGN KEY (`parent_link_id`) REFERENCES `menu` (`id_link`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (8,NULL,1,NULL,0),(20,NULL,6,NULL,1),(23,NULL,11,NULL,2),(33,23,13,NULL,1),(34,23,14,NULL,2),(35,23,15,NULL,3),(37,20,17,NULL,1),(38,20,18,NULL,2),(41,20,21,NULL,3),(42,NULL,22,NULL,3),(43,NULL,23,NULL,4);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `id_migration` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `migration_timestamp` int(11) NOT NULL,
  `executed_file` varchar(255) NOT NULL DEFAULT '',
  `executed_at` int(11) NOT NULL,
  `snapshot_file` varchar(255) NOT NULL DEFAULT '',
  `commit` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_migration`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES (1,20150708,'20150708.sql',1436609559,'1436609558.sql',''),(2,20150714,'20150714.sql',1436991258,'1436991257.sql',''),(3,20150716,'20150716.sql',1437327023,'1437327021.sql',''),(4,20150720,'20150720.sql',1437424879,'1437424878.sql',''),(5,20150721,'20150721.sql',1437590781,'1437590780.sql',''),(6,20150722,'20150722.sql',1437590782,'1437590780.sql',''),(7,20150808,'20150808.sql',1439068052,'1439068051.sql','7a2693f1f05b374910060564a45e61e5d0530414');
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_trans`
--

DROP TABLE IF EXISTS `page_trans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_trans` (
  `id_trans` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `page_title_short` varchar(150) NOT NULL DEFAULT '',
  `page_title_long` varchar(255) NOT NULL DEFAULT '',
  `page_html` longtext NOT NULL,
  `page_keywords` varchar(255) NOT NULL DEFAULT '',
  `page_description` varchar(255) NOT NULL DEFAULT '',
  `page_affix` longtext NOT NULL,
  PRIMARY KEY (`id_trans`),
  KEY `topage2` (`page_id`),
  KEY `tolang2` (`lang_id`),
  CONSTRAINT `tolang2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id_lang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `topage2` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id_page`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_trans`
--

LOCK TABLES `page_trans` WRITE;
/*!40000 ALTER TABLE `page_trans` DISABLE KEYS */;
INSERT INTO `page_trans` VALUES (9,6,3,'??','??','<p>???????????????????????</p>\r\n<p><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" alt=\"\" width=\"157\" height=\"117\" /></p>','??','???????????',''),(10,7,3,'??','???????','<p><strong>????</strong>???<strong>????</strong>?<strong>????</strong>?<strong>??</strong>??<a class=\"new\" title=\"?????????\" href=\"https://zh.wikipedia.org/w/index.php?title=%E6%8D%95%E6%BC%81&action=edit&redlink=1\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AD%9A%E9%A1%9E\">??</a>????????????????????????????????????</p>\r\n<p>?????????????????????????????????????????????????<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%8A%E9%AD%9A\">??</a>???????????<a title=\"??????\" href=\"https://zh.wikipedia.org/wiki/%E6%B5%B7%E6%B4%8B%E7%94%9F%E6%85%8B%E7%B3%BB%E7%B5%B1\">??????</a>?</p>\r\n<p>???????????????????????????????????????????????????????????????????????????????????<a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%B1%92%E9%AD%9A\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%89%E9%AD%9A\">??</a>?????????????????????????????</p>\r\n<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a></p>','????','',''),(11,8,3,'VIP','VIP','<p><img src=\"images/upload/o_19q17lqcufqvsbu1401hd71fqvh.jpg\" alt=\"\" width=\"153\" height=\"109\" /></p>\r\n<p>�</p>\r\n<p style=\"text-align: right;\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"156\" height=\"299\" /></p>','VIP','???????',''),(13,11,3,'????','????','<h4><a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></h4>\r\n<p>�</p>','????','????????????',''),(14,12,3,'???????','???????','','???????','?????????????????',''),(24,6,4,'??','??','<p><span>??????????????????????</span></p>\r\n<p><span><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" alt=\"\" width=\"157\" height=\"117\" /></span></p>','??','???????????',''),(25,7,4,'??','???????','<p>?????????????????????<a class=\"new\" title=\"?????????\" href=\"https://zh.wikipedia.org/w/index.php?title=%E6%8D%95%E6%BC%81&action=edit&redlink=1\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AD%9A%E9%A1%9E\">??</a>????????????????????????????????????</p>\r\n<p>?????????????????????????????????????????????????<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%8A%E9%AD%9A\">??</a>???????????<a title=\"??????\" href=\"https://zh.wikipedia.org/wiki/%E6%B5%B7%E6%B4%8B%E7%94%9F%E6%85%8B%E7%B3%BB%E7%B5%B1\">??????</a>?</p>\r\n<p>???????????????????????????????????????????????????????????????????????????????????<a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%B1%92%E9%AD%9A\">??</a>???<a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%AF%89%E9%AD%9A\">??</a>?????????????????????????????</p>\r\n<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a></p>','????','',''),(26,8,4,'VIP','VIP','<p><img src=\"images/upload/o_19q17lqcufqvsbu1401hd71fqvh.jpg\" alt=\"\" width=\"153\" height=\"109\" /></p>\r\n<p style=\"padding-left: 30px; text-align: right;\"><img src=\"images/upload/o_19q189l5s1f0r1ars1io7174tsag9.jpg\" alt=\"\" width=\"156\" height=\"299\" /></p>','VIP','???????',''),(28,11,4,'????','????','<h4>�<a title=\"??\" href=\"rest/zhong\">??</a>��<a title=\"??\" href=\"rest/russ\">??</a>��<a title=\"??\" href=\"rest/view\">??</a></h4>','????','????????????',''),(29,12,4,'???????','???????','','??????','?????? ????? ??????',''),(38,9,3,'','','<p><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg\" alt=\"\" /></a><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg\" alt=\"\" /></a><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q17kvu240ue5s1j9nhpt10lb9.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q17kvu240ue5s1j9nhpt10lb9.jpg.thumb.jpg\" alt=\"\" /></a></p>','','',''),(39,9,4,'','','','','',''),(46,13,3,'??','??','<h4><a title=\"??\" href=\"zhong\">??</a> �<a title=\"??\" href=\"rest/russ\">??</a>� <a title=\"??\" href=\"rest/view\">??</a></h4>','??','??',''),(47,13,4,'??','??','<h4><a title=\"??\" href=\"zhong\">??</a> �<a title=\"??\" href=\"rest/russ\">??</a>��<a title=\"??\" href=\"rest/view\">??</a></h4>','??','??',''),(50,14,3,'???','???','','???','????????????????�???',''),(51,14,4,'???','???','','???','???????????????????',''),(52,16,3,'??','??','<p><strong>??</strong><span>???</span><strong>??</strong><span>?</span><strong>??</strong><span>???</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E4%BA%BA%E9%A1%9E\">??</a><span>??</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%8B%95%E7%89%A9\">??</a><span>???</span><a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%A3%9F%E7%89%A9\">??</a><span>???????????????????????????????????</span></p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-cn/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>???????????�????????????????��<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�????????????�????????????�???????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??�?????????????????????????�??????????????????????????????�?????????�<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-cn/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????�??????????????�<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-cn/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>\r\n<p>�</p>\r\n<hr />\r\n<p><span style=\"text-decoration: underline;\"><strong>????</strong></span><strong>� � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � �</strong></p>\r\n<p>????????????????????????????????????????????�<strong> � �</strong></p>\r\n<p><br />�� � ��</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\"><br />�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>\r\n<p style=\"text-align: right;\">�</p>','','???????',''),(53,16,4,'??','??','<p><strong>??</strong><span>???</span><strong>??</strong><span>?</span><strong>??</strong><span>???</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E4%BA%BA%E9%A1%9E\">??</a><span>??</span><a class=\"mw-redirect\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%8B%95%E7%89%A9\">??</a><span>???</span><a title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E9%A3%9F%E7%89%A9\">??</a><span>???????????????????????????????????</span></p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>?????????????????????????????�<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�?????????????????????????????????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??????????????????????????????????????????????????????????????????????<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????????????????????<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>','','???????',''),(58,15,3,'??','??','<h4>�</h4>\r\n<p>qwe</p>','??','??',''),(59,15,4,'??','??','<h4>�</h4>','??','??',''),(112,20,3,'??','????','<p><span style=\"font-size: 14pt;\">� �??</span></p>\r\n<table style=\"height: 232px;\" width=\"783\">\r\n<tbody>\r\n<tr>\r\n<td><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg\" data-lyte-options=\"group:page\"><img style=\"margin-right: 15px; margin-left: 15px;\" src=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"vertical-align: top;\">\r\n<p style=\"text-align: left;\"><span style=\"font-size: 12pt;\">????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�</p>','???????','????',''),(113,20,4,'??','????','<p><span style=\"font-size: 14pt;\">� �??</span></p>\r\n<table style=\"height: 232px;\" width=\"783\">\r\n<tbody>\r\n<tr>\r\n<td><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg\" data-lyte-options=\"group:page\"><img style=\"margin-right: 15px; margin-left: 15px;\" src=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"vertical-align: top;\">\r\n<p style=\"text-align: left;\"><span style=\"font-size: 12pt;\">????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�</p>','???????','????',''),(140,21,3,'????','????','<p><span style=\"font-size: 14pt;\">� ????</span></p>\r\n<table style=\"height: 233px;\" width=\"725\">\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align: center; vertical-align: middle;\"><span style=\"font-size: 12pt;\"><strong>�1</strong></span></td>\r\n<td style=\"text-align: center; vertical-align: middle;\">�<span style=\"font-size: 12pt;\"><strong>2</strong></span></td>\r\n<td style=\"text-align: center; vertical-align: middle;\"><strong><span style=\"font-size: 12pt;\">3�</span></strong></td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align: center; vertical-align: middle;\">�<a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8iq59lnbofk1blj1kl81kc915.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8iq59lnbofk1blj1kl81kc915.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"text-align: center; vertical-align: top;\">�<a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8lt8jh5g91una216vil1j1a.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8lt8jh5g91una216vil1j1a.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"text-align: center; vertical-align: top;\">�<a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8obfg30hk881q491tm012r71f.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8obfg30hk881q491tm012r71f.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�</p>','','???????',''),(141,21,4,'????','????','<p><span style=\"font-size: 14pt;\">� ????</span></p>\r\n<table style=\"height: 233px;\" width=\"725\">\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align: center; vertical-align: middle;\"><span style=\"font-size: 12pt;\"><strong>�1</strong></span></td>\r\n<td style=\"text-align: center; vertical-align: middle;\">�<span style=\"font-size: 12pt;\"><strong>2</strong></span></td>\r\n<td style=\"text-align: center; vertical-align: middle;\"><span style=\"font-size: 12pt;\"><strong>3�</strong></span></td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align: center; vertical-align: top;\">�<a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8iq59lnbofk1blj1kl81kc915.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8iq59lnbofk1blj1kl81kc915.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"text-align: center; vertical-align: top;\">�<a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8lt8jh5g91una216vil1j1a.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8lt8jh5g91una216vil1j1a.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"text-align: center; vertical-align: top;\">�<a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8obfg30hk881q491tm012r71f.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8obfg30hk881q491tm012r71f.jpg.thumb.jpg\" alt=\"\" /></a><br /><br /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�</p>','','???????',''),(146,23,3,'??','??','<h3><span style=\"font-size: 18pt;\"><span style=\"text-decoration: underline;\">???</span></span></h3>\r\n<p><span style=\"font-size: 12pt;\">???</span></p>\r\n<p><span style=\"font-size: medium;\">???</span></p>\r\n<p><span style=\"font-size: medium;\">???</span></p>','????','',''),(147,23,4,'??','??','<p><span style=\"font-size: 18pt;\"><strong><span style=\"text-decoration: underline;\">???</span></strong></span></p>\r\n<p><span style=\"font-size: medium;\">???</span></p>\r\n<p><span style=\"font-size: medium;\">???</span></p>\r\n<p><span style=\"font-size: medium;\">???</span></p>\r\n<p>�</p>','????','',''),(182,17,3,'????','????','<p><span style=\"color: #000000; font-size: 10pt;\">�</span><span style=\"color: #000000;\"><a class=\"btn btn-default\" href=\"shoulie\">??</a> �� <a class=\"btn btn-default\" href=\"buyu\">??</a> ��<span style=\"background-color: #cccccc;\">?????</span> �<span style=\"background-color: #cccccc;\">????????????</span>� <span style=\"background-color: #cccccc;\">????</span></span></p>\r\n<hr />\r\n<p><img style=\"border-width: 2px;\" src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" alt=\"??\" width=\"244\" height=\"182\" /></p>\r\n<p><!-- pagebreak --></p>\r\n<p>�</p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>?????????????????????????????�<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�?????????????????????????????????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??????????????????????????????????????????????????????????????????????<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????????????????????<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>\r\n<p>�</p>\r\n<p>�</p>','?????????????????????????????????????????????????????????','??????????',''),(183,17,4,'????','????','<p><a class=\"btn btn-default\" href=\"shoulie\">??</a>�� <a class=\"btn btn-default\" href=\"buyu\">??</a> ��<span style=\"background-color: #cccccc;\">?????</span> �<span style=\"background-color: #cccccc;\">????????????</span> �<span style=\"background-color: #cccccc;\">????</span></p>\r\n<hr />\r\n<p><img src=\"images/upload/o_19q3ugg9k1gsd104a1td417o2nch9.png\" alt=\"??\" width=\"273\" height=\"203\" /></p>\r\n<p><!-- pagebreak --></p>\r\n<p>�</p>\r\n<p>???????????<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%9F%B3%E5%AE%97%E5%85%83\">???</a>????????????????????<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-1\">[1]</a></sup>?????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E9%BB%84%E5%BA%AD%E5%9D%9A\">???</a>?????????????????????????????�<a title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E5%85%83%E5%A5%BD%E9%97%AE\">???</a>�?????????????????????????????????????<a class=\"mw-redirect\" title=\"???\" href=\"https://zh.wikipedia.org/wiki/%E6%88%B4%E6%9D%B1%E5%8E%9F\">???</a>??????????????????????????????????????????????????????????????????????<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-2\">[2]</a></sup>?</p>\r\n<p>???????????????????<a class=\"mw-disambig\" title=\"??\" href=\"https://zh.wikipedia.org/wiki/%E5%AE%B6%E6%B3%95\">??</a>????????????????????<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"https://zh.wikipedia.org/zh-hk/%E7%8B%A9%E7%8D%B5#cite_note-3\">[3]</a></sup>??????????<a class=\"mw-redirect\" title=\"????\" href=\"https://zh.wikipedia.org/wiki/%E6%9C%A8%E5%85%B0%E5%9B%B4%E5%9C%BA\">????</a>????????????????????????????????????????????????????????????????????????????105??</p>','?????????????????????????????????????????????????????????','??????????',''),(186,22,3,'????','????','<p><a title=\"qwe\" href=\"activities\">????</a></p>','???','',''),(187,22,4,'????','????','','???','',''),(234,18,3,'??','??','<p style=\"padding-left: 30px;\"><span style=\"font-size: 10pt;\"> <a class=\"btn btn-default\" href=\"lvyou\">??</a>� <a class=\"btn btn-default\" href=\"activities\">????</a>� <a class=\"btn btn-default\" href=\"yee\">??</a>� <a class=\"btn btn-default\" href=\"lvyouxiangmu\">????</a>�</span></p>\r\n<table style=\"height: 328px; background-color: #ffffff; float: left;\" width=\"1020\">\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align: center;\" rowspan=\"4\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rl188r7cqecmic945h314ma7.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rl188r7cqecmic945h314ma7.jpg.thumb.jpg\" alt=\"\" width=\"330\" /></a></td>\r\n<td style=\"text-align: center;\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rk8gl2ctp3ugb1kmsf2u1uvdf.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rk8gl2ctp3ugb1kmsf2u1uvdf.jpg.thumb.jpg\" alt=\"\" width=\"100\" /></a></td>\r\n<td style=\"text-align: center;\" rowspan=\"4\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rkvkldc86agtrah91vcq4a6c.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rkvkldc86agtrah91vcq4a6c.jpg.thumb.jpg\" alt=\"\" width=\"330\" /></a></td>\r\n<td style=\"text-align: center; vertical-align: top;\" rowspan=\"4\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rkvqf6t157tqapip51rh016b0h.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rkvqf6t157tqapip51rh016b0h.jpg.thumb.jpg\" alt=\"\" width=\"330\" /></a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align: center;\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg.thumb.jpg\" alt=\"\" width=\"100\" /></a></td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align: center;\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg\" alt=\"\" width=\"100\" /></a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�<!-- pagebreak --></p>\r\n<p style=\"padding-left: 30px;\"><span style=\"font-size: medium;\">?????????????</span></p>\r\n<p>�</p>\r\n<p>�</p>\r\n<p>�</p>','????','?????????',''),(235,18,4,'??','??','<p style=\"padding-left: 30px;\"><span style=\"font-size: 10pt;\"><a class=\"btn btn-default\" href=\"lvyou\">??</a>��<a class=\"btn btn-default\" href=\"activities\">????</a>��<a class=\"btn btn-default\" href=\"yee\">??</a>��<a class=\"btn btn-default\" href=\"lvyouxiangmu\">????</a></span><span></span></p>\r\n<table style=\"height: 347px;\" width=\"1121\">\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align: center;\" rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rkvdj5e1rd81p9r1hn33aiug7.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rkvdj5e1rd81p9r1hn33aiug7.jpg.thumb.jpg\" alt=\"\" width=\"330\" /></a></td>\r\n<td style=\"text-align: center;\" rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rkvkldc86agtrah91vcq4a6c.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rkvkldc86agtrah91vcq4a6c.jpg.thumb.jpg\" alt=\"\" width=\"330\" /></a></td>\r\n<td style=\"text-align: center; vertical-align: top;\" rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rkvqf6t157tqapip51rh016b0h.jpg\" data-lyte-options=\"group:page\"><img src=\"images/upload/o_19rkvqf6t157tqapip51rh016b0h.jpg.thumb.jpg\" alt=\"\" width=\"330\" /></a></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�<!-- pagebreak --></p>\r\n<p style=\"padding-left: 30px;\"><span style=\"font-size: medium;\">?????????????</span></p>\r\n<p>�</p>\r\n<p>�</p>\r\n<p>�</p>','????','?????????',''),(268,1,3,'??','??','<div id=\"carousel-example-generic\" class=\"carousel slide\" data-ride=\"carousel\"><!-- Indicators --> <!-- Wrapper for slides -->\r\n<div class=\"carousel-inner\">\r\n<div class=\"item active\"><img src=\"images/upload/o_19rmnhv138471ci11mp61rji2rd.jpg\" alt=\"?????\" width=\"900\" height=\"500\" />\r\n<div class=\"carousel-caption\">�</div>\r\n</div>\r\n<div class=\"item\"><img src=\"images/upload/o_19rmnhv13q2n1lc41ei5m8c1slbc.jpg\" alt=\"????\" width=\"900\" height=\"500\" />\r\n<div class=\"carousel-caption\">�</div>\r\n</div>\r\n<div class=\"item\"><img src=\"images/upload/o_19rmnhv131i6pc441doc7t31g9kb.jpg\" alt=\"??\" width=\"900\" height=\"500\" />\r\n<div class=\"carousel-caption\">�</div>\r\n</div>\r\n<div class=\"item\"><img src=\"images/upload/o_19rmnhv13maquloung776plca.jpg\" alt=\"??\" width=\"900\" height=\"500\" />\r\n<div class=\"carousel-caption\">�</div>\r\n</div>\r\n<!-- Controls --> <a class=\"left carousel-control\" href=\"#carousel-example-generic\" data-slide=\"prev\"> <span class=\"glyphicon glyphicon-chevron-left\"></span> <span class=\"sr-only\">Previous</span> </a> <a class=\"right carousel-control\" href=\"#carousel-example-generic\" data-slide=\"next\"> <span class=\"glyphicon glyphicon-chevron-right\"></span> <span class=\"sr-only\">Next</span> </a></div>\r\n</div>','??????????????????????','??? - ????????????',''),(269,1,4,'??','??','','??????????????????????','??? ? ????????????',''),(270,19,3,'??','??','<p><span style=\"font-size: 14pt;\">� �<a title=\"??\" href=\"lieniao\">??</a></span></p>\r\n<table style=\"height: 230px; background-color: #ffffff;\" width=\"1084\">\r\n<tbody>\r\n<tr>\r\n<td rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg\" data-lyte-options=\"group:page\"><img style=\"margin-right: 15px; margin-left: 15px;\" src=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg.thumb.jpg\" alt=\"\" width=\"213\" /></a></td>\r\n<td style=\"vertical-align: top;\" rowspan=\"2\"><span style=\"font-size: 12pt;\">� � ????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>�</p>\r\n<table style=\"height: 484px; background-color: #ffffff;\" width=\"1089\">\r\n<tbody>\r\n<tr>\r\n<td style=\"vertical-align: top;\" rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7es1sp2a1pel16jh12ll1t10c.jpg\" data-lyte-options=\"group:page\"><img style=\"margin-right: 15px; margin-left: 15px;\" src=\"images/upload/o_19rj7es1sp2a1pel16jh12ll1t10c.jpg.thumb.jpg\" alt=\"\" /></a></td>\r\n<td style=\"vertical-align: top;\" rowspan=\"2\">\r\n<p><span style=\"font-size: 14pt;\">????</span></p>\r\n<p><span style=\"font-size: 12pt;\">�???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</span></p>\r\n<p><span style=\"font-size: 12pt;\"><button class=\"btn btn-primary\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapseExample\">???? </button></span></p>\r\n<div id=\"collapseExample\" class=\"collapse\">\r\n<div class=\"well\">\r\n<div id=\"collapseOne\" class=\"panel-collapse collapse in\">\r\n<div class=\"panel-body\">???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</div>\r\n</div>\r\n</div>\r\n</div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>�</p>','?????','??????????',''),(271,19,4,'??','??','<p><span style=\"font-size: 14pt;\">� �<a title=\"??\" href=\"lieniao\">??</a></span></p>\r\n<table style=\"height: 228px;\" width=\"714\">\r\n<tbody>\r\n<tr>\r\n<td rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg\" data-lyte-options=\"group:page\"><img style=\"margin-right: 15px; margin-left: 15px;\" src=\"images/upload/o_19rj7elaqicu38g1qo21j59188q7.jpg.thumb.jpg\" alt=\"\" width=\"213\" /></a></td>\r\n<td style=\"vertical-align: top;\" rowspan=\"2\"><span style=\"font-size: 12pt;\"> � �????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>� � �<span style=\"font-size: 19px;\">??</span></p>\r\n<table style=\"height: 251px;\" width=\"710\">\r\n<tbody>\r\n<tr>\r\n<td style=\"vertical-align: top;\" rowspan=\"2\"><a class=\"lytebox robin-thumb\" href=\"images/upload/o_19rj7es1sp2a1pel16jh12ll1t10c.jpg\" data-lyte-options=\"group:page\"><img style=\"margin-right: 15px; margin-left: 15px;\" src=\"images/upload/o_19rj7es1sp2a1pel16jh12ll1t10c.jpg.thumb.jpg\" alt=\"\" width=\"213\" /></a></td>\r\n<td style=\"vertical-align: top;\" rowspan=\"2\"><span style=\"font-size: 12pt;\">� �???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????<br /><br /></span></td>\r\n</tr>\r\n</tbody>\r\n</table>','?????','??????????',''),(284,26,3,'Test page','Test page','<p><span class=\"expanded\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span> <span class=\"collapsed\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut en...</span> <a class=\"collapsed\">������ ������</a> <a class=\"expanded\">������</a></p>','','',''),(285,26,4,'go to other lang','go to other lang','<p>go to other lang</p>','','','');
/*!40000 ALTER TABLE `page_trans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id_page` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_url` varchar(50) NOT NULL DEFAULT '',
  `template_id` int(10) unsigned NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_page`),
  UNIQUE KEY `page_url` (`page_url`),
  KEY `totemplate` (`template_id`),
  CONSTRAINT `totemplate` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'index',7),(6,'lvyou',7),(7,'buyu',7),(8,'VIP',7),(9,'test_baidu',15),(11,'food',7),(12,'clubs',7),(13,'restaurant',7),(14,'cafe',7),(15,'bar',7),(16,'hunt',7),(17,'activities',7),(18,'yee',7),(19,'shoulie',7),(20,'lieniao',7),(21,'lvyouxiangmu',7),(22,'aboutus',7),(23,'contacts',7),(26,'t',7);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `skey` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `type_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `key` (`skey`),
  KEY `totype` (`type_id`),
  CONSTRAINT `totype` FOREIGN KEY (`type_id`) REFERENCES `settings_types` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'thumb_size','500','������ ������� ������',0,2),(2,'email_from','no@no.no','�� ���� ���������� �����',0,1),(3,'index_page','index','URL ������� ��������',0,1),(4,'site_image','','����������� ��������',0,1),(5,'captcha_font_file','images/captcha/digits2.png','���� � �������� � �������',0,1),(7,'last_usage_report','1439105992','��������� ����������� �� �������������',1,2),(8,'upload_dir','images/upload','������� ��� �������� �����������',0,1),(9,'menu_depth','&nbsp;&nbsp;&nbsp;','����������� ����������� ���� ����� ������� ������',0,5),(10,'uploads_show_per_cycle','20','������� ���������� ������� �� ���',0,2),(11,'email_prefix','NN','������� �����',0,1),(12,'email_logo','images/logo.png','���� � ������',0,1),(13,'email_tech','j.e.morrow@yandex.ru','����� ������������ ��������������',0,1),(14,'anti_brute_tries','3','����� ������� ����� ������ �� ��������� ����������',0,2),(15,'anti_brute_time','600','����� ���������� �������� ��� ��������� �������� ����� ������ (� ��������)',0,2),(16,'use_double_click_menu','1','�������� ������������ � �������� ����������� ����',0,3),(17,'maintenance_mode','1','�������� ����� �� ������������',0,3);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings_types`
--

DROP TABLE IF EXISTS `settings_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings_types` (
  `id_type` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_code` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_type`),
  UNIQUE KEY `type_code` (`type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings_types`
--

LOCK TABLES `settings_types` WRITE;
/*!40000 ALTER TABLE `settings_types` DISABLE KEYS */;
INSERT INTO `settings_types` VALUES (3,'bool'),(4,'float'),(5,'html'),(2,'int'),(1,'string');
/*!40000 ALTER TABLE `settings_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `styles` (
  `id_style` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `style_code` varchar(50) NOT NULL,
  `style_description` varchar(255) DEFAULT NULL,
  `style_link` varchar(255) DEFAULT NULL,
  `style_text` longtext,
  PRIMARY KEY (`id_style`),
  UNIQUE KEY `style_code` (`style_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'common','����������� ����� ��������','','body\r\n{\r\n	background-image: url(http://mositong.com/images/design/background.jpg);\r\n	background-repeat: no-repeat;\r\n	\r\n	background-position: top center;\r\n        padding-top: 20px;\r\n}\r\n\r\n.je_header_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			vertical-align: top;\r\n}\r\n\r\n.je_footer_block {\r\n			margin-bottom: 30px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			height: 42px;\r\n			background-color: #ca0d11;\r\n			border-radius: 0px;\r\n			color: #fff;\r\n}\r\n\r\n.je_footer_block > p > a {\r\n			color: #fff;\r\n			text-decoration: underline;\r\n		}\r\n		\r\n.je_weather_block {\r\n			margin-bottom: 10px;\r\n			padding: 10px;\r\n			padding-top: 18px;\r\n			width: 100%;\r\n			height: 100px;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n			text-align: center;\r\n}\r\n\r\nli {\r\n list-style-type: none;\r\n}\r\n\r\n.je_main_block {\r\n			margin-bottom: 25px;\r\n			padding: 10px;\r\n			width: 100%;\r\n			background-color: #fff;\r\n			border-radius: 0px;\r\n}\r\n\r\n.je_main_block > .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {\r\n			background-color: #ca0d11;\r\n			color: #fff;\r\n}\r\n\r\n.wh {\r\n    color:#fff;\r\n}\r\n\r\n.je_main_block > .nav > li > a {\r\n			color: #ca0d11;\r\n}\r\n\r\na:hover {\r\n			color:red !important;\r\n}\r\n\r\n.mce-fullscreen\r\n{\r\n	padding-top: 60px !important;\r\n}\r\n\r\n.modal\r\n{\r\n	z-index: 65537;\r\n}\r\n\r\na.robin-thumb\r\n{\r\n	position: relative;\r\n}\r\n\r\na.robin-thumb > img\r\n{\r\n	border-radius: 5px;\r\n	margin: 5px;\r\n	border: 1px solid rgba(0,0,0,0.3);\r\n}\r\n\r\n/* affix and scrollspy */\r\n\r\n.affix\r\n{\r\n   top: 30px;\r\n}\r\n\r\n.page-affix-dropdown-btn\r\n{\r\n    position: fixed;\r\n    left: 20px;\r\n    top: 65px;\r\n    z-index: 2;\r\n}\r\n\r\n.page-affix-dropdown-btn > .nav\r\n{\r\n    max-height: 65vh;\r\n    width: 65vw;\r\n    overflow-y: scroll;\r\n}\r\n\r\n.page-affix-dropdown-btn .nav > li > a\r\n{\r\n    padding: 5px 10px !important;\r\n    color: #262626;\r\n}\r\n\r\n#page-affix .nav .nav\r\n{\r\n    display: none;\r\n}\r\n\r\n#page-affix > .nav > li > .nav\r\n{\r\n    font-size: smaller;\r\n}\r\n\r\n#page-affix > .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav > li > a\r\n{\r\n    padding-left: 25px !important;\r\n}\r\n\r\n#page-affix > .nav .nav .nav > li > a, .page-affix-dropdown-btn .nav .nav .nav > li > a\r\n{\r\n    padding-left: 50px !important;\r\n}\r\n\r\n\r\n#page-affix li > a\r\n{\r\n    border-left: 2px solid transparent;\r\n    padding: 3px;\r\n}\r\n\r\n#page-affix li.active > a\r\n{\r\n    border-left: 2px solid black;\r\n}\r\n\r\n#page-affix li.active > .nav\r\n{\r\n    display: block;\r\n}\r\n\r\nh1 > a[id]:not([href]), h2 > a[id]:not([href]), h3 > a[id]:not([href]), h4 > a[id]:not([href])\r\n{\r\n    top: -130px;\r\n    position: relative;\r\n    display: block;\r\n}\r\n@media screen and (min-width: 992px) {\r\n    .je_weather_block {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 992px) {\r\n    .je_weather_block {\r\n  display: none !important;\r\n  }\r\n}\r\n@media screen and (min-width: 360px) {\r\n    .lang {\r\n  display: block !important;\r\n  }\r\n}  \r\n@media screen and (max-width: 360px) {\r\n    .lang {\r\n  position:absolute; !important;\r\n  top: 90px; !important;\r\n  left: 130px; !important;\r\n  }\r\n}'),(2,'admin','�����-�����','','body {\r\n            background-image: none !important;\r\n        }\r\n\r\n        .modal\r\n        {\r\n            z-index: 65537;\r\n        }\r\n\r\n        .sidebar {\r\n            background-color: #003D66;\r\n            height: 100%;\r\n            display: none;\r\n            text-align: center;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            .sidebar {\r\n                height: 1300px;\r\n                display: block;\r\n            }\r\n        }\r\n\r\n        .sidebar > p > a > img{\r\n            width: 100px;\r\n            height: auto;\r\n            margin: 10px;\r\n        }\r\n\r\n        .sidebar > h4 > a\r\n        {\r\n            color: white;\r\n        }\r\n\r\n        .sidebar > h4{\r\n            color: #fff;\r\n        }\r\n\r\n        @media (min-width: 768px) {\r\n            nav.navbar {\r\n                display: none;\r\n            }\r\n        }\r\n\r\n	.panel-admin{\r\n		min-height: 300px;\r\n	}\r\n\r\n\r\n    a.robin-thumb\r\n    {\r\n    	position: relative;\r\n    }\r\n    \r\n    a.robin-thumb > img\r\n    {\r\n    	border-radius: 5px;\r\n    	margin: 5px;\r\n    	border: 1px solid rgba(0,0,0,0.3);\r\n    }');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_styles`
--

DROP TABLE IF EXISTS `template_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_styles` (
  `rid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `style_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `template_id` (`template_id`,`style_id`),
  KEY `tostyle` (`style_id`),
  CONSTRAINT `totemplate2` FOREIGN KEY (`template_id`) REFERENCES `templates` (`id_template`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tostyle` FOREIGN KEY (`style_id`) REFERENCES `styles` (`id_style`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_styles`
--

LOCK TABLES `template_styles` WRITE;
/*!40000 ALTER TABLE `template_styles` DISABLE KEYS */;
INSERT INTO `template_styles` VALUES (17,7,1),(2,8,1),(4,11,2),(11,15,1),(15,17,1);
/*!40000 ALTER TABLE `template_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id_template` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template_code` varchar(50) NOT NULL DEFAULT '',
  `template_html` text NOT NULL,
  `template_description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_template`),
  UNIQUE KEY `template_code` (`template_code`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (3,'email','<html>\r\n <head>\r\n  <meta content=\'text/html; charset=utf-8\' http-equiv=\'Content-Type\'>\r\n </head>\r\n <body style=\'background-color: #eeeeee; font-face: sans-serif; padding-top: 40px;\' alink=\'#000099\' link=\'#000099\' text=\'#000000\' vlink=\'#000099\'>\r\n   <table align=\'center\' bgcolor=\'#ffffff\' border=\'0\' cellpadding=\'5\' cellspacing=\'0\' width=\'80%\'>\r\n    <tbody>\r\n     <tr>\r\n       <td style=\'padding-left: 50px; padding-top: 20px; padding-bottom: 20px;\' valign=\'middle\'><h3 style=\'margin: 0px;\'><%title%></h3></td>\r\n       <td style=\'padding-right: 50px; padding-top: 20px; padding-bottom: 20px;\' align=\'right\' valign=\'top\'>\r\n       <img alt=\'<%prefix%>\' src=\'<%logo%>\' border=\'0\' height=\'100px\'></td>\r\n     </tr><tr>\r\n      <td style=\'border-top: 1px solid #cccccc; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' valign=\'top\'>\r\n        <%content%>\r\n      </td>\r\n    </tr><tr>\r\n      <td style=\'background-color: #666666; color: white; padding-top: 20px; padding-bottom: 20px; padding-left: 50px; padding-right: 50px;\' colspan=\'2\' nowrap=\'nowrap\' valign=\'top\'>\r\n    <span style=\'font-weight: bold; margin-right: 50px;\'><#brand#></span>\r\n   <span style=\'color: silver; margin-right: 50px;\'><#slogan#></span>\r\n    <a style=\'color: silver;\' href=\'http://<%domain%>\'><%domain%></a>\r\n       </td>\r\n     </tr>\r\n   </tbody>\r\n </table>\r\n <br>\r\n <div align=\'center\'><font color=\'#666666\' size=\'-2\'><a style=\'color: #666666;\' href=\'http://<%domain%>/login/recoveryform.php\'>� �� ����� ������</a>.</font></div>\r\n</body></html>','����������� ������'),(7,'page','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n    <script>\r\n        $(document).ready(function() {\r\n        $(\".expanded\").hide();\r\n        \r\n        $(\".expanded, .collapsed\").click(function() {\r\n            $(this).parent().children(\".expanded, .collapsed\").toggle();\r\n        });\r\n        });\r\n    </script>\r\n  </body>\r\n</html>','��������'),(8,'page_backup','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <!-- Bootstrap -->\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <link href=\"/vendor/twbs/bootstrap/dist/css/bootstrap.min.css\" rel=\"stylesheet\">\r\n  <!-- custom -->\r\n<style>\r\n  <%custom_style%>\r\n</style>\r\n\r\n\r\n    <link rel=\"stylesheet\" href=\"/lytebox/lytebox.css\" type=\"text/css\" media=\"screen\">\r\n\r\n    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->\r\n    <!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->\r\n    <!--[if lt IE 9]>\r\n      <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>\r\n      <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>\r\n    <![endif]-->\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','��������'),(11,'admin','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n    <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n\r\n    <%styles%>\r\n\r\n    <link rel=\"stylesheet\" href=\"/lytebox/lytebox.css\" type=\"text/css\" media=\"screen\">\r\n\r\n</head>\r\n<body>\r\n<nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n                <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <a class=\"navbar-brand\" href=\"/admin/\">Engine <small><%version%></small></a>\r\n        </div>\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                <%admin_menu%>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>\r\n\r\n<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-sm-3 col-md-2 sidebar\">\r\n            <p><a href=\'/admin/\'><img src=\"/images/design/engine_logo.png\"></a></p>\r\n            <h4><a href=\'/admin/\'>Engine <small><%version%></small></a></h4>\r\n            <div class=\"list-group\">\r\n                <%admin_menu_list%>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-9 col-md-10\"><br>\r\n            <%contains%>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<%scripts%>\r\n</body>\r\n</html>','�����-�����'),(15,'test_baidu','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n    <script type=\"text/javascript\">\r\n        var cse;\r\n        \r\n        //??????????\r\n        function display (data) {\r\n            console.log(data);\r\n        }\r\n    \r\n        //?????????\r\n        function init () {\r\n            cse = new BCse.Search(\"7099775790379624042\");    //?????API??ID??????????\r\n            \r\n            cse.getResult(\"???\", display);    //????????????1???????2??????????????????\r\n        }\r\n    \r\n        function loadScript () { \r\n            var script = document.createElement(\"script\"); \r\n            script.type = \"text/javascript\";\r\n            script.charset = \"utf-8\";\r\n            script.src = \"http://zhannei.baidu.com/api/customsearch/apiaccept?sid=7099775790379624042&v=2.0&callback=init\";\r\n            var s = document.getElementsByTagName(\'script\')[0];\r\n            s.parentNode.insertBefore(script, s);\r\n        }\r\n    \r\n        loadScript();\r\n    </script>\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>',''),(17,'page_slide','<!DOCTYPE html>\r\n<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n  <head>\r\n    <meta charset=\"utf-8\">\r\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n    <meta name=\"description\" content=\"<%description%>\"> \r\n    <meta name=\"Keywords\" content=\"<%keywords%>\"> \r\n    <link rel=\"image_src\" href=\"<%site_image%>\">\r\n    <meta property=\"og:image\" content=\"<%site_image%>\">\r\n    <%redirect_link%>\r\n    <title><%title_short%></title>\r\n\r\n  <link rel=\"shortcut icon\" type=\"image/vnd.microsoft.icon\" href=\"/favicon.ico\">\r\n  <%styles%>\r\n\r\n  </head>\r\n  <body>\r\n<div class=\"container\">\r\n			<div class=\"row\">\r\n  				<div class=\"col-md-10\">\r\n  					<div class=\"je_header_block\">\r\n  						<a href=\"http://mositong.com\">\r\n  							<img src=\"http://mositong.com/images/design/logo.png\" alt=\"<#brand#>\" height=\"100%\">\r\n  						</a>\r\n  						<div class=\"pull-right lang\">\r\n	  						<button type=\"button\" class=\"btn btn-default\">\r\n		          				<%lang_selector%>\r\n		      				</button>\r\n		      			</div>	\r\n					</div>\r\n  				</div>\r\n  				<div class=\"col-md-2\">\r\n  					<div class=\"je_weather_block\">\r\n  						<!-- weather widget start --><a href=\"//www.booked.net/weather/moscow-18171\"><img src=\"//w.bookcdn.com/weather/picture/23_18171_1_1_e74c3c_250_c0392b_ffffff_ffffff_1_2071c9_ffffff_0_3.png?scode=124&domid=\" /></a><!-- weather widget end -->\r\n					</div>\r\n  				</div>	\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<ul class=\"nav nav-pills\">\r\n  						 <%main_menu%>\r\n						</ul>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_main_block\">\r\n  						<div class=\"page-header\">\r\n  						<h1><%title_long%></small></h1>\r\n						</div>\r\n							<%contains%>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n  			<div class=\"row\">\r\n  				<div class=\"col-md-12\">\r\n  					<div class=\"je_footer_block\">\r\n  						<p class=\"pull-right\"><a class=\"wh\" href=\"/contacts\">??</a> � <a class=\"wh\" href=\"mailto:j.e.morrow@yandex.ru\">�</a></p>\r\n  						 <p>????? - ??????</p>\r\n  					</div>\r\n  				</div>\r\n  			</div>\r\n<div class=\"text-center\">\r\n<span class=\"dropdown\">\r\n    <button class=\"btn btn-default dropdown-toggle btn-sm\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-expanded=\"true\">\r\n    <span class=\"glyphicon glyphicon-user\" aria-hidden=\"true\"></span>\r\n    </button>\r\n    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu1\">\r\n        <%user_menu%> \r\n    </ul>\r\n</span>\r\n</div>\r\n		</div>\r\n		<!-- BEGIN JIVOSITE CODE {literal} -->\r\n		<script type=\'text/javascript\'>\r\n		(function(){ var widget_id = \'eipi75zhR1\';\r\n		var s = document.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true; s.src = \'//code.jivosite.com/script/widget/\'+widget_id; var ss = document.getElementsByTagName(\'script\')[0]; ss.parentNode.insertBefore(s, ss);})();</script>\r\n		<!-- {/literal} END JIVOSITE CODE -->\r\n    <!-- extra -->\r\n    <%scripts%>\r\n  </body>\r\n</html>','�������� � ���������');
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id_upload` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_upload`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (1,'o_19q1644vt1cig1uug12mr5uf1up59.jpg','o_19q1644vt1cig1uug12mr5uf1up59.jpg.thumb.jpg',4,1436706555),(2,'o_19q164dub13n1gl01jjs1ug9p8qe.jpg','o_19q164dub13n1gl01jjs1ug9p8qe.jpg.thumb.jpg',4,1436706556),(3,'o_19q17kvu240ue5s1j9nhpt10lb9.jpg','o_19q17kvu240ue5s1j9nhpt10lb9.jpg.thumb.jpg',4,1436708146),(4,'o_19q17lqcufqvsbu1401hd71fqvh.jpg','o_19q17lqcufqvsbu1401hd71fqvh.jpg.thumb.jpg',4,1436708173),(5,'o_19q189l5s1f0r1ars1io7174tsag9.jpg','o_19q189l5s1f0r1ars1io7174tsag9.jpg.thumb.jpg',4,1436708823),(6,'o_19q3ugg9k1gsd104a1td417o2nch9.png','o_19q3ugg9k1gsd104a1td417o2nch9.png.thumb.jpg',3,1436799233),(7,'o_19rj7elaqicu38g1qo21j59188q7.jpg','o_19rj7elaqicu38g1qo21j59188q7.jpg.thumb.jpg',3,1438385660),(8,'o_19rj7es1sp2a1pel16jh12ll1t10c.jpg','o_19rj7es1sp2a1pel16jh12ll1t10c.jpg.thumb.jpg',3,1438385666),(9,'o_19rk8do8kf5fb0o1mbf6i1ie0g.jpg','o_19rk8do8kf5fb0o1mbf6i1ie0g.jpg.thumb.jpg',4,1438420234),(10,'o_19rk8do8ldtp16u71bq34gj9j2h.jpg','o_19rk8do8ldtp16u71bq34gj9j2h.jpg.thumb.jpg',4,1438420234),(11,'o_19rk8do8ljok1gsk1ne71qqfeb3i.jpg','o_19rk8do8ljok1gsk1ne71qqfeb3i.jpg.thumb.jpg',4,1438420234),(12,'o_19rk8ek7h1u561dri12eh1llj17q110.jpg','o_19rk8ek7h1u561dri12eh1llj17q110.jpg.thumb.jpg',4,1438420266),(13,'o_19rk8gl2crsq14sass01fv51rcsd.jpg','o_19rk8gl2crsq14sass01fv51rcsd.jpg.thumb.jpg',3,1438420329),(14,'o_19rk8gl2c8qb1e0amnn1qm0jaie.jpg','o_19rk8gl2c8qb1e0amnn1qm0jaie.jpg.thumb.jpg',3,1438420330),(15,'o_19rk8gl2ctp3ugb1kmsf2u1uvdf.jpg','o_19rk8gl2ctp3ugb1kmsf2u1uvdf.jpg.thumb.jpg',3,1438420330),(16,'o_19rk8gl2c3nh18gm180atvh1ij9g.jpg','o_19rk8gl2c3nh18gm180atvh1ij9g.jpg.thumb.jpg',3,1438420331),(17,'o_19rk8gl2c8ul1o1u1u4n1h3412ouh.jpg','o_19rk8gl2c8ul1o1u1u4n1h3412ouh.jpg.thumb.jpg',3,1438420331),(18,'o_19rk8iq59lnbofk1blj1kl81kc915.jpg','o_19rk8iq59lnbofk1blj1kl81kc915.jpg.thumb.jpg',3,1438420398),(19,'o_19rk8lt8jh5g91una216vil1j1a.jpg','o_19rk8lt8jh5g91una216vil1j1a.jpg.thumb.jpg',3,1438420500),(20,'o_19rk8obfg30hk881q491tm012r71f.jpg','o_19rk8obfg30hk881q491tm012r71f.jpg.thumb.jpg',3,1438420580),(21,'o_19rkdnpff5h18ge13t3c6o1k879.jpg','o_19rkdnpff5h18ge13t3c6o1k879.jpg.thumb.jpg',3,1438425806),(22,'o_19rkdnpffd2u132i8nnjpg1rsma.jpg','o_19rkdnpffd2u132i8nnjpg1rsma.jpg.thumb.jpg',3,1438425809),(23,'o_19rkdnpff5pe4k5t4fmv910rb.jpg','o_19rkdnpff5pe4k5t4fmv910rb.jpg.thumb.jpg',3,1438425810),(24,'o_19rkg7sit1dum1j9u1j5c1qj9o7nm.jpg','o_19rkg7sit1dum1j9u1j5c1qj9o7nm.jpg.thumb.jpg',3,1438428430),(25,'o_19rkgfpno1j8718icsk4uia6g9r.jpg','o_19rkgfpno1j8718icsk4uia6g9r.jpg.thumb.jpg',3,1438428689),(26,'o_19rkhskev17jhc6h1n0fg4t1aoi7.jpg','o_19rkhskev17jhc6h1n0fg4t1aoi7.jpg.thumb.jpg',3,1438430159),(27,'o_19rki2nbc1hq41vqo1qsd1njdvsu7.jpg','o_19rki2nbc1hq41vqo1qsd1njdvsu7.jpg.thumb.jpg',3,1438430357),(28,'o_19rkicejj14nleu311dd1p0i6lgc.jpg','o_19rkicejj14nleu311dd1p0i6lgc.jpg.thumb.jpg',3,1438430676),(29,'o_19rkifmv5191v1an5ip9182i1ri07.jpg','o_19rkifmv5191v1an5ip9182i1ri07.jpg.thumb.jpg',3,1438430783),(30,'o_19rkikor8dvsavp1mvb1bgt15sc.jpg','o_19rkikor8dvsavp1mvb1bgt15sc.jpg.thumb.jpg',3,1438430949),(31,'o_19rkivprn1ujp1r9e13dc2t919dr7.jpg','o_19rkivprn1ujp1r9e13dc2t919dr7.jpg.thumb.jpg',3,1438431310),(32,'o_19rkj1vvm1o521orv180v18c71fr3c.jpg','o_19rkj1vvm1o521orv180v18c71fr3c.jpg.thumb.jpg',3,1438431381),(33,'o_19rkl3iae166dj41amgp8c9u47.jpg','o_19rkl3iae166dj41amgp8c9u47.jpg.thumb.jpg',3,1438433532),(34,'o_19rktmte6106m139bjp61hor15bp7.jpg','o_19rktmte6106m139bjp61hor15bp7.jpg.thumb.jpg',3,1438442553),(35,'o_19rku8o2bis51st214g9n4v8527.jpg','o_19rku8o2bis51st214g9n4v8527.jpg.thumb.jpg',3,1438443138),(36,'o_19rkucfgd71c19nm1l971kjr165lc.jpg','o_19rkucfgd71c19nm1l971kjr165lc.jpg.thumb.jpg',3,1438443259),(37,'o_19rkudj6k15cr11ri1dddo951qi2h.jpg','o_19rkudj6k15cr11ri1dddo951qi2h.jpg.thumb.jpg',3,1438443295),(38,'o_19rkvdj5e1rd81p9r1hn33aiug7.jpg','o_19rkvdj5e1rd81p9r1hn33aiug7.jpg.thumb.jpg',3,1438444344),(39,'o_19rkvkldc86agtrah91vcq4a6c.jpg','o_19rkvkldc86agtrah91vcq4a6c.jpg.thumb.jpg',3,1438444576),(40,'o_19rkvqf6t157tqapip51rh016b0h.jpg','o_19rkvqf6t157tqapip51rh016b0h.jpg.thumb.jpg',3,1438444766),(41,'o_19rl13el61je6110boavj1g1kih7.jpg','o_19rl13el61je6110boavj1g1kih7.jpg.thumb.jpg',3,1438446110),(42,'o_19rl188r7cqecmic945h314ma7.jpg','o_19rl188r7cqecmic945h314ma7.jpg.thumb.jpg',3,1438446267),(43,'o_19rled6k71po71t4ev4b14r11lebc.jpg','o_19rled6k71po71t4ev4b14r11lebc.jpg.thumb.jpg',3,1438460061),(44,'o_19rled6k71643djh1u011obr1ic8d.jpg','o_19rled6k71643djh1u011obr1ic8d.jpg.thumb.jpg',3,1438460061),(45,'o_19rled6k71i0t1u731md11d997gle.jpg','o_19rled6k71i0t1u731md11d997gle.jpg.thumb.jpg',3,1438460062),(46,'o_19rled6k717jm13n91ma016bbmkgf.jpg','o_19rled6k717jm13n91ma016bbmkgf.jpg.thumb.jpg',3,1438460062),(47,'o_19rled6k7uj7102mtjd1fjg41ng.jpg','o_19rled6k7uj7102mtjd1fjg41ng.jpg.thumb.jpg',3,1438460063),(48,'o_19rled6k71685b7r11muslmtabh.jpg','o_19rled6k71685b7r11muslmtabh.jpg.thumb.jpg',3,1438460064),(49,'o_19rmn3g6m5711n8fca31dem17ij7.jpg','o_19rmn3g6m5711n8fca31dem17ij7.jpg.thumb.jpg',3,1438502734),(50,'o_19rmnhv13maquloung776plca.jpg','o_19rmnhv13maquloung776plca.jpg.thumb.jpg',3,1438503208),(51,'o_19rmnhv131i6pc441doc7t31g9kb.jpg','o_19rmnhv131i6pc441doc7t31g9kb.jpg.thumb.jpg',3,1438503209),(52,'o_19rmnhv13q2n1lc41ei5m8c1slbc.jpg','o_19rmnhv13q2n1lc41ei5m8c1slbc.jpg.thumb.jpg',3,1438503210),(53,'o_19rmnhv138471ci11mp61rji2rd.jpg','o_19rmnhv138471ci11mp61rji2rd.jpg.thumb.jpg',3,1438503211);
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id_user` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_password` varchar(255) NOT NULL DEFAULT '',
  `user_email` varchar(255) NOT NULL DEFAULT '',
  `user_name` varchar(255) NOT NULL DEFAULT '',
  `tries` int(11) NOT NULL DEFAULT '0',
  `date_last_try` int(11) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `lang_code` varchar(2) NOT NULL DEFAULT 'cn',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (2,'1ab4f0253ac67be0d7ffca1cfcb424e8','robin_tail@me.com','robin',0,0,1,'cn'),(3,'ead09a0f7accc3ee1d3874aa5f92a3a7','j.e.morrow@yandex.ru','galahed',0,0,1,'cn'),(4,'37e531f32473be47b911d5f10d16ad04','info@mositong.com','Mositong',0,0,1,'cn');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-09 13:16:16
