<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 12:25
 */

use kernel\Output\Scripts;

include("admin_init.php");

if (kernel\Input::get()->readInt('update'))
{
    if (kernel\Style::get()->isExists(
        kernel\Input::get()->read('style_code'),
        kernel\Input::get()->readInt('id_style')
    )) {
        kernel\Output::get()->error("<#error-style-code-exists#>");
    }

    kernel\Style::get()->write(
        kernel\Style\Item::create()
            ->writeId(kernel\Input::get()->readInt('id_style'))
            ->writeCode(kernel\Input::get()->read('style_code'))
            ->writeLink(kernel\Input::get()->read('style_link'))
            ->writeText(kernel\Input::get()->read('style_text'))
            ->writeDescription(kernel\Input::get()->read('style_description'))
    );
    kernel\Output::get()->redirect('/admin/styles.php');
}



$style = kernel\Style::get()->readById(kernel\Input::get()->readInt('id_style'));

$html = <<<HTM

    <form action='' method='post' onsubmit="ace_fetch('style_text', 'style_text_ace');">
        <input type='hidden' name='id_style' value='{$style->readId()}'>
        <input type='hidden' name='update' value='1'>
        <div class='form-group has-feedback'>
			<label class='control-label'><#code#></label>
			<input class='form-control' name='style_code' value='{$style->readCode()}' onchange="check_style_code();">
			<span class="glyphicon form-control-feedback"></span>
			<div class='help-block'></div>
		</div>
		<div class='form-group'>
			<label><#description#></label>
			<input class='form-control' name='style_description' value='{$style->readDescription()}'>
		</div>
        <div class='form-group'>
            <label><#link-to-css#></label>
            <p class='help-block'><#link-to-css-help#></p>
            <input class='form-control' name='style_link' value='{$style->readLink()}'>
        </div>
        <div class='form-group'>
            <label><#or#> <#style-text#></label>
            <p class='help-block'><#style-text-help#></p>
            <textarea style='display: none;' id='style_text' name='style_text'></textarea>
			<div id='style_text_ace'>{$style->readTextForEditor()}</div>
        </div>
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
    </form>

HTM;

Scripts::get()->addScript("<script src='/admin/style_edit.js'></script>");
kernel\Output::get()->writeOption('contains', $html)
                    ->render();