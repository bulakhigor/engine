<?

use kernel\Output\Scripts;

include("admin_init.php");


if (kernel\Input::get()->readInt('update'))
{
    if (kernel\Lang\Constant::get()->isExists(
        kernel\Input::get()->read('const_code'),
        kernel\Input::get()->readInt('id_const')
    )){
        kernel\Output::get()->error("<#error-lang-const-code-exists#>");
    }

    kernel\Lang\Constant::get()->write(
        kernel\Input::get()->readInt('id_const'),
        kernel\Input::get()->read('const_code'),
        kernel\Input::get()->read('translations')
    );
	kernel\Output::get()->redirect('/admin/language_const.php');
}


$const = \kernel\Lang\Constant::get()->readById(kernel\Input::get()->readInt('id_const'));
$id = $const->readId();
$code = $const->readCode();

$html = <<<HTM
	<form action='' method='post'>
		<input type='hidden' name='id_const' value='{$id}'>
		<input type='hidden' name='update' value='1'>
		<div class='form-group has-feedback'>
			<label class='control-label'><#code#></label>
			<input class='form-control' name='const_code' value='{$code}' onchange="check_const_code();">
			<span class="glyphicon form-control-feedback"></span>
			<div class='help-block'></div>
		</div>
HTM;

foreach(kernel\Lang::get()->readAll() as $entry)
{
    if ($entry->readIsEnabled()) {
        $html .= "
		<div class='form-group'>
			<label>" . $entry->readName() . "</label>
			<input class='form-control' name='translations[" . $entry->readCode() . "]' value='" . $const->readTrans($entry->readCode()) . "'>
		</div>";
    }
}

$html .= <<<HTM
		<button type='submit' class='btn btn-lg btn-primary'><#save#></button>
	</form>
HTM;
Scripts::get()->addScript("<script src='/admin/language_const_edit.js'></script>");
kernel\Output::get()->writeOption('contains', $html)
                    ->render();