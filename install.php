<?

// display errors
ini_set('display_errors',true);

// errors are equals to all except notices
Error_Reporting(E_ALL & ~E_NOTICE);

// locale is RU
setlocale(LC_ALL, 'ru_RU');

// time zone is Moscow
date_default_timezone_set('Europe/Moscow');

// change current directory to root
chdir(dirname(__FILE__));

// check the database settings exists
if (!file_exists("kernel/DB/Settings.php")) die("Please rename file kernel/DB/Settings_default.php to kernel/DB/Settings.php and edit it before run this script.");

// require classes for installation
include_once("kernel/Path.php");
include_once("kernel/DB/Settings.php");
include_once("kernel/Output/CLI.php");
include_once("kernel/Install.php");

// do the installation
kernel\Install::get()->proceed();
