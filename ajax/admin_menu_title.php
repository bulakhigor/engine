<?

include("admin_init.php");

if (!kernel\Input::get()->readInt('id_link')) kernel\Ajax::error(kernel\Output::get()->parseTranslate("id_link - <#error-smth-not-specified#>"));
if (!kernel\Input::get()->read('link_title_short')) kernel\Ajax::error(kernel\Output::get()->parseTranslate("link_title_short - <#error-smth-not-specified#>"));

kernel\Menu::get()->updateTextItemTitle(kernel\Input::get()->readInt('id_link'), kernel\Input::get()->read('link_title_short'));

kernel\Ajax::message('OK');