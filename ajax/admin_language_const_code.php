<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 23.06.15
 * Time: 18:19
 */

include("admin_init.php");

$code = kernel\Input::get()->read('const_code');
$id = kernel\Input::get()->readInt('id_const');

if (!$code) kernel\Ajax::error(kernel\Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));

if (kernel\Lang\Constant::get()->isExists($code, $id)) {
    kernel\Ajax::error(kernel\Output::get()->parseTranslate("<#error-lang-const-code-exists#>"));
} else {
    kernel\Ajax::message("OK");
}
