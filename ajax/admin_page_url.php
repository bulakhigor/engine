<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 14.06.15
 * Time: 23:42
 *
 * Checks the defined page_url argument is not reserved by file, directory or other page in DB
 * Also uses argument id_page to exclude this page from check
 */

include("admin_init.php");

$url = kernel\Input::get()->read('page_url');
$id = kernel\Input::get()->readInt('id_page');

if (!$url) kernel\Ajax::error(kernel\Output::get()->parseTranslate('<#error-url-not-specified#>'));
if (file_exists(kernel\Path::resolve($url))) kernel\Ajax::error(kernel\Output::get()->parseTranslate('<#error-file-or-directory-exists#>'));

if (kernel\Page::get()->isExists($url, $id))
{
    kernel\Ajax::error(kernel\Output::get()->parseTranslate('<#error-page-url-exists#>'));
} else {
    kernel\Ajax::message("OK");
}