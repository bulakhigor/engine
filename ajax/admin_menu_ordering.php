<?

include("admin_init.php");

if (!kernel\Input::get()->readInt('id_link')) kernel\Ajax::error(kernel\Output::get()->parseTranslate("id_link - <#error-smth-not-specified#>"));
if (!kernel\Input::get()->exist('link_ordering')) kernel\Ajax::error(kernel\Output::get()->parseTranslate("link_ordering - <#error-smth-not-specified#>"));

kernel\Menu::get()->updateItemOrdering(kernel\Input::get()->readInt('id_link'), kernel\Input::get()->readInt('link_ordering'));

kernel\Ajax::message('OK');