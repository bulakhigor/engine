<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 17.07.15
 * Time: 17:02
 */


include("admin_init.php");

$code = kernel\Input::get()->read('lang_code');
$id = kernel\Input::get()->readInt('id_lang');

if (!$code) kernel\Ajax::error(kernel\Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));

if (kernel\Lang::get()->isExists($code, $id)) {
    kernel\Ajax::error(kernel\Output::get()->parseTranslate("<#error-lang-code-exists#>"));
} else {
    kernel\Ajax::message("OK");
}
