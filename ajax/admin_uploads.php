<?

include("admin_init.php");

$pre = kernel\Upload::get()->readSome(
    kernel\Input::get()->read('period'),
    kernel\Input::get()->readInt('from')
);
$result = array();
foreach($pre as $item)
{
    $result[] = array(
                    'id' => $item->readId(),
                    'src' => $item->readSrc(),
                    'thumb' => $item->readThumb()
    );
}

kernel\Ajax::json($result);
