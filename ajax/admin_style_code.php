<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 06:57
 */

include("admin_init.php");

$code = kernel\Input::get()->read('style_code');
$id = kernel\Input::get()->readInt('id_style');

if (!$code) kernel\Ajax::error(kernel\Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));

if (kernel\Style::get()->isExists($code, $id)) {
    kernel\Ajax::error(kernel\Output::get()->parseTranslate("<#error-style-code-exists#>"));
} else {
    kernel\Ajax::message("OK");
}
