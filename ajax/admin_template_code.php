<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 23.06.15
 * Time: 17:53
 */

include("admin_init.php");

$code = kernel\Input::get()->read('template_code');
$id = kernel\Input::get()->readInt('id_template');

if (!$code) kernel\Ajax::error(kernel\Output::get()->parseTranslate("code - <#error-smth-not-specified#>"));

if (kernel\Template::get()->isExists($code, $id)) {
    kernel\Ajax::error(kernel\Output::get()->parseTranslate("<#error-template-code-exists#>"));
} else {
    kernel\Ajax::message("OK");
}
