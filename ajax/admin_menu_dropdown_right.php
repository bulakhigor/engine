<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 18.08.15
 * Time: 20:48
 */


include("admin_init.php");

if (!kernel\Input::get()->readInt('id_link')) kernel\Ajax::error(kernel\Output::get()->parseTranslate("id_link - <#error-smth-not-specified#>"));
if (!kernel\Input::get()->exist('is_dropdown_right')) kernel\Ajax::error(kernel\Output::get()->parseTranslate("is_dropdown_right - <#error-smth-not-specified#>"));

kernel\Menu::get()->updateItemDropdownRight(kernel\Input::get()->readInt('id_link') , kernel\Input::get()->readBool('is_dropdown_right'));

kernel\Ajax::message('OK');
