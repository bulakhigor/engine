<?php
spl_autoload_register(function($className) {
    if (file_exists(__DIR__.'/'.str_replace('\\', '/', $className).'.php')) {
        require __DIR__ . '/' . str_replace('\\', '/', $className) . '.php';
    }
});
