<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 26.06.15
 * Time: 09:27
 */

namespace kernel;


class Legacy {

    /**
     * PHP 5.3 Legacy: function()['key']
     * @param array $array
     * @param mixed $key
     * @return mixed
     */
    public static function array_fetch($array, $key)
    {
        return $array[$key];
    }

    /**
     * @param bool $assert
     * @param string $description
     * @throws \Exception
     */
    public static function assert($assert, $description)
    {
        $backtrace = debug_backtrace();
        $initiator = $backtrace[1];
        if (!$assert) throw new \Exception($initiator['file'].":".$initiator['line']."  ".$description);
    }

}