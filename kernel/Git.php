<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 07.07.15
 * Time: 16:58
 */

namespace kernel;


class Git extends \SebastianBergmann\Git\Git {

    public function __construct()
    {
        if (ini_get('safe_mode'))
        {
            throw new \Exception("PHP is in safe mode. Can not execute git commands.");
        }
        parent::__construct(Path::getRoot());
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        try{
            $tmp = parent::execute('git --version');
            preg_match('/git version ([\d\.]+)/', $tmp[0], $matches);
            $result = $matches[1];
        } catch (\Exception $e)
        {
            $result = "FAILED ".$e->getMessage();
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getCurrentBranch()
    {
        if (version_compare($this->getVersion(), '1.7.10', '>=')) {
            try {
                return parent::getCurrentBranch();
            } catch (\Exception $e) {
            }
        }
        return self::getCurrentBranchAlternative();
    }

    /**
     * @return string
     */
    private function getCurrentBranchAlternative()
    {
        try{
            $tmp = parent::execute('git status');
            preg_match("/On branch (.+)/siu", $tmp[0], $matches);
            $result = $matches[1];
        } catch(\Exception $e) {
            $result = "FAILED ".$e->getMessage();
        }
        return $result;
    }

    /**
     * @return array
     */
    public function pull()
    {
        return parent::execute("git pull");
    }

    /**
     * @param int $count
     * @return array
     */
    public function getLastRevisions($count=3)
    {
        try {
            $revisions = array_slice(array_reverse(parent::getRevisions()), 0, $count);
        } catch (\Exception $e)
        {
            $revisions = array(array('sha1' => 'FAILED', 'message' => $e->getMessage()));
        }
        return Arr2dim::kv($revisions, 'sha1', 'message');
    }

    /**
     * @return string|bool
     */
    public function getCurrentCommit()
    {
        try{
            $result = parent::execute('git log -1');
        } catch (\Exception $e) {
            return false;
        }
        preg_match("/commit\s+([0-9a-f]+)/siu", $result[0], $matches);
        return $matches[1];
    }

    /**
     * @param string $commitHash
     * @return string
     */
    public function resetHard($commitHash)
    {
        try
        {
            $result = parent::execute('git reset --hard '.$commitHash);
        } catch (\Exception $e)
        {
            return "FAILED ".$e->getMessage();
        }
        return implode($result, "\r\n");
    }

} 