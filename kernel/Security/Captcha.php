<?php

namespace kernel\Security;


use kernel\DB;
use kernel\Path;

class Captcha implements Captcha\methods
{
    public static $symbols = 6;
    public static $width=200;
    public static $height=80;
    public static $digits=false; // will be loaded
    public static $digit_height=false; // will be loaded

    public static function loadDigits()
    {
        self::$digits = array();
        $file = @imagecreatefrompng(\kernel\Path::resolve(\kernel\Settings::get()->readValue('captcha_font_file')));
        if (!$file) die("captcha: could not load font ".\kernel\Path::resolve(\kernel\Settings::get()->readValue('captcha_font_file')));
        $lastx=-1;
        for($x=0;$x<imagesx($file);$x++)
        {
            $color_array = imagecolorsforindex($file, imagecolorat($file,$x,0) );
            if ( (($color_array['red']==255) && ($color_array['green']==0) && ($color_array['blue']==0)) || ($x==imagesx($file)-1) )
            {
                $tmp = imagecreate($x-$lastx-1,imagesy($file));
                imagecopy($tmp,$file,0,0,$lastx+1,0,$x-$lastx-1,imagesy($file));
                imagecolortransparent($tmp, imagecolorat($tmp,0,0));
                self::$digits[] = $tmp;
                $lastx=$x;
            }
        }
        self::$digit_height = imagesy($file);
        imagedestroy($file);
    }





    public static function get($rid)
    {
        \kernel\DB::get()->prepare("SELECT code FROM captcha WHERE id_captcha=?")
                        ->bind(1, $rid)
                        ->execute();
        if ($row=\kernel\DB::get()->fetchRow())
        {
            return $row['code'];
        } else {
            return false;
        }
    }

    
    
    
    public static function check($rid, $code)
    {
        if (!($rid  && $code)) return false;
        self::kill(); // kill old
        $real_code = self::get($rid);
        if ($real_code)
        {
            if ($real_code == $code)
            {
                return true;
            }
        }
        return false;
    }

    public static function set()
    {
        $code = "";
        for ($i=0;$i<self::$symbols;$i++)
        {
            $code .= mt_rand(0,9);
        }
        \kernel\DB::get()->prepare("INSERT INTO captcha (date_create_unix,code)
                                    VALUES (?,?)")
                        ->bind(1, time())
                        ->bind(2, $code)
                        ->execute();
        \kernel\DB::get()->prepare("SELECT id_captcha
                                    FROM captcha
                                    WHERE code=?
                                    ORDER BY ID_Captcha DESC
                                    LIMIT 0,1")
                        ->bind(1, $code)
                        ->execute();
        if ($row=\kernel\DB::get()->fetchRow())
        {
            return $row['id_captcha'];
        } else {
            return false;
        }
    }

    private static function kill()
    {
        $lifetime = 60*30; // 30 minutes
        $deadline = time() - $lifetime;
        \kernel\DB::get()->prepare("DELETE FROM captcha
                                    WHERE date_create_unix <= ?")
                        ->bind(1, $deadline)
                        ->execute();
        return true;
    }


}
