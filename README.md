![logo_white_600.png](https://bitbucket.org/repo/kAj7ga/images/2376797315-logo_white_600.png)




# ENGINE #

Thank you for your interest to this website cosy engine.

## Requirements ##

* PHP >= 5.4.8 (with gd, iconv and PDO)
* MySQL

## Dependencies ##

Third-part repositories:

* Bootstrap (*/vendor/twbs/bootstrap*)
* jQuery (*/vendor/components/jquery*)
* tinyMCE (*/vendor/tinymce/tinymce*)
* plUpload (*/vendor/moxiecode/plupload*)
* phpUnit (*/vendor/phpunit/phpunit*)

Third-part packages:

* Lytebox (*/vendor/markusfhay/lytebox*)
* Ace (*/vendor/ajaxorg/ace-builds*)

## Installation ##

Via SSH connection simply execute this commands in the empty website directory:

```
#!bash
git clone --no-checkout git@bitbucket.org:robintail/engine.git .
git checkout master
```

After that execute this command to rename default settings file:

```
#!bash
mv kernel/DB/Settings_default.php kernel/DB/Settings.php
```

Then edit this file, for example with nano:

```
#!bash
nano kernel/DB/Settings.php
```

This file contains code like this:

```
#!php
<?
namespace kernel\DB;

/**
 * Class Settings
 * @package kernel\DB
 */
class Settings
{
    /**
     * Specify your MySQL server host
     * For localhost please use 127.0.0.1
     * @var string
     */
    protected static $host = '127.0.0.1';

    /**
     * Specify your MySQL server port
     * Default is 3306
     * @var string
     */
    protected static $port = '3306';

    /**
     * Specify your MySQL user name
     * @var string
     */
    protected static $user = 'root';

    /**
     * Specify your MySQL user password
     * @var string
     */
    protected static $pass = 'root';

    /**
     * Specify your MySQL database name
     * @var string
     */
    protected static $base = 'engine';

    /**
     * If specified database does not exist set this property to false.
     * Installer will create this database.
     * @var bool
     */
    protected static $baseIsExists = true;
}
```

Edit these options, close with Ctrl+X, agree to save changes with Y and ENTER.
Execute installer CLI script with this command:

```
#!bash
php install.php <adminName> <adminEmail> <adminPassword>
```

### Tune ###

In browser follow the website root. Click on the dropdown link in the main menu and log in. Go to Admin CP. Go to System Settings and edit their values.

## Runtime error reporting ##

By default all errors are not shown to your website users for security reasons. Last error information can be found in the file "/tmp/last_error.txt". Also, this information will be send to your email every 15 minutes. Engine uses the system setting "email_tech", which is automatically set up equals to your admin account email, provided while setup process, but you can change it.

## Updates ##

You can easily update your Engine by using self-update CLI script via SSH. Self-update script will automatically update your code using git, backup your database to snapshot file, execute migration SQL scripts and tests. In case of failure it will automatically restore your database from the snapshot file and restore your code to commit before update started.

```
#!bash
php selfupdate.php <adminEmail> <adminPassword>
```

## Tests ##

Tests are already configured in file 'phpunit.xml' and can be executed with update process.

## Contact ##

Email me to: robin_tail@me.com