<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 20:57
 */

namespace kernel;


class pageTest extends \PHPUnit_Framework_TestCase {
    private $testHtml = '<div style="background-color: white"><p>Hello, <span>world</span></p></div>';
    private $testHtmlUncoverted = '&lt;div style="background-color: white"&gt;&lt;p&gt;Hello, &lt;span&gt;world&lt;/span&gt;&lt;/p&gt;&lt;/div&gt;';


    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testLoading()
    {
        $page = Page::get();
        $this->assertTrue(is_object($page), "Page can not be loaded");
    }

    public function testReadAllReturnsNonEmptyArray()
    {
        $array = Page::get()->readAll();
        $this->assertTrue(count($array)>0, "Pages are empty");
    }

    public function testIndexPageIsExists()
    {
        $url = Settings::get()->readValue('index_page');
        $this->assertTrue(Page::get()->isExists($url, 0), "index page is not exists");
    }

    public function testReadByUrlWorks()
    {
        $url = Settings::get()->readValue('index_page');
        $page = Page::get()->readByUrl($url);
        $this->assertTrue($page->readUrl() === $url);
    }

    public function testReadIdInteger()
    {
        $url = Settings::get()->readValue('index_page');
        $page = Page::get()->readByUrl($url);
        $this->assertTrue(is_numeric($page->readId()) && $page->readId()>0);
    }

    public function testReadByIdWorks()
    {
        $url = Settings::get()->readValue('index_page');
        $page = Page::get()->readByUrl($url);
        $this->assertTrue(Page::get()->readById($page->readId())->readUrl() === $url);
    }

    public function testWriteHtmlWorks()
    {
        $page = Page\Item::create()->writeUrl('phpunit_test_'.md5(time()))
                            ->writeTrans(
                                Lang::get()->getUsing(),
                                Page\Item\Trans::create()
                                    ->writeDescription('this is phpunit test')
                                    ->writeHtml($this->testHtml)
                            );
        $this->assertTrue($page->readTrans(Lang::get()->getUsing())->readHtml() === $this->testHtml, $page->readTrans(Lang::get()->getUsing())->readHtml());
    }

    public function testReadHtmlForEditorWorks()
    {
        $page = Page\Item::create()->writeUrl('phpunit_test_'.md5(time()))
                                    ->writeTrans(
                                        Lang::get()->getUsing(),
                                        Page\Item\Trans::create()
                                            ->writeDescription('this is phpunit test')
                                            ->writeHtml($this->testHtml)
                                    );
        $this->assertTrue($page->readTrans(Lang::get()->getUsing())->readHtmlForEditor() === $this->testHtmlUncoverted, $page->readTrans(Lang::get()->getUsing())->readHtmlForEditor());
    }

    public function testWriteWorks()
    {
        $page = Page\Item::create()->writeUrl('phpunit_test_'.md5(time()))
            ->writeTrans(
                Lang::get()->getUsing(),
                Page\Item\Trans::create()
                    ->writeDescription('this is phpunit test')
                    ->writeHtml($this->testHtml)
            );
        Page::get()->write($page);
        $this->assertTrue($page->readId() > 0);
    }

}
 