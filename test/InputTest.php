<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 07.07.15
 * Time: 22:28
 */

namespace kernel;



class inputTest extends \PHPUnit_Framework_TestCase {

    public function testInputIsEmptyInitial()
    {
        $this->assertTrue( count( Input::get()->readAll() ) === 0 , "Initial array of input is not empty");
    }

    public function testWrite()
    {
        Input::get()->write('int', 15);
        Input::get()->write('float', 3.1415);
        Input::get()->write('string', 'test');
        Input::get()->write('bool', true);
        Input::get()->write('null', null);
        Input::get()->write('zero', 0);
        Input::get()->write('int_array', array(15, 3.1415, 'test'));
        Input::get()->write('float_array', array(15, 3.1415, 'test'));
    }

    public function testRead()
    {
        $this->assertTrue(Input::get()->read('string')==='test');
    }

    public function testReadInt()
    {
        $this->assertTrue(Input::get()->readInt('int')===15);
    }

    public function testReadIntOfFloat()
    {
        $this->assertTrue(Input::get()->readInt('float')===3);
    }

    public function testReadIntOfString()
    {
        $this->assertTrue(Input::get()->readInt('string')===0);
    }

    public function testReadFloat()
    {
        $this->assertTrue(Input::get()->readFloat('float')===3.1415);
    }

    public function testReadFloatOfString()
    {
        $this->assertTrue(Input::get()->readFloat('string')===0.0);
    }

    public function testReadIntOrNullOfFloat()
    {
        $this->assertTrue(Input::get()->readIntOrNull('float')===3);
    }

    public function testReadIntOrNullOfString()
    {
        $this->assertTrue(Input::get()->readIntOrNull('string')===null);
    }

    public function testReadIntArray()
    {
        $this->assertTrue(Input::get()->readIntArray('int_array')===array(15, 3, 0));
    }

    public function testReadIntArrayOfString()
    {
        $this->assertTrue(Input::get()->readIntArray('string')===array());
    }

    public function testReadFloatArray()
    {
        $this->assertTrue(Input::get()->readFloatArray('float_array')===array(15.0, 3.1415, 0.0));
    }

    public function testReadFloatArrayOfString()
    {
        $this->assertTrue(Input::get()->readFloatArray('string')===array());
    }

    public function testReadBool()
    {
        $this->assertTrue(Input::get()->readBool('bool')===true);
    }

    public function testReadBoolOfString()
    {
        $this->assertTrue(Input::get()->readBool('string')===true);
    }

    public function testReadBoolOfNull()
    {
        $this->assertTrue(Input::get()->readBool('null')===false);
    }

    public function testReadBoolOfZero()
    {
        $this->assertTrue(Input::get()->readBool('zero')===false);
    }

    public function testExist()
    {
        $this->assertTrue(Input::get()->exist('string')===true);
    }

    public function testKill()
    {
        Input::get()->kill('string');
        $this->assertTrue(Input::get()->exist('string')===false);
    }

}
 