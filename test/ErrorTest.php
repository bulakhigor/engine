<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 20.06.15
 * Time: 03:31
 */

namespace kernel;


class errorTest extends \PHPUnit_Framework_TestCase {

    public function testErrorThrowException()
    {
        $this->setExpectedException("ErrorException");
        $d=1/0;
    }

    public function testErrorLogDirExist()
    {
        $this->assertTrue(is_dir(Error::getLogDir()), Error::getLogDir());
    }

    public function testErrorLogWritable()
    {
        try {
            $written = fwrite(fopen(Error::getLogFile(), 'w'), 'PHP Unit test applied');
        } catch (\Exception $e) {
            throw $e;
        }
        $this->assertTrue($written!==false);
    }

    public function testEmailFlagWritable()
    {
        try {
            $written = fwrite(fopen(Error::getEmailFlag(), 'w'), 'PHP Unit test applied');
        } catch (\Exception $e) {
            throw $e;
        }
        $this->assertTrue($written!==false);
    }


}
 