<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 15.07.15
 * Time: 12:14
 */

namespace kernel;


class securityTest extends \PHPUnit_Framework_TestCase {

    public function testGetPasswordLengthIsMoreThanZero()
    {
        $this->assertTrue(Security::getPasswordLength() > 0);
    }

    public function testCryptPasswordWorks()
    {
        $pwd = 'test';
        $salt = PHPunit::getPrivateProperty(new Security(), 'salt');
        $this->assertTrue(Security::cryptPassword($pwd) === md5(md5($pwd).$salt));
    }

    public function testGetRecoveryHashWorks()
    {
        $cryptedPassword = 'phpunit_test';
        $test = md5(md5($cryptedPassword).strrev($cryptedPassword).strrev(md5($cryptedPassword)).date("d", time()));
        $this->assertTrue(Security::getRecoveryHash($cryptedPassword) === $test);
    }

}
 