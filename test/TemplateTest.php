<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 30.06.15
 * Time: 12:39
 */

namespace kernel;


class templateTest extends \PHPUnit_Framework_TestCase {

    private $testHtml = '<html><body><#contains#></body></html>';
    private $testHtmlUncoverted = '&lt;html&gt;&lt;body&gt;&lt;#contains#&gt;&lt;/body&gt;&lt;/html&gt;';

    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testLoading()
    {
        $obj = Template::get();
        $this->assertTrue(is_object($obj), "Template can not be loaded");
    }

    public function testReadAllReturnsNonEmptyArray()
    {
        $array = Template::get()->readAll();
        $this->assertTrue(count($array)>0, "Templates are empty");
    }

    public function testDefaultTemplateExists()
    {
        $test = Template::get()->isExists(Template::get()->getDefaultCode(), 0);
        $this->assertTrue($test, "Default template does not exist.");
    }

    public function testReadByCodeWorks()
    {
        $template = Template::get()->readByCode(Template::get()->getDefaultCode());
        $this->assertTrue($template->readCode() === Template::get()->getDefaultCode(), "getByCode gets wrong template");
    }

    public function testReadIdInteger()
    {
        $template = Template::get()->readByCode(Template::get()->getDefaultCode());
        $this->assertTrue(is_numeric($template->readId()) && $template->readId()>0 , "readId returns invalid value");
    }

    public function testReadByIdWorks()
    {
        $id = Template::get()->readByCode(Template::get()->getDefaultCode())->readId();
        $this->assertTrue(Template::get()->readById($id)->readCode() === Template::get()->getDefaultCode(), "getById gets wrong template");
    }

    public function testWriteHtmlWorks()
    {
        $template = Template\Item::create()->writeCode('phpunit_test_'.md5(time()))
                                    ->writeDescription('this is the phpunit test')
                                    ->writeHtml($this->testHtml);
        $this->assertTrue($template->readHtml() === $this->testHtml, "writeHtml does not convert html properly ".$template->readHtml());
    }

    public function testReadHtmlForEditorWorks()
    {
        $template = Template\Item::create()->writeCode('phpunit_test_'.md5(time()))
            ->writeDescription('this is the phpunit test')
            ->writeHtml($this->testHtml);
        $this->assertTrue($template->readHtmlForEditor() === $this->testHtmlUncoverted, "readHtmlForEditor does not convert html properly ".$template->readHtmlForEditor());
    }

    public function testReadStylesIsIntArray()
    {
        $template = Template::get()->readByCode(Template::get()->getDefaultCode());
        $styles = $template->readStyles();
        $this->assertTrue($styles === Regex::arrayIntval($styles, false), "Styles are not array of int ".var_export($styles, true).var_export(Regex::arrayIntval($styles, false), true));
    }

    public function testWrite()
    {
        $template = Template\Item::create()
                    ->writeCode('phpunit_test_'.md5(time()))
                    ->writeDescription('this is the phpunit test')
                    ->writeHtml($this->testHtml);
        Template::get()->write($template);
        $this->assertTrue($template->readId() > 0 , "save does not work");
    }

}
 