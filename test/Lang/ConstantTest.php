<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 17.07.15
 * Time: 15:27
 */

namespace kernel;


class constantTest extends \PHPUnit_Framework_TestCase {


    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testReadAllReturnArray()
    {
        $this->assertTrue(is_array(Lang\Constant::get()->readAll()));
    }


}
 