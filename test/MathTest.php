<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 05.06.15
 * Time: 21:46
 */

namespace kernel;

class mathTest extends \PHPUnit_Framework_TestCase {


    public function test_gmp_fact_throw_on_less_than_zero()
    {
        $this->setExpectedException("Exception", "Argument should be greater or equals to zero");
        Math::gmpFact(-1);
    }

    public function test_gmp_fact_throw_on_float()
    {
        $this->setExpectedException("Exception", "Argument should be an integer");
        Math::gmpFact(3.5);
    }

    public function test_gmp_fact_is_1_for_0()
    {
        $this->assertTrue(Math::gmpFact(0)===1);
    }

    public function test_gmp_fact_is_24_for_4()
    {
        $this->assertTrue(Math::gmpFact(4)===24);
    }

}
