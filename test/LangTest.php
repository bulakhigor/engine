<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 18:12
 */

namespace kernel;


class langTest extends \PHPUnit_Framework_TestCase {

    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testGetUsingLangReturn2CharCode()
    {
        $this->assertTrue(strlen(Lang::get()->getUsing())===2);
    }

}
 