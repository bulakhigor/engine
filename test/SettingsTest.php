<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 28.06.15
 * Time: 01:27
 */

namespace kernel;


class settingsTest extends \PHPUnit_Framework_TestCase {

    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testLoading()
    {
        $obj = Settings::get();
        $this->assertTrue(is_object($obj), "Settings can not be loaded");
    }

    public function testReadValue()
    {
        $value = Settings::get()->readValue('thumb_size');
        $this->assertTrue(is_numeric($value), "Setting thumb_size is not numeric");
    }

    public function testWrite()
    {
        Settings::get()->write('thumb_size', 251);
    }

    public function testAfterWrite()
    {
        DB::get()->prepare("SELECT value FROM settings WHERE skey=?")
                ->bind(1, 'thumb_size')
                ->execute();
        $row = DB::get()->fetchRow();
        $this->assertTrue($row['value']==251, "Value in DB does not match written.");
    }



}
 