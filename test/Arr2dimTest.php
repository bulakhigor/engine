<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 05.06.15
 * Time: 23:28
 */

namespace kernel;

class arr2dimTest extends \PHPUnit_Framework_TestCase {

    public function test_is_2dim_false_on_string()
    {
        $this->assertFalse(Arr2dim::is2dim('string'));
    }

    public function test_is_2dim_false_on_1dim_array()
    {
        $this->assertFalse(Arr2dim::is2dim(array(1,2,3)));
    }

    public function test_is_2dim_true_on_2dim_array()
    {
        $this->assertTrue(Arr2dim::is2dim(array('a'=>array('b'=>'c'),'d'=>array('e'=>'f'))));
    }





    public function test_make_2dim_throw_on_string()
    {
        $this->setExpectedException("Exception", "Argument should be an array");
        Arr2dim::make2dim('string');
    }

    public function test_make_2dim_array_for_array()
    {
        $this->assertTrue(Arr2dim::make2dim(array()) === array());
    }

    public function test_make_2dim_creates_2dim_on_1dim_array()
    {
        $this->assertTrue(Arr2dim::make2dim(array(1,2,3)) === array(
            0=>array('id'=>0,'value'=>1),
            1=>array('id'=>1,'value'=>2),
            2=>array('id'=>2,'value'=>3)
        ));
    }






    public function test_kv_throw_on_string()
    {
        $this->setExpectedException('Exception', "Argument should be an array");
        Arr2dim::kv('string','a','b');
    }

    public function test_kv_array_on_array()
    {
        $this->assertTrue(Arr2dim::kv(array(),'a','b') === array());
    }

    public function test_kv_array_on_2dim_array()
    {
        $this->assertTrue(Arr2dim::kv(array(
                array('key'=>'a','value'=>'b'),
                array('key'=>'c','value'=>'d')
            ),'key','value')===array('a'=>'b','c'=>'d'));
    }






    public function test_inc_throw_on_string_as_2dim_array()
    {
        $this->setExpectedException('Exception', "Argument should be an array");
        Arr2dim::inc('string',array());
    }

    public function test_inc_throw_on_string_as_1dim_array()
    {
        $this->setExpectedException('Exception', "Argument should be an array");
        Arr2dim::inc(array(),'string');
    }

    public function test_inc_merge_arrays()
    {
        $this->assertTrue(Arr2dim::inc(
                    array(array('a'=>'b')),
                    array('c'=>'d'))
            ===array(
                    array('a'=>'b'),
                    array('c'=>'d')
            ));
    }

    public function test_inc_merge_arrays_to_top()
    {
        $this->assertTrue(Arr2dim::inc(
                array(array('a'=>'b')),
                array('c'=>'d'),
                true)
            ===array(
                array('c'=>'d'),
                array('a'=>'b')
            ));
    }








    public function test_distinct_throw_on_string()
    {
        $this->setExpectedException('Exception', "Argument should be an array");
        Arr2dim::distinct('string','a', 'b');
    }

    public function test_distinct_sort_2dim_array_by_value_and_key()
    {
        $test = Arr2dim::distinct(array(
            array('key'=>'a', 'value'=>'e'),
            array('key'=>'a', 'value'=>'b'),
            array('key'=>'c', 'value'=>'d')
        ),'key', 'value');
        $expected = array(
            'Ba' => array('key'=>'a', 'value'=>'b'),
            'Dc' => array('key'=>'c', 'value'=>'d'),
            'Ea' => array('key'=>'a', 'value'=>'e')
        );
        $this->assertTrue($test===$expected, var_export($test,true));
    }






    public function test_keys_throw_on_string()
    {
        $this->setExpectedException('Exception', "Argument should be an array");
        Arr2dim::keys('string', 'a');
    }

    public function test_keys_fetch_unique_keys_of_2dim_array()
    {
        $test = Arr2dim::keys(array(
            array('key'=>'b', 'value'=>'e'),
            array('key'=>'a', 'value'=>'d'),
            array('key'=>'b', 'value'=>'c')
        ), 'key');
        $expected = array('b','a');
        $this->assertTrue($test===$expected, var_export($test,true));
    }

    public function test_keys_fetch_unique_keys_with_empty()
    {
        $test = Arr2dim::keys(array(
            array('key'=>'b', 'value'=>'e'),
            array('key'=>'a', 'value'=>'d'),
            array('key'=>'b', 'value'=>'c'),
            array('value'=>'empty')
        ), 'key');
        $expected = array('b','a',NULL);
        $this->assertTrue($test===$expected, var_export($test,true));

    }

}
 