<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 12.07.15
 * Time: 19:20
 */

namespace kernel;


class styleTest extends \PHPUnit_Framework_TestCase {
    private $testText = "body > div { background-image: url('http://'); }";
    private $testTextUnconverted = "body &gt; div { background-image: url('http://'); }";


    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testLoading()
    {
        $obj = Style::get();
        $this->assertTrue(is_object($obj), "Style can not be loaded");
    }

    public function testReadAllReturnsNonEmptyArray()
    {
        $array = Style::get()->readAll();
        $this->assertTrue(count($array)>0, "Styles are empty");
    }

    public function testIsExistsWorks()
    {
        $array = Style::get()->readAll();
        $this->assertTrue(Style::get()->isExists($array[0]->readCode(), 0));
    }

    public function testReadByCodeWorks()
    {
        $array = Style::get()->readAll();
        $this->assertTrue(Style::get()->readByCode($array[0]->readCode())->readCode() === $array[0]->readCode());
    }

    public function testReadByIdWorks()
    {
        $array = Style::get()->readAll();
        $this->assertTrue(Style::get()->readById($array[0]->readId())->readId() === $array[0]->readId());
    }

    public function testWriteTextWorks()
    {
        $style = Style\Item::create()->writeCode('phpunit_test_'.md5(time()))
                                ->writeDescription('this is phpunit test')
                                ->writeText($this->testText);
        $this->assertTrue($style->readText() === $this->testText);
    }

    public function testReadTextForEditorWorks()
    {
        $style = Style\Item::create()->writeCode('phpunit_test_'.md5(time()))
            ->writeDescription('this is phpunit test')
            ->writeText($this->testText);
        $this->assertTrue($style->readTextForEditor() === $this->testTextUnconverted);
    }

    public function testWrite()
    {
        $style = Style\Item::create()->writeCode('phpunit_test_'.md5(time()))
            ->writeDescription('this is phpunit test')
            ->writeText($this->testText);
        Style::get()->write($style);
        $this->assertTrue($style->readId() > 0);
    }


}
 