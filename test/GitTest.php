<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 10.07.15
 * Time: 18:09
 */

namespace kernel;


class gitTest extends \PHPUnit_Framework_TestCase {

    public function testGetCurrentBranchIsString()
    {
        $git = new Git();
        $this->assertTrue(is_string($git->getCurrentBranch())===true);
    }

}
 