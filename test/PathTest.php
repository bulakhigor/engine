<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 12.07.15
 * Time: 19:37
 */

namespace kernel;


class pathTest extends \PHPUnit_Framework_TestCase {

    public function testGetRoot()
    {
        $this->assertTrue(Path::getRoot() === realpath(__DIR__.'/..'));
    }

    public function testResolveWithoutSlash()
    {
        $this->assertTrue(Path::resolve('tmp') === Path::getRoot().'/tmp');
    }

    public function testResolveWithSlash()
    {
        $this->assertTrue(Path::resolve('/tmp') === Path::getRoot().'/tmp');
    }


}
 