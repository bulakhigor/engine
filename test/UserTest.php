<?php
/**
 * Created by PhpStorm.
 * User: Robin
 * Date: 15.07.15
 * Time: 11:54
 */

namespace kernel;


class userTest extends \PHPUnit_Framework_TestCase {
    private $email = 'phpunit@test.test';
    private $password = 'test';

    public static function setUpBeforeClass()
    {
        PHPunit::initFakeDatabaseForTests();
    }

    public function testWrite()
    {
        $data = User\Item::create()
                    ->writeName('phpunit test user')
                    ->writeEmail($this->email)
                    ->writePassword($this->password);
        $newid = User::get()->write($data);
        $this->assertTrue($newid > 0);
    }

    public function testValidate()
    {
        $result = User::get()->validate($this->email, $this->password);
        $this->assertTrue($result);
    }

}
 