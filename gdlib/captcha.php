<?
include("../init.php");

		$rid = intval(kernel\Input::get()->read('rid'));
		if (!$rid) die("captcha: no rid key given");
		$code = kernel\Security\Captcha::get($rid);
		if ($code) 
			kernel\GDlib::captcha(preg_split('//', $code, -1, PREG_SPLIT_NO_EMPTY));
		else
			die("captcha: requested key was not found in db or empty");


?>