<?php

// display errors
ini_set('display_errors', true);

// errors are equals to all except notices
Error_Reporting(E_ALL & ~E_NOTICE);

// locale is RU excluding numeric
setlocale(LC_ALL, 'ru_RU.UTF-8');
setlocale(LC_NUMERIC, 'C');

// time zone is Moscow
date_default_timezone_set('Europe/Moscow'); 

// autoload classes
include_once("autoload.php");
include_once("vendor/autoload.php");

// use error handling for libxml
libxml_use_internal_errors(true);

// error handler is kernel\Error::ErrorHandler()
set_error_handler(array('kernel\Error', 'ErrorHandler'), E_ALL & ~E_NOTICE);

// exception handler is the same
set_exception_handler(array('kernel\Error', 'ExceptionHandler'));

// assertion failure throws and exception
assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_CALLBACK, function($file, $line, $code, $desc = null) { throw new Exception($file.":".$line." (".$code.") ".$desc); });
